function output = collect_log_from_folder(Folder2SaveTable,FolderLocal,FolderClust,Expe)

%FolderClust='/mnt/HDD_ORGERLAB/adrien/ExperimentalData/SmallCircularOMRv2/Data'
%FolderLocal='G:\SmallCircularOMRv2\Data'
%Expe='SmallCircularOMRv2'
%Folder2SaveTable=G:\SmallCircularOMRv2

FilesLocation=[FolderLocal,'\**\',Expe,'_*000.txt']
files = dir(FilesLocation);

%% Remove Non Unique Filenames:
files_hash=[];

for i=1:numel(files)
    filename=files(i).name;
    sha256hasher = System.Security.Cryptography.SHA256Managed;
    sha256 = uint8(sha256hasher.ComputeHash(uint8(filename))); %consider the string as 8-bit characters   
    files_hash=[files_hash;sha256];
end
[a,id_unique]=unique(files_hash,'rows');


%% Initialize Table Fields to get info from log name:

fish_rig=[];
fish_folder_local=[];
fish_filename=[];
fish_stimlog_filename=[];
fish_date=[];
fish_strain=[];
fish_tank=[];
fish_clutch=[];
fish_age=[];
fish_timeperiod=[];
fish_resolution=[];

fish_circle=[];

iter=1;
for i=1:length(id_unique)

    filename=files(id_unique(i)).name;
    
    
    id=strfind(filename,'_');
    
    % Find Which Rig was used:
    rig=filename(id(end)+1:end);
    j=strfind(rig,'0');
    rig=rig(1:j-1);
    
    fish_rig{iter}=rig;
    
    % Collect Files Location:
    
    fish_folder_local{iter}=files(id_unique(i)).folder;
    fish_filename{iter}=filename;
    
    % Rig Diameter:
    diameter=str2num(filename(id(end-1)+1:id(end)-1));
    fish_circle=[fish_circle;diameter];
    % Resolution microns per pixel
    res=str2num(filename(id(end-2)+1:id(end-1)-1));
    fish_resolution=[fish_resolution;res];
    
    % Time Period
    P=str2num(filename(id(end-3)+2:id(end-2)-1));
    fish_timeperiod=[fish_timeperiod;P];
    
    % Age
    age=str2num(filename(id(end-4)+1:id(end-3)-4));
    fish_age=[fish_age;age];
    
    % Clutch Number:
    C=str2num(filename(id(end-5)+2:id(end-4)-1));
    fish_clutch=[fish_clutch;C];    
    
    % Tank Number:
    T=str2num(filename(id(end-6)+5:id(end-5)-1));
    fish_tank=[fish_tank;T];     
        
    % Fish Strain
    fish_strain{iter}=filename(id(end-7)+1:id(end-6)-1);
    
    % Recording Date:
    date=filename(id(end-10)+1:id(end-7)-1);
    
    fish_date=[fish_date;date];
    
    % Find StimLog Filename:
    files_stimLog=dir(fish_folder_local{iter})
    
    for k=1:numel(files_stimLog)
        filename=files_stimLog(k).name;
        
        id=strfind(filename,'.txt');
        if numel(id)>0
        id=strfind(filename,'stimlog');
        if numel(id)>0
            fish_stimlog_filename{iter}=filename(1:end-4);
        end
        end
        
    end
    iter=iter+1;
end
fish_strain=fish_strain';
fish_rig=fish_rig';
fish_folder_local=fish_folder_local';
fish_filename=fish_filename';
fish_stimlog_filename=fish_stimlog_filename';

% Transform Local Folder to Cluster 

fish_folder_cluster=[];
for i=1:numel(fish_folder_local)
    i
    
    FoldLoc=fish_folder_local{i};
    FoldClust = strrep(FoldLoc,FolderLocal,FolderClust);
    FoldClust = strrep(FoldClust,'\','/');
    fish_folder_cluster{i}=FoldClust;
end
fish_folder_cluster=fish_folder_cluster';
%
T=table(fish_strain,fish_age,fish_date,fish_timeperiod,fish_circle,fish_resolution,fish_rig,fish_tank,fish_clutch,fish_folder_local,fish_folder_cluster,fish_filename,fish_stimlog_filename) %creat table with all the variables

%
TableName=[Folder2SaveTable,'\',Expe,'_Table.mat']
save(TableName,'T')
TableName=[Folder2SaveTable,'\',Expe,'_Table.csv']
writetable(T,TableName)

end

% pause
% %%
% clear all;close all;clc
% addpath('./ReadData')
% load('SmallCircularOMRv2_Table.mat')
% for i=1:size(T,1)
%     i    
%     folder=cell2mat(T.fish_folder_local(i));
%     filename=cell2mat(T.fish_stimlog_filename(i));
%     if exist([folder,'\',filename,'.mat'])~=2
%         StimLog2Mat([folder,'\',filename,'.txt'])
%     end
% end

