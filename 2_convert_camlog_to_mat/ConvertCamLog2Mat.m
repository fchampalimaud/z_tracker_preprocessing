clear all;close all;clc
addpath('./ReadData')

load('SmallCircularOMRv2_Table.mat')

pause
for i=1:size(T,1)
    i
    folder=cell2mat(T.fish_folder_local(i));
    filename=cell2mat(T.fish_filename(i));
    if exist([folder,'\',filename(1:end-4),'.mat'])~=2
         SaveTxt2Mat([folder,'\',filename])
    end
end