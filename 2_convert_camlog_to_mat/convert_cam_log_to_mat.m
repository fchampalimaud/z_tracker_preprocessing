function convert_cam_log_to_mat(Table)

    for i=1:size(Table,1)
        i
        folder=cell2mat(Table.fish_folder_local(i));
        filename=cell2mat(Table.fish_filename(i));
        if exist([folder,'\',filename(1:end-4),'.mat'])~=2
            SaveTxt2Mat([folder,'\',filename])
        end
    end

end
