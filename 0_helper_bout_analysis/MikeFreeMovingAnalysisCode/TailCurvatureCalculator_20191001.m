
 function [boutCurvature] = TailCurvatureCalculator_20191001(cumsumInterpolatedAngleByBout,tailMM)
%very noisy

%this function calculates the curvature of the tail 

%%
%%%%%%%%%%%%transform angles into positions%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sizeSegment = mean(diff(tailMM));

tailPosX = cumsumInterpolatedAngleByBout.*0;
tailPosY = cumsumInterpolatedAngleByBout.*0;

for nn = 1 : size(cumsumInterpolatedAngleByBout,2)%loop througg each segment

%     %calcualte rho
%     rho = sizeSegment./cos(cumsumInterpolatedAngleByBout(:,nn));
    
 [tailPosNormX,tailPosNormY] = pol2cart(cumsumInterpolatedAngleByBout(:,nn),sizeSegment);
%  tailPosNormX = tailPosNormX*sizeSegment;
%  tailPosNormY = tailPosNormY*sizeSegment;

 
  if nn == 1
 
 tailPosX(:,1) = tailPosNormX;
 tailPosY(:,1) = tailPosNormY;
 
 
 else
     tailPosX(:,nn) = tailPosX(:,(nn-1)) + tailPosNormX;
     tailPosY(:,nn) = tailPosY(:,(nn-1)) + tailPosNormY;
%      
 end
end

% %test tail positions
% for n = 1 :10: size(halfBeatTailAngles2,1)
%     
%     
%     
% hold off
% plot(tailPosX(n,:),tailPosY(n,:))
% hold on
% plot(tailPosX(n,:),tailPosY(n,:), 'ro')
% 
% axis([-5 5 -5 5])
% pause
% end



%%
% %%%%%%%%%%%%%%%%%%smooth dail data%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% halfBeatTailAngles =cumsumInterpolatedAngleByBout*0;
% for n = 1:size(cumsumInterpolatedAngleByBout,2)
%     
% halfBeatTailAngles(:,n) = smooth(cumsumInterpolatedAngleByBout(:,n),100);
% 
% end
% 
% 
% halfBeatTailAngles2 =halfBeatTailAngles*0;
% for n = 1:size(cumsumInterpolatedAngleByBout,1)
%     
% halfBeatTailAngles2(n,:) = smooth(halfBeatTailAngles(n,:),10);
% 
% end
% 
% plot(cumsumInterpolatedAngleByBout,'color', 'blue')
% hold on
% plot(halfBeatTailAngles2, 'color', 'red')


%%
%%%%%%%%%%calculate curvature%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%test tail curvature 
nframes=size(tailPosY,1);

xSpeed2=diff(tailPosX,[],2);
xAc2 = [diff(xSpeed2,[],2) zeros(nframes,1)];
ySpeed2 = diff(tailPosY,[],2);
yAc2 =  [diff(ySpeed2,[],2) zeros(nframes,1)];

curvature2=[zeros(nframes,1) sqrt(xAc2.^2 + yAc2.^2)./(xSpeed2.^2 + ySpeed2.^2)];
curvature2(find(tailPosY<=0))=curvature2(find(tailPosY<=0))*-1;

%%
%%%%%%%%%%%%%%%%%average all segments%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
boutCurvature = nanmean(curvature2(:,2:end),2);
% plot(meanCurvature)

% %pause
% figure
% plot(cumsumInterpolatedAngleByBout, 'color', 'k')
% hold on
% plot(halfBeatTailAngles2, 'color', 'r')
% 
% 
% figure
% plot(curvature)
% figure
% imagesc(curvature)
% 
% figure
% imagesc(cumsumInterpolatedAngleByBout)