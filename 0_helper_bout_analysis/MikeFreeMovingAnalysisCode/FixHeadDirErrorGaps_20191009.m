

function [fixedSegmentAngles, errorListObends,fixedBodyAngles,totalNumberFrames] = FixHeadDirErrorGaps_20191009(fixedSegmentAngles,chosenSegmentValues,bodyAngles);

% hold off
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%test function%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
errorListObends=zeros(size(fixedSegmentAngles,1),1);
%  erasedSegmentAngle = chosenTailAngles; 

%%
totalLength=size(fixedSegmentAngles,1);
% errorList = zeros(1,size(erasedSegmentAngle,1));
totalNumberFrames=0;
%loop through each segment and fix when tail tracking fails
% size(sum(abs(fixedSegmentAngles')))
% size(chosenSegmentValues(:,2))
% 'prctile'

chosenSegmentValues(isnan(chosenSegmentValues))=0;
sumValues=sum(chosenSegmentValues')';
threshold1=median(chosenSegmentValues(:,1))/3;
threshold2=median(sumValues)/3;

% allTrackingFailurePoints=(sum(abs(fixedSegmentAngles'))==0)'|(chosenSegmentValues(:,2)<(min(100,prctile(chosenSegmentValues(:,2),1)*0.9)));
allTrackingFailurePoints=(sum(abs(fixedSegmentAngles'))==0)'|(chosenSegmentValues(:,1)<threshold1)|(sumValues<threshold2);

trackingFailureStarts=find(diff(allTrackingFailurePoints)==1)+1;
trackingFailureEnds=find(diff(allTrackingFailurePoints)==-1);

if ((~isempty(trackingFailureStarts))|(~isempty(trackingFailureEnds)))
    
if (trackingFailureEnds(1)<trackingFailureStarts(1))
    trackingFailureStarts=[1; trackingFailureStarts];
end
if (length(trackingFailureStarts)>length(trackingFailureEnds))
    
    trackingFailureEnds=[trackingFailureEnds; length(trackingFailureStarts)];
end

angleDiff=diff(bodyAngles);

for i = 1 : length(trackingFailureStarts)
if (trackingFailureStarts(i)>1)
sumang=sum(angleDiff(trackingFailureStarts(i)-1:trackingFailureEnds(i)));
lngth=length(angleDiff(trackingFailureStarts(i)-1:trackingFailureEnds(i)));
if (sumang>pi)  sumang=sumang-ceil((sumang-pi)/(2*pi))*2*pi;  end
if (sumang<-pi)   sumang=sumang+ceil(abs(sumang+pi)/(2*pi))*2*pi;   end
    angleDiff(trackingFailureStarts(i)-1:trackingFailureEnds(i))=sumang/lngth;
else 
sumang=sum(angleDiff(trackingFailureStarts(i):trackingFailureEnds(i)));
lngth=length(angleDiff(trackingFailureStarts(i):trackingFailureEnds(i)));
if (sumang>pi)   sumang=sumang-ceil((sumang-pi)/(2*pi))*2*pi;   end
if (sumang<-pi)  sumang=sumang+ceil(abs(sumang+pi)/(2*pi))*2*pi;   end
    angleDiff(trackingFailureStarts(i):trackingFailureEnds(i))=sumang/lngth;
end
end
    

bodyAngles=cumsum([bodyAngles(1) angleDiff']');



for i = 1 : length(trackingFailureStarts)
    if (trackingFailureStarts(i)>1)
    clear xs
numberThisFix=1;
    xs(1)=-1;
    if  ((trackingFailureStarts(i)>2)&~(allTrackingFailurePoints(trackingFailureStarts(i)-2)))
    xs=[xs -2];
    end
    if  ((trackingFailureStarts(i)>3)&~(allTrackingFailurePoints(trackingFailureStarts(i)-3)))
    xs=[xs -3];
    end
    currentPoint=trackingFailureStarts(i);
    pointsAdded=0;
    while(pointsAdded<3)
        currentPoint=currentPoint+1;
        
        if (currentPoint<=totalLength)
            if (~allTrackingFailurePoints(currentPoint))
            xs=[xs currentPoint-trackingFailureStarts(i)];
            pointsAdded=pointsAdded+1;
            else
                totalNumberFrames=totalNumberFrames+1;
                numberThisFix=numberThisFix+1;
            end
       else
          break 
       end
    end
    
%     xs
%     numberThisFix
   
%     figure
if ((numberThisFix<10)&(pointsAdded>0))
%     subplot(1,2,1)
% hold off
%     plot(fixedSegmentAngles(trackingFailureStarts(i)-50:trackingFailureStarts(i)+50,:),'b')
%     hold on
    for whichseg=1:size(fixedSegmentAngles,2)
%         size(xs)
%         size(fixedSegmentAngles(xs+trackingFailureStarts(i),whichseg))
     polycoeffs = polyfit(xs',fixedSegmentAngles(xs+trackingFailureStarts(i),whichseg),2);
     fixedSegmentAngles(trackingFailureStarts(i):trackingFailureStarts(i)+numberThisFix-1,whichseg)=polyval(polycoeffs,[0:numberThisFix-1]);
    end
    
     polycoeffs = polyfit(xs',bodyAngles(xs+trackingFailureStarts(i)),2);
     bodyAngles(trackingFailureStarts(i):trackingFailureStarts(i)+numberThisFix-1)=polyval(polycoeffs,[0:numberThisFix-1]);
%     subplot(1,2,2)
%     hold off
%     plot(fixedSegmentAngles(trackingFailureStarts(i)-50:trackingFailureStarts(i)+50,:),'r')
%      clear xs
%      drawnow
%      pause
%     pause
end
    errorListObends(trackingFailureStarts(i):trackingFailureStarts(i)+numberThisFix-1)=numberThisFix;
    
    else
    numberThisFix=trackingFailureEnds(1);
    errorListObends(trackingFailureStarts(1):trackingFailureEnds(1))=numberThisFix;
    end
end
end
    %%
    fixedBodyAngles=bodyAngles;
