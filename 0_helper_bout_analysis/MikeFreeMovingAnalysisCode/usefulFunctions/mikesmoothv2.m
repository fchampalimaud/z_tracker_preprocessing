function [ output_data ] = mikesmoothv2( input_data , span )
%UNTITLED Summary of this function goes here
%   This is a function to do 1D boxcar filtering on 1D or 2D data, without making artefacts at the extreme values 
output_data=zeros(size(input_data));

output_data=conv2(input_data,ones(span,1)/span,'same');


endspan=span;
if (mod(endspan,2)==0)
    endspan=endspan+1;
end
for m=0:(endspan-3)/2;
    output_data(m+1,:)=mean(input_data(1:m*2+1,:),1);
    output_data(end-m,:)=mean(input_data(end-m*2:end,:),1);
end




end

