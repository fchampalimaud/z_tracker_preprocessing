
% datafolder{1}='M:\OMR_Ontogeny_VOL\c3po'
startFolder='Z:\SmallCircularOMR\SmallCircularOMRv2\Data';
% startFolder='F:\Pedro_WallE\VirtualOpenLoop_OMR'
allfish=dir(fullfile(startFolder,'Small*Anj*'));

allBoutKinematicParameters=[];
    allBoutLengths=[];
    allInterBoutLengths=[];
    
for whichfish=1:length(allfish)
whichfish
fishfolder=fullfile(startFolder,allfish(whichfish).name)

        	thisdatafile=dir(fullfile(fishfolder,'matfiles','*000MAIN.mat'));
            if (length(thisdatafile)>0)
           
            dataFolder=fishfolder;
            saveFolder=fullfile(dataFolder,'matfiles');
            dataflag=false;

            if (exist(fullfile(saveFolder,'tempBout7.mat')))
            load(fullfile(saveFolder,'tempBout7.mat'));
            dataflag=true;
            end
            if (dataflag)
                allboutstarts(find(rejectedBouts))=[];
                allboutends(find(rejectedBouts))=[];
                
           boutLengths=allboutends-allboutstarts;
           interboutLengths=allboutstarts(2:end)-allboutends(1:end-1);
    allBoutKinematicParameters=[allBoutKinematicParameters ; BoutKinematicParameters];
    allBoutLengths=[allBoutLengths ; boutLengths'];
    allInterBoutLengths=[allInterBoutLengths ; interboutLengths'];
    
            end
            end
        end
    save(fullfile(startFolder,'Anjupooled.mat'),'allBoutKinematicParameters','allBoutLengths','allInterBoutLengths')
    