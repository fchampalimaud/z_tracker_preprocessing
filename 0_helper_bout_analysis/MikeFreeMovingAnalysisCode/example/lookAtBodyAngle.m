clear all
close all
% ii=figure
% datafolder{1}='M:\OMR_Ontogeny_VOL\c3po'
datafolder{1}='F:\Pedro_WallE\VirtualOpenLoop_OMR';
datafolder{2}='F:\Pedro_WallE\atlas'
datafolder{3}='F:\Pedro_WallE\c3po'
count=1;
ii=figure
for whichrig=1:length(datafolder)

alldatasets=dir(fullfile(datafolder{whichrig}));

for whichdataset=3:length(alldatasets)
    thisdataset=fullfile(datafolder{whichrig},alldatasets(whichdataset).name);
    alldays=dir(fullfile(thisdataset,'*dpf'));
    for whichday=1:length(alldays)
    thisday=fullfile(thisdataset,alldays(whichday).name);
    allfish=dir(fullfile(thisday,'*P*'));
    allBoutKinematicParameters=[];
    allBoutLengths=[];
    allInterBoutLengths=[];
        for whichfish=1:length(allfish)
            fishfolder=fullfile(thisday,allfish(whichfish).name)
        	thisdatafile=dir(fullfile(fishfolder,'matfiles','*000MAIN.mat'));
            if (length(thisdatafile)>0)
           
                  dataFolder=fishfolder;
            saveFolder=fullfile(dataFolder,'matfiles');
            dataflag=false;
%             if (exist(fullfile(saveFolder,'tempBout.mat')))
%             load(fullfile(saveFolder,'tempBout.mat'));
%             dataflag=true;
%             end
debugfile=dir(fullfile(saveFolder,'*FLAGS_AND_DEBUG.mat'));
load(fullfile(saveFolder,debugfile(1).name));
[nums,bins]=hist(tailValues(:,1),[0:2:1000]);
% if (mod(count-1,25)==0)

% end
figure(ii)
% subplot(5,5,mod(count-1,25)+1)
subplot(1,2,1)
tailValues(isnan(tailValues))=0;
plot(sum(tailValues'),'.')
medianval=median(sum(tailValues'));
axis([1 2000000 0 2000])
line([1 2000000],[medianval/3 medianval/3])
% hold onx  
% [nums,bins]=hist(tailValues(:,2),[0:2:1000]);
% plot(bins,log(nums),'r')

subplot(1,2,2)
plot(tailValues(:,1),'.')
medianval=median(tailValues(:,1));
axis([1 2000000 0 800])
line([1 2000000],[medianval/3 medianval/3])
% 
% subplot(1,3,3) 
% plot(tailValues(:,2),'.')
% axis([1 2000000 0 800])
% line([1 2000000],[100 100])
drawnow
pause
% close(ii)
            if (exist(fullfile(saveFolder,'tempBout5.mat')))
            load(fullfile(saveFolder,'tempBout5.mat'));
            dataflag=true;
            end
            
            if (exist(fullfile(saveFolder,'tempBout5b.mat')))
            load(fullfile(saveFolder,'tempBout5b.mat'));
            dataflag=true;
            end
            if (dataflag)
                thisname{count}=fishfolder;
                errors(count)=length(find(abs(diff(realBodyAngles))>pi))
                
               count=count+1;
    
            end
            end
        end
%     save(fullfile(thisday,'pooled.mat'),'allBoutKinematicParameters','allBoutLengths','allInterBoutLengths')
    end
end
end