close all
rootfolder='F:\Pedro_WallE\VirtualOpenLoop_OMR'
CTRS{1}=-180:4:180;
CTRS{2}=-40000:1000:80000;

 ii=figure
 jj=figure
 days=[4 5 6  7 8 10];
 for n=1:6
     thisfilename=fullfile(rootfolder,strcat('giantpooled',num2str(days(n)),'.mat'));
     load (thisfilename)

 figure(jj)
 [nums,bins]=hist(pooledBoutLengths,1:5:1000)
 plot(bins(1:end-1),nums(1:end-1)/sum(nums),'Color',[1 0 0]*n/6+[0 1 0]*(6-n)/6) 
 hold on
 giantbouts(n,:)=nums;

 figure(ii)
 [nums,bins]=hist(pooledInterBoutLengths,1:5:1000)
plot(bins(1:end-1),nums(1:end-1)/sum(nums),'Color',[0 1 1]*n/6+[1 0 0]*(6-n)/6) 
 giantinterbouts(n,:)=nums;
 end
 
 
 ii=figure
 jj=figure
 days=[4 5 6 8 10 12 14];
 for n=1:7
     thisfilename=fullfile(rootfolder,strcat('tupooled',num2str(days(n)),'.mat'));
     load (thisfilename)
 

 figure(jj)
 [nums,bins]=hist(pooledBoutLengths,1:5:1000)
 plot(bins(1:end-1),nums(1:end-1)/sum(nums),'Color',[1 0 0]*n/7+[0 1 0]*(7-n)/7) 
 hold on
 tubouts(n,:)=nums;

 figure(ii)
 [nums,bins]=hist(pooledInterBoutLengths,1:5:1000)
plot(bins(1:end-1),nums(1:end-1)/sum(nums),'Color',[0 1 1]*n/7+[1 0 0]*(7-n)/7) 
 hold on
 tuinterbouts(n,:)=nums;
 end
 bincentres=bins;
 
 figure
 for n=1:6
    plot(bincentres(1:100)*1000/700,giantbouts(n,1:100)/sum(giantbouts(n,:)))
    hold on
    
 end
 xlabel('Time in ms')
 ylabel('Proportion of bouts')
 title('Giant Danio OMR Bout Durations')
 legend('4 dpf','5 dpf','6 dpf','7 dpf','8 dpf','10 dpf')
 
 figure
 for n=1:6
    plot(bincentres(1:199)*1000/700,giantinterbouts(n,1:199)/sum(giantinterbouts(n,:)))
    hold on
    
 end
 xlabel('Time in ms')
 ylabel('Proportion of interbouts')
 title('Giant Danio OMR Interbout Durations')
 legend('4 dpf','5 dpf','6 dpf','7 dpf','8 dpf','10 dpf')
 
 days=[4 5 6  7 8 10];
 
 
 figure
 for n=1:7
    plot(bincentres(1:100)*1000/700,tubouts(n,1:100)/sum(tubouts(n,:)))
    hold on
    
 end
 xlabel('Time in ms')
 ylabel('Proportion of bouts')
 title('Tu Zebrafish OMR Bout Durations')
 legend('4 dpf','5 dpf','6 dpf','8 dpf','10 dpf','12 dpf','14 dpf')
 
 
 figure
 for n=1:7
    plot(bincentres(1:199)*1000/700,tuinterbouts(n,1:199)/sum(tuinterbouts(n,:)))
    hold on
    
 end
 xlabel('Time in ms')
 ylabel('Proportion of interbouts')
 title('Tu Zebrafish OMR Interbout Durations')
 legend('4 dpf','5 dpf','6 dpf','8 dpf','10 dpf','12 dpf','14 dpf')
 
 
 days=[4 5 6 8 10 12 14];
 