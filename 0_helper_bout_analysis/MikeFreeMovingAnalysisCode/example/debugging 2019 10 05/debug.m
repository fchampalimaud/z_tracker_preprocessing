
boutMapFolder='F:\FreelyMovingBehaviorAnalysis_16\assignBoutTypesUsingMap\BoutMap';
rawDataFolder=saveFolder;
basename=dataFile(1:end-4);


boutMapFolder='F:\FreelyMovingBehaviorAnalysis_16\assignBoutTypesUsingMap\BoutMap';
rawDataFolder='F:\Pedro_WallE\VirtualOpenLoop_OMR\Giant_Danio_second_run\5dpf\P1\matfiles';
basename='OMR_Ontogeny_VOL_11_03_19_Giant_Tank2_C16_05dpf_P1_76_66_walle000';

for n=1:length(allboutstarts)
   thismaxsm(n)=max(smootherTailCurveMeasure(allboutstarts(n):allboutends(n))); 
end
smallbouts=find((thismaxsm>0.13)&(thismaxsm<0.16));
for n=1:length(smallbouts)
    plot(cumsumInterpFixedSegmentAngles(allboutstarts(smallbouts(n)):allboutends(smallbouts(n)),1:8),'b')
    hold on
    plot(smootherTailCurveMeasure(allboutstarts(smallbouts(n)):allboutends(smallbouts(n)))+1,'r')
    hold off
    drawnow
    disp(thismaxsm(smallbouts(n)))
    pause
end
startp=1067
endp=1069

figure
plot(cumsumInterpFixedSegmentAngles(allboutstarts(startp):allboutends(endp),2:2:8),'b')
    hold on
    plot(smootherTailCurveMeasure(allboutstarts(startp):allboutends(endp))*10,'r')
    for m=startp:endp
line([allboutstarts(m) allboutstarts(m)]-allboutstarts(startp),[-1 1],'Color',[1 0 0])
line([allboutends(m) allboutends(m)]-allboutstarts(startp),[-1 1],'Color',[0 1 0])
line([truestarts(m) truestarts(m)]-allboutstarts(startp),[-1 1],'Color',[1 0 1])
% line([indRealEnds(m) indRealEnds(m)]-allboutstarts(startp),[-1 1],'Color',[0 1 1])
end
    hold off
    
    for n=1:length(allshort)
        startp=allshort(n)
        endp=allshort(n)
plot(cumsumInterpFixedSegmentAngles(allboutstarts(startp):allboutends(endp),8),'b')
    hold on
    plot(smootherTailCurveMeasure(allboutstarts(startp):allboutends(endp))*10,'r')
    hold off
    drawnow 
    pause
    end
%     drawnow
%     disp(thismaxsm(smallbouts(n)))
%     pause

figure
for n=1:10:floor(length(smootherTailCurveMeasure)/5000)
    
hold off
 plot([1:5000]+5000*(n-1),smoothedCumsumInterpFixedSegmentAngles([1:5000]+5000*(n-1),8))
hold on
usefulbouts=find(allboutstarts>5000*(n-1)+1,1,'first'):find(allboutends<5000*(n-1)+5000,1,'last')
title(usefulbouts')
for m=usefulbouts
line([allboutstarts(m) allboutstarts(m)],[-1 1],'Color',[1 0 0])
line([allboutends(m) allboutends(m)],[-1 1],'Color',[0 1 0])
line([indRealEnds(m) indRealEnds(m)],[-1 1],'Color',[0 1 1])
end
drawnow;
pause
end

allbig=find((BoutKinematicParameters(:,7)>90)&(BoutKinematicParameters(:,222)>50));
for n=1:length(allbig)
m=allbig(n);
plot(smoothedCumsumInterpFixedSegmentAngles(allboutstarts(m):allboutends(m),1:7))
hold on
plot(realBodyAngles(allboutstarts(m):allboutends(m))-realBodyAngles(allboutstarts(m)))
hold off
drawnow
pause
end

% plot3(allBoutKinematicParameters(:,17),allBoutKinematicParameters(:,11),allBoutKinematicParameters(:,222),'.')
data=allBoutKinematicParameters(1:10:end,[17 11 222]);
[idx]=kmeans(zscore(data),6);
figure

plot3(data(find(idx==1),1),data(find(idx==1),2),data(find(idx==1),3),'r.')
hold on
plot3(data(find(idx==2),1),data(find(idx==2),2),data(find(idx==2),3),'g.')
plot3(data(find(idx==3),1),data(find(idx==3),2),data(find(idx==3),3),'b.')
plot3(data(find(idx==4),1),data(find(idx==4),2),data(find(idx==4),3),'m.')
plot3(data(find(idx==5),1),data(find(idx==5),2),data(find(idx==5),3),'c.')
plot3(data(find(idx==6),1),data(find(idx==6),2),data(find(idx==6),3),'k.')
hold off
axis([ -20000 40000 -200 200 0 100])
drawnow
Y=tsne(allBoutKinematicParameters(1:10:end,:));
figure
figure
plot(Y(find(idx==1),1),Y(find(idx==1),2),'r.')
hold on
plot(Y(find(idx==2),1),Y(find(idx==2),2),'g.')
plot(Y(find(idx==3),1),Y(find(idx==3),2),'b.')
plot(Y(find(idx==4),1),Y(find(idx==4),2),'m.')
plot(Y(find(idx==5),1),Y(find(idx==5),2),'c.')
plot(Y(find(idx==6),1),Y(find(idx==6),2),'k.')
hold off
drawnow


plot(Y(:,1),Y(:,2),'.')

figure

plot3(allBoutKinematicParameters(find(idx==1),17),allBoutKinematicParameters(find(idx==1),11),allBoutKinematicParameters(find(idx==1),222),'r.')
hold on
plot3(allBoutKinematicParameters(find(idx==2),17),allBoutKinematicParameters(find(idx==2),11),allBoutKinematicParameters(find(idx==2),222),'g.')
plot3(allBoutKinematicParameters(find(idx==3),17),allBoutKinematicParameters(find(idx==3),11),allBoutKinematicParameters(find(idx==3),222),'b.')
plot3(allBoutKinematicParameters(find(idx==4),17),allBoutKinematicParameters(find(idx==4),11),allBoutKinematicParameters(find(idx==4),222),'m.')
plot3(allBoutKinematicParameters(find(idx==5),17),allBoutKinematicParameters(find(idx==5),11),allBoutKinematicParameters(find(idx==5),222),'c.')
plot3(allBoutKinematicParameters(find(idx==6),17),allBoutKinematicParameters(find(idx==6),11),allBoutKinematicParameters(find(idx==6),222),'k.')
hold off
axis([-20000 40000 -200 200 0 100])

figure
plot(Y(find(idx==1),1),Y(find(idx==1),2),'r.')
hold on
plot(Y(find(idx==2),1),Y(find(idx==2),2),'g.')
plot(Y(find(idx==3),1),Y(find(idx==3),2),'b.')
plot(Y(find(idx==4),1),Y(find(idx==4),2),'m.')
plot(Y(find(idx==5),1),Y(find(idx==6),2),'c.')
plot(Y(find(idx==6),1),Y(find(idx==7),2),'k.')

hold off
figure
aaa(~isfinite(aaa))=0;
bursts=find((aaa(:,7)<100)&(aaa(:,7)>50)&(aaa(:,222)<100)&(aaa(:,222)>50));
slow=find((aaa(:,11)<30)&(aaa(:,11)>-30)&(aaa(:,17)<10000)&(aaa(:,17)>0));
for n=1:25
    subplot(5,5,n)
   fpam=randi(240);
   spam=randi(240);
   plot(aaa(:,fpam), aaa(:,spam),'.')
   hold on
   plot(aaa(bursts,fpam), aaa(bursts,spam),'r.')
   plot(aaa(slow,fpam), aaa(slow,spam),'g.')
   hold off 
   axis([prctile(aaa(:,fpam),0.1) prctile(aaa(:,fpam),99.9)+0.1 prctile(aaa(:,spam),0.1) prctile(aaa(:,spam),99.9)+0.1 ])
   title(strcat(num2str(fpam),'xxx',num2str(spam)));
end

