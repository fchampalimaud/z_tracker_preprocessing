clear all
datafolder{1}='F:\Pedro_WallE\VirtualOpenLoop_OMR';
datafolder{2}='F:\Pedro_WallE\atlas'
datafolder{3}='F:\Pedro_WallE\c3po'
savefolder='F:\Pedro_WallE\poolallrigs';
% alldatasets=dir(fullfile(startFolder,'Giant*'));


for whichday=[4 5 6 7 8 10] 
    whichday
    allrigsBoutKinematicParameters=[];
    allrigsBoutLengths=[];
    allrigsInterBoutLengths=[];
    for whichrig=1:length(datafolder)
    datafilepath=fullfile(datafolder{whichrig},strcat('giantpooled',num2str(whichday),'.mat'))
    
    load(datafilepath)
  
    allrigsBoutKinematicParameters=[allrigsBoutKinematicParameters ; pooledBoutKinematicParameters];
    allrigsBoutLengths=[allrigsBoutLengths ; pooledBoutLengths];
    allrigsInterBoutLengths=[allrigsInterBoutLengths ; pooledInterBoutLengths];
    end 
     pooledBoutKinematicParameters=allrigsBoutKinematicParameters;
     pooledBoutLengths=allrigsBoutLengths;
     pooledInterBoutLengths=allrigsInterBoutLengths;
    save(fullfile(savefolder,strcat('xxxgiantpooled',num2str(whichday),'.mat')),'pooledBoutKinematicParameters','pooledBoutLengths','pooledInterBoutLengths')
end


for whichday=[4 5 6  8 10 12 14] 
    whichday
    allrigsBoutKinematicParameters=[];
    allrigsBoutLengths=[];
    allrigsInterBoutLengths=[];
for whichrig=1:length(datafolder)
%     whichrig
    datafilepath=fullfile(datafolder{whichrig},strcat('tupooled',num2str(whichday),'.mat'))
    
    load(datafilepath)
    allrigsBoutKinematicParameters=[allrigsBoutKinematicParameters ; pooledBoutKinematicParameters];
    allrigsBoutLengths=[allrigsBoutLengths ; pooledBoutLengths];
    allrigsInterBoutLengths=[allrigsInterBoutLengths ; pooledInterBoutLengths];
    size(allrigsBoutKinematicParameters)
    end 
     pooledBoutKinematicParameters=allrigsBoutKinematicParameters;
     pooledBoutLengths=allrigsBoutLengths;
     pooledInterBoutLengths=allrigsInterBoutLengths;
    save(fullfile(savefolder,strcat('xxxtupooled',num2str(whichday),'.mat')),'pooledBoutKinematicParameters','pooledBoutLengths','pooledInterBoutLengths')
end

