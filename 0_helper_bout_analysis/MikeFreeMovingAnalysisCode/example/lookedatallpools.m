close all

CTRS{1}=-180:4:180;
CTRS{2}=-40000:1000:80000;

 hh=figure
 ii=figure
 jj=figure
 days=[4 5 6  7 8 10];
 for n=1:6
     thisfilename=fullfile('F:\Pedro_WallE\poolallrigs',strcat('xxxgiantpooled',num2str(days(n)),'.mat'));
     load (thisfilename)
     nbouts(n)=size(pooledBoutKinematicParameters,1);
figure(hh)
subplot(2,3,n)
 N=hist3(pooledBoutKinematicParameters(:,[11 17]),CTRS);
 imagesc(log(N))
 
 [lognums,logbins]=hist(log(1:30000),2:.1:10)
 figure(ii)
 [nums,bins]=hist(log(pooledBoutLengths),2:.1:10)
 nums=nums./lognums;
 plot(bins,nums/sum(nums),'Color',[1 0 0]*n/6+[0 1 0]*(6-n)/6) 
 hold on

 [nums,bins]=hist(log(pooledInterBoutLengths),2:.1:10)
 nums=nums./lognums;
plot(bins,nums/sum(nums),'Color',[0 1 1]*n/6+[1 0 1]*(6-n)/6) 

 figure(jj)
 [nums,bins]=hist(pooledBoutLengths,1:5:1000)
 plot(bins(1:end-1),nums(1:end-1)/sum(nums),'Color',[1 0 0]*n/6+[0 1 0]*(6-n)/6) 
 hold on

 [nums,bins]=hist(pooledInterBoutLengths,1:5:1000)
plot(bins(1:end-1),nums(1:end-1)/sum(nums),'Color',[0 1 1]*n/6+[1 0 0]*(6-n)/6) 
 end
 
      nbouts
      
% figure(hh)
% figure(ii)
% figure(jj)
      
% CTRS{1}=-180:4:180;
% CTRS{2}=-40000:1000:80000;

 hh=figure
 ii=figure
 jj=figure
 days=[4 5 6 8 10 12 14];
 for n=1:7
     thisfilename=fullfile('F:\Pedro_WallE\poolallrigs',strcat('xxxtupooled',num2str(days(n)),'.mat'));
     load (thisfilename)
     nbouts(n)=size(pooledBoutKinematicParameters,1);
     if (~isempty(pooledBoutKinematicParameters))
figure(hh)
subplot(2,4,n)
 N=hist3(pooledBoutKinematicParameters(:,[11 17]),CTRS);
 imagesc(log(N))
 
 [lognums,logbins]=hist(log(1:30000),2:.1:10)
 
 figure(ii)
 [nums,bins]=hist(log(pooledBoutLengths),2:.1:10)
 nums=nums./lognums;
 plot(bins,nums/sum(nums),'Color',[1 0 0]*n/7+[0 1 0]*(7-n)/7) 
 hold on

 [nums,bins]=hist(log(pooledInterBoutLengths),2:.1:10)
 nums=nums./lognums;
plot(bins,nums/sum(nums),'Color',[0 1 1]*n/7+[1 0 1]*(7-n)/7) 

 figure(jj)
 [nums,bins]=hist(pooledBoutLengths,1:5:1000)
 plot(bins(1:end-1),nums(1:end-1)/sum(nums),'Color',[1 0 0]*n/7+[0 1 0]*(7-n)/7) 
 hold on

 [nums,bins]=hist(pooledInterBoutLengths,1:5:1000)
plot(bins(1:end-1),nums(1:end-1)/sum(nums),'Color',[0 1 1]*n/7+[1 0 0]*(7-n)/7) 
     end
 end
      nbouts