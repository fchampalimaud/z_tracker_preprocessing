startFolder='Z:\SmallCircularOMR\SmallCircularOMRv2\Data';
saveFolder='Z:\SmallCircularOMR\SmallCircularOMRv2\SummaryImages';
mkdir(saveFolder)
% startFolder='F:\Pedro_WallE\VirtualOpenLoop_OMR'
alldatasets=dir(fullfile(startFolder,'Small*'));

for whichdataset=1:length(alldatasets)
    whichdataset
   fishfolder=fullfile(startFolder,alldatasets(whichdataset).name,'images');
        	thisdatafile=dir(fullfile(fishfolder,'*.png'));
           for n=1:length(thisdatafile)
              if (n==1)
                 thisimage=imread(fullfile(fishfolder,thisdatafile(n).name)); 
              else
                 thisimage=min(thisimage,imread(fullfile(fishfolder,thisdatafile(n).name))); 
              end
           end
           imwrite(thisimage,fullfile(saveFolder,strcat(alldatasets(whichdataset).name,'.png')),'PNG')
        end
    