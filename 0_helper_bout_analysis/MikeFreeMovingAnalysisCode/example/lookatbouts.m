figure
xdiffbout=fishXposition(allboutends)-fishXposition(allboutstarts);
ydiffbout=fishYposition(allboutends)-fishYposition(allboutstarts);
xdiffinterbout=fishXposition(allboutstarts(2:end))-fishXposition(allboutends(1:end-1));
ydiffinterbout=fishYposition(allboutstarts(2:end))-fishYposition(allboutends(1:end-1));


plot(xdiffbout,ydiffbout,'b.')

hold on

plot(xdiffinterbout,ydiffinterbout,'r.')

interboutdist=sqrt(power(xdiffinterbout,2)+power(ydiffinterbout,2));
[y,i]=sort(interboutdist);
figure
for n=0:50
hold on
plot(smoothedCumsumInterpFixedSegmentAngles(allboutends(i(end-n)):allboutstarts(i(end-n)+1))+n/2)
end
figure
for n=0:50
hold on
plot(smoothedCumsumInterpFixedSegmentAngles(allboutstarts(i(end-n)):allboutends(i(end-n)))+n/2)
end

figure
for n=1:floor(length(smootherTailCurveMeasure)/5000)
    
hold off
 plot([1:5000]+5000*(n-1),smoothedCumsumInterpFixedSegmentAngles([1:5000]+5000*(n-1),8))
hold on
usefulbouts=find(allboutstarts>5000*(n-1)+1,1,'first'):find(allboutends<5000*(n-1)+5000,1,'last')
for m=usefulbouts
line([allboutstarts(m) allboutstarts(m)],[-1 1],'Color',[1 0 0])
line([allboutends(m) allboutends(m)],[-1 1],'Color',[0 1 0])
line([indRealEnds(m) indRealEnds(m)],[-1 1],'Color',[0 1 1])
end
drawnow;
pause
end