
datafolder{1}='F:\Pedro_WallE\VirtualOpenLoop_OMR';
datafolder{2}='F:\Pedro_WallE\atlas'
datafolder{3}='F:\Pedro_WallE\c3po'
figure
for whichrig=1:length(datafolder)
alldatasets=dir(fullfile(datafolder{whichrig},'Giant*'));

count=0;
for whichday=[4 5 6 7 8 10] 
    count=count+1;
    whichday
    pooledBoutKinematicParameters=[];
    pooledBoutLengths=[];
    pooledInterBoutLengths=[];
    pooledPCAScores=[];
    for whichdataset=1:2
        
% for whichdataset=3:length(alldatasets)
    thisdataset=fullfile(datafolder{whichrig},alldatasets(whichdataset).name)
%     alldays=dir(fullfile(thisdataset,'*dpf'));
%     for whichday=1:length(alldays)
    thisday=fullfile(thisdataset,strcat(num2str(whichday),'dpf'));
    if (exist(fullfile(thisday,'pooledX1.mat')))
    load(fullfile(thisday,'pooledX1.mat'))
  
  size(allBoutKinematicParameters)
%     size(allInterBoutLengths)
%     size(allBoutLengths)
    pooledBoutKinematicParameters=[pooledBoutKinematicParameters ; allBoutKinematicParameters];
    pooledBoutLengths=[pooledBoutLengths ; allBoutLengths];
    pooledInterBoutLengths=[pooledInterBoutLengths ; allInterBoutLengths];
    
    pooledPCAScores=[pooledPCAScores;allPCAScores];
    end 
    end
    subplot(2,3,count)
        plot(pooledPCAScores(:,1),pooledPCAScores(:,2),'.')
        hold on
    save(fullfile(datafolder{whichrig},strcat('giantpooled',num2str(whichday),'.mat')),'pooledBoutKinematicParameters','pooledBoutLengths','pooledInterBoutLengths')
end

alldatasets=dir(fullfile(datafolder{whichrig},'Tu*'));


for whichday=[4 5 6  8 10 12 14] 
    whichday
    pooledBoutKinematicParameters=[];
    pooledBoutLengths=[];
    pooledInterBoutLengths=[];
    for whichdataset=1:2
        
% for whichdataset=3:length(alldatasets)
    thisdataset=fullfile(datafolder{whichrig},alldatasets(whichdataset).name)
%     alldays=dir(fullfile(thisdataset,'*dpf'));
%     for whichday=1:length(alldays)
    thisday=fullfile(thisdataset,strcat(num2str(whichday),'dpf'));
    load(fullfile(thisday,'pooledX1.mat'))
  
  size(allBoutKinematicParameters)
    size(allInterBoutLengths)
    size(allBoutLengths)
    pooledBoutKinematicParameters=[pooledBoutKinematicParameters ; allBoutKinematicParameters];
    pooledBoutLengths=[pooledBoutLengths ; allBoutLengths];
    pooledInterBoutLengths=[pooledInterBoutLengths ; allInterBoutLengths];
            
        end
    save(fullfile(datafolder{whichrig},strcat('tupooled',num2str(whichday),'.mat')),'pooledBoutKinematicParameters','pooledBoutLengths','pooledInterBoutLengths')
end

end