startFolder='F:\Pedro_WallE\VirtualOpenLoop_OMR';
startFolder='M:\OMR_Ontogeny_VOL\c3po';
alldatasets=dir(fullfile(startFolder));

for whichdataset=3:length(alldatasets)
    thisdataset=fullfile(startFolder,alldatasets(whichdataset).name);
    alldays=dir(fullfile(thisdataset,'*dpf'));
    for whichday=1:length(alldays)
    thisday=fullfile(thisdataset,alldays(whichday).name);
    allfish=dir(fullfile(thisday,'P*'));
    allBoutKinematicParameters=[];
    allBoutLengths=[];
    allInterBoutLengths=[];
        for whichfish=1:length(allfish)
            fishfolder=fullfile(thisday,allfish(whichfish).name)
        	thisdatafile=dir(fullfile(fishfolder,'*000.txt'));
            if (length(thisdatafile)>0)
           
                  dataFolder=fishfolder;
            saveFolder=fullfile(dataFolder,'matfiles');
            dataflag=false;
%             if (exist(fullfile(saveFolder,'tempBout.mat')))
%             load(fullfile(saveFolder,'tempBout.mat'));
%             dataflag=true;
%             end
            
            if (exist(fullfile(saveFolder,'tempBout3.mat')))
            load(fullfile(saveFolder,'tempBout3.mat'));
            dataflag=true;
            end
            if (dataflag)
                allboutstarts(find(rejectedBouts))=[];
                allboutends(find(rejectedBouts))=[];
                
           boutLengths=allboutends-allboutstarts;
           interboutLengths=allboutstarts(2:end)-allboutends(1:end-1);
    allBoutKinematicParameters=[allBoutKinematicParameters ; BoutKinematicParameters];
    allBoutLengths=[allBoutLengths ; boutLengths'];
    allInterBoutLengths=[allInterBoutLengths ; interboutLengths'];
    
            end
            end
        end
    save(fullfile(thisday,'pooled.mat'),'allBoutKinematicParameters','allBoutLengths','allInterBoutLengths')
    end
end