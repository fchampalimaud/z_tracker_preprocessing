clear all
figure
% datafolder{1}='M:\OMR_Ontogeny_VOL\c3po'
datafolder{1}='M:\OMR_Ontogeny_VOL\atlas'
datafolder{2}='M:\OMR_Ontogeny_VOL\c3po'
savefolder{1}='F:\Pedro_WallE\atlas'
savefolder{2}='F:\Pedro_WallE\c3po'
count=1;
% for whichrig=1:length(datafolder)
for whichrig=1:2
    
alldatasets1=dir(fullfile(datafolder{whichrig},'Giant*'));
alldatasets2=dir(fullfile(datafolder{whichrig},'Tu*'));
alldatasets=[alldatasets1; alldatasets2]

for whichdataset=1:length(alldatasets)
    thisdataset=fullfile(datafolder{whichrig},alldatasets(whichdataset).name);
    savedataset=fullfile(savefolder{whichrig},alldatasets(whichdataset).name);
    alldays=dir(fullfile(thisdataset,'*dpf'));
    if (~isempty(alldays))
    mkdir(savedataset)
        for whichday=1:length(alldays)
        thisday=fullfile(thisdataset,alldays(whichday).name);
        saveday=fullfile(savedataset,alldays(whichday).name);
        mkdir(saveday)
        allfish=dir(fullfile(thisday,'*P*'));
  
            for whichfish=1:length(allfish)
                fishfolder=fullfile(thisday,allfish(whichfish).name)
                savefish=fullfile(saveday,allfish(whichfish).name)
                mkdir(savefish)
                copyfile(fishfolder,savefish)
                thisdatafile=dir(fullfile(savefish,'*000.txt'));
                if ~isempty(thisdatafile)
    recycle('off')
    delete(fullfile(savefish,thisdatafile(1).name));
                end
                 thisdatafile=dir(fullfile(savefish,'*000.csv'));
                if ~isempty(thisdatafile)
    recycle('off')
    delete(fullfile(savefish,thisdatafile(1).name));
                end
                 thisdatafile=dir(fullfile(savefish,'*000.pickle'));
                if ~isempty(thisdatafile)
    recycle('off')
    delete(fullfile(savefish,thisdatafile(1).name));
                end
                 thisdatafile=dir(fullfile(savefish,'*000.mat'));
                if ~isempty(thisdatafile)
    recycle('off')
    delete(fullfile(savefish,thisdatafile(1).name));
                end
                 thisdatafile=dir(fullfile(savefish,'*000_MergedLog.pickle'));
                if ~isempty(thisdatafile)
    recycle('off')
    delete(fullfile(savefish,thisdatafile(1).name));
                end
            end
%     save(fullfile(thisday,'pooled.mat'),'allBoutKinematicParameters','allBoutLengths','allInterBoutLengths')
        end
    
    end
end
end

% clear all
% figure
% % datafolder{1}='M:\OMR_Ontogeny_VOL\c3po'
% datafolder{1}='F:\Pedro_WallE\VirtualOpenLoop_OMR';
% datafolder{2}='M:\OMR_Ontogeny_VOL\atlas'
% datafolder{3}='M:\OMR_Ontogeny_VOL\c3po'
% count=1;
% % for whichrig=1:length(datafolder)
% for whichrig=1
%     
% alldatasets1=dir(fullfile(datafolder{whichrig},'Giant*'));
% alldatasets2=dir(fullfile(datafolder{whichrig},'Tu*'));
% alldatasets=[alldatasets1; alldatasets2]
% 
% for whichdataset=1:length(alldatasets)
%     thisdataset=fullfile(datafolder{whichrig},alldatasets(whichdataset).name);
%     alldays=dir(fullfile(thisdataset,'*dpf'));
%     for whichday=1:length(alldays)
%     thisday=fullfile(thisdataset,alldays(whichday).name);
%     allfish=dir(fullfile(thisday,'*P*'));
% %     allBoutKinematicParameters=[];
% %     allBoutLengths=[];
% %     allInterBoutLengths=[];
%         for whichfish=1:length(allfish)
%             fishfolder=fullfile(thisday,allfish(whichfish).name)
%         	thisdatafile=dir(fullfile(fishfolder,'*000.txt'));
%            
%             if ~isempty(thisdatafile)
% recycle('off')
% delete(fullfile(fishfolder,thisdatafile(1).name));
%             end
%         end
% %     save(fullfile(thisday,'pooled.mat'),'allBoutKinematicParameters','allBoutLengths','allInterBoutLengths')
%     end
% end
% end