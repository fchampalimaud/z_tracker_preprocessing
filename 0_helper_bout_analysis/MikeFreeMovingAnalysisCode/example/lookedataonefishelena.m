close all
allBoutLengths=allboutends-allboutstarts;
allInterBoutLengths=allboutstarts(2:end)-allboutends(1:end-1);
CTRS{1}=-360:2:360;
CTRS{2}=-40000:1000:80000;

 hh=figure
 ii=figure
 jj=figure
 kk=figure
 
 figure(hh)
 N=hist3(BoutKinematicParameters(:,[11 17]),CTRS);
 imagesc(log(N))
 
 CTRS2{1}=0:300
 CTRS2{2}=0:100
 
 figure(kk)
 N=hist3(BoutKinematicParameters(:,[7 222]),CTRS2);
 imagesc(log(N))
 
 [lognums,logbins]=hist(log(1:30000),2:.1:10)
 figure(ii)
 [nums,bins]=hist(log(allBoutLengths),2:.1:10)
%  nums=nums./lognums;
 plot(bins,nums/sum(nums),'Color',[1 0 0]) 
 hold on

 [nums,bins]=hist(log(allInterBoutLengths),2:.1:10)
%  nums=nums./lognums;
plot(bins,nums/sum(nums),'Color',[0 1 1]) 

 figure(jj)
 [nums,bins]=hist(allBoutLengths,1:5:1000)
 plot(bins(1:end-1),nums(1:end-1)/sum(nums),'Color',[1 0 0]) 
 hold on

 [nums,bins]=hist(allInterBoutLengths,1:5:1000)
plot(bins(1:end-1),nums(1:end-1)/sum(nums),'Color',[0 1 1]) 

%  
%  for n=1:240
%  hh=figure
%  CTRS{1}=-360:2:360;
%  maxb=prctile(allBoutKinematicParameters(isfinite(allBoutKinematicParameters(:,n)),n),99);
%  minb=prctile(allBoutKinematicParameters(isfinite(allBoutKinematicParameters(:,n)),n),1);
%  if (minb~=maxb)
% CTRS{2}=minb:(maxb-minb)/200:maxb
%  N=hist3(allBoutKinematicParameters(:,[11 n]),CTRS);
%  imagesc(log(N))
%  title(num2str(n))
%  drawnow
%  pause
%  close(hh)
%  end
%  end
%  