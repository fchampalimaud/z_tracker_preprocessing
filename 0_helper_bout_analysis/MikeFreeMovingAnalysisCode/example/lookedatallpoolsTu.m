
CTRS{1}=-200:4:200;
CTRS{2}=-40000:500:80000;

 hh=figure
 ii=figure
 days=[4 5 6  8 10 12 14];
 for n=1:7
     thisfilename=fullfile('F:\Pedro_WallE\VirtualOpenLoop_OMR',strcat('tupooled',num2str(days(n)),'.mat'));
     load (thisfilename)
     
figure(hh)
subplot(2,4,n)
 N=hist3(pooledBoutKinematicParameters(:,[11 17]),CTRS);
 imagesc(log(N))
 
 figure(ii)
 [nums,bins]=hist(log(pooledBoutLengths),2:.1:10)
 plot(bins,nums/sum(nums),'Color',[1 0 0]*n/7) 
 hold on

 [nums,bins]=hist(log(pooledInterBoutLengths),2:.1:10)
plot(bins,nums/sum(nums),'Color',[0 1 1]*n/7) 
 end