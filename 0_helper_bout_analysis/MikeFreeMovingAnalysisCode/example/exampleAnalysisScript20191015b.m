startFolder='F:\Pedro_WallE\c3po';
% startFolder='F:\Pedro_WallE\VirtualOpenLoop_OMR'
alldatasets=dir(fullfile(startFolder));

for whichdataset=3:length(alldatasets)
    thisdataset=fullfile(startFolder,alldatasets(whichdataset).name);
    alldays=dir(fullfile(thisdataset,'*dpf'));
    for whichday=1:length(alldays)
    thisday=fullfile(thisdataset,alldays(whichday).name);
    allfish=dir(fullfile(thisday,'P*'));
        for whichfish=1:length(allfish)
            fishfolder=fullfile(thisday,allfish(whichfish).name)
        	thisdatafile=dir(fullfile(fishfolder,'matfiles','*000MAIN.mat'));
            if (length(thisdatafile)>0)
            tokens=split(thisdatafile(1).name,'_');
            pixelSize=str2num(tokens{end-2})/1000;
            dataFolder=fishfolder;

            dataFile=thisdatafile(1).name;

            saveFolder=fullfile(dataFolder,'matfiles');

            testfile=dir(fullfile(saveFolder,'*MAIN.mat'));
            if (isempty(testfile))
            convertPedroData2Mat20190925(dataFolder,dataFile,saveFolder);
            end
            boutMapFolder='F:\FreelyMovingBehaviorAnalysis_16\assignBoutTypesUsingMap\BoutMap';
            
            testfile=dir(fullfile(saveFolder,'*BoutX2.mat'));
            if (isempty(testfile))
        MikeKinAnalysisAndBoutCat20190925v7(boutMapFolder,saveFolder,dataFile(1:end-8),pixelSize);
            end
            end
        end
    
    end
end