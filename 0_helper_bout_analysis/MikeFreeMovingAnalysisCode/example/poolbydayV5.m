
datafolder{1}='F:\Pedro_WallE\VirtualOpenLoop_OMR'
datafolder{2}='F:\Pedro_WallE\c3po'
datafolder{3}='F:\Pedro_WallE\atlas'

for whichRig=1:length(datafolder)

startFolder=datafolder{whichRig};
alldatasets=dir(fullfile(startFolder));

for whichdataset=3:length(alldatasets)
    thisdataset=fullfile(startFolder,alldatasets(whichdataset).name);
    alldays=dir(fullfile(thisdataset,'*dpf'));
    for whichday=1:length(alldays)
    thisday=fullfile(thisdataset,alldays(whichday).name);
    allfish=dir(fullfile(thisday,'P*'));
    allBoutKinematicParameters=[];
    allPCAScores=[];
    allBoutLengths=[];
    allInterBoutLengths=[];
    allDists=[];
    allBoutCats=[];
        for whichfish=1:length(allfish)
            fishfolder=fullfile(thisday,allfish(whichfish).name)
        	thisdatafile=dir(fullfile(fishfolder,'matfiles','*000MAIN.mat'));
            if (length(thisdatafile)>0)
           
                  dataFolder=fishfolder;
            saveFolder=fullfile(dataFolder,'matfiles');
            dataflag=false;
%             if (exist(fullfile(saveFolder,'tempBout.mat')))
%             load(fullfile(saveFolder,'tempBout.mat'));
%             dataflag=true;
%             end
            thisfile=dir(fullfile(saveFolder,'tempBoutX2.mat'));
            
            if (length(thisfile)>0)
            load(fullfile(saveFolder,thisfile(1).name));
            dataflag=true;
            end
            if (dataflag)
                allboutstarts(find(rejectedBouts))=[];
                allboutends(find(rejectedBouts))=[];
                
           boutLengths=allboutends-allboutstarts;
           interboutLengths=allboutstarts(2:end)-allboutends(1:end-1);
    allBoutKinematicParameters=[allBoutKinematicParameters ; BoutKinematicParameters];
    allBoutLengths=[allBoutLengths ; boutLengths'];
    allInterBoutLengths=[allInterBoutLengths ; interboutLengths'];
    allPCAScores=[allPCAScores; newScore];
    allDists=[allDists; distToCenter'];
    allBoutCats=[allBoutCats; boutCat'];
    
            end
            end
        end
        if (~isempty(allPCAScores))
        figure
for n=1:13
plot(allPCAScores(find(allBoutCats==n),1),allPCAScores(find(allBoutCats==n),2),'.')
hold on
end
        end
    save(fullfile(thisday,'pooledX2.mat'),'allBoutKinematicParameters','allBoutLengths','allInterBoutLengths','allPCAScores','allDists','allBoutCats')
    end
end
end