% close all

CTRS{1}=-180:2:180;
CTRS{2}=-2000:100:10000;

 hh=figure
 ii=figure
 jj=figure
 kk=figure
ll=figure
 
 figure(hh)
 N=hist3(allBoutKinematicParameters(:,[11 19]),CTRS);
 imagesc(log(N))
 
 
 CTRS3{1}=0:100
 CTRS3{2}=-40000:1000:80000
 
 figure(ll)
 N=hist3(allBoutKinematicParameters(:,[7 213]),CTRS3);
 imagesc(log(N))
 
 CTRS2{1}=0:100
 CTRS2{2}=0:100
 
 figure(kk)
 N=hist3(allBoutKinematicParameters(:,[7 222]),CTRS2);
 imagesc(log(N))
 
 [lognums,logbins]=hist(log(1:30000),2:.1:10);
 figure(ii)
 [nums,bins]=hist(log(allBoutLengths),2:.1:10);
%  nums=nums./lognums;
 plot(bins,nums/sum(nums),'Color',[1 0 0]) 
 hold on

 [nums,bins]=hist(log(allInterBoutLengths),2:.1:10);
%  nums=nums./lognums;
plot(bins,nums/sum(nums),'Color',[0 1 1]) 

 figure(jj)
 [nums,bins]=hist(allBoutLengths,1:5:1000);
 plot(bins(1:end-1),nums(1:end-1)/sum(nums),'Color',[1 0 0]) 
 hold on

 [nums,bins]=hist(allInterBoutLengths,1:5:1000);
plot(bins(1:end-1),nums(1:end-1)/sum(nums),'Color',[0 1 1]) 

 
 