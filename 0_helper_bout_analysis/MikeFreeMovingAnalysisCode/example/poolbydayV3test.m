
datafolder{1}='F:\Pedro_WallE\VirtualOpenLoop_OMR'
% datafolder{2}='F:\Pedro_WallE\c3po'
% datafolder{3}='F:\Pedro_WallE\atlas'
figure

for whichRig=1:length(datafolder)

startFolder=datafolder{whichRig};
alldatasets=dir(fullfile(startFolder));

for whichdataset=5:length(alldatasets)
    thisdataset=fullfile(startFolder,alldatasets(whichdataset).name);
%     alldays=dir(fullfile(thisdataset,'*dpf'));
    for whichday=1
    thisday=fullfile(thisdataset,'12dpf');
    allfish=dir(fullfile(thisday,'P*'));
    allBoutKinematicParameters=[];
    allPCAscores=[];
    allBoutLengths=[];
    allInterBoutLengths=[];
        for whichfish=1:length(allfish)
            fishfolder=fullfile(thisday,allfish(whichfish).name)
        	thisdatafile=dir(fullfile(fishfolder,'matfiles','*000MAIN.mat'));
            if (length(thisdatafile)>0)
           
                  dataFolder=fishfolder;
            saveFolder=fullfile(dataFolder,'matfiles');
            dataflag=false;
%             if (exist(fullfile(saveFolder,'tempBout.mat')))
%             load(fullfile(saveFolder,'tempBout.mat'));
%             dataflag=true;
%             end
            thisfile=dir(fullfile(saveFolder,'tempBoutX1.mat'));
            
            if (length(thisfile)>0)
            load(fullfile(saveFolder,thisfile(1).name));
            dataflag=true;
            
            end
            if (dataflag)
                allboutstarts(find(rejectedBouts))=[];
                allboutends(find(rejectedBouts))=[];
                
           boutLengths=allboutends-allboutstarts;
           interboutLengths=allboutstarts(2:end)-allboutends(1:end-1);
           [nums,bins]=hist(interboutLengths,1:5:1000)
           figure
                plot(bins(1:199),nums(1:199)/sum(nums))
                hold on
                title(saveFolder)
           drawnow
    allBoutKinematicParameters=[allBoutKinematicParameters ; BoutKinematicParameters];
    allBoutLengths=[allBoutLengths ; boutLengths'];
    allInterBoutLengths=[allInterBoutLengths ; interboutLengths'];
    
            end
            end
        end
%     save(fullfile(thisday,'pooled.mat'),'allBoutKinematicParameters','allBoutLengths','allInterBoutLengths')
    end
end
end