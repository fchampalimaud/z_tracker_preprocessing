

%close all


function [objPixelReal] = ObjDetector_20190926(stats,cumsumInterpolatedAnglesGray)

areaStats = regionprops(stats, 'area');

objPixel = regionprops(stats, 'PixelList');

indRealObj = [areaStats.Area] > 3;


if ~isempty(indRealObj)%avoid case where all objects are too small

objPixelReal = objPixel(indRealObj);
% objOrientationReal = objOrientation(indRealObj);


%%
%%%%%%%%%%%%erase obj that are not long enouth%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


realObj = zeros(1,length(objPixelReal));
 for ii = 1 : length(objPixelReal)
     
     if max(objPixelReal(ii).PixelList(:,1)) == size(cumsumInterpolatedAnglesGray,2)%erase segments that don't exist in last segment
         
     realObj(ii) = length(unique(objPixelReal(ii).PixelList(:,1)));
     
     else 
         
     realObj(ii) = 0;%it will be detected as short and erased
     
     end
 end

indRealObj = realObj>=2;

if ~isempty(indRealObj)

objPixelReal = objPixelReal(indRealObj);
% objOrientationReal = objOrientationReal(indRealObj);
else

    objPixelReal = [];
    
    
end

else 
    
    objPixelReal = [];
    
end
