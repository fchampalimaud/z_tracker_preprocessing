
boutMapFilename = 'BoutMapCenters_kNN4_74Kins4dims_1.75Smooth_slow_3000_auto_4roc_merged11.mat';

boutMapFolder='F:\FreelyMovingBehaviorAnalysis_16\assignBoutTypesUsingMap\BoutMap';



%load 14 solution fish map
% filename2 = 'BoutMapCenters_kNN4_74Kins4dims_1.75Smooth_slow_3000_auto_4roc_withChasingDots.mat';


% load(fullfile(boutMapFolder, boutMapFilename),'behavioralSpaceStructure','finalClustering','indClosestBoutToTheCenter')
% 
%                     meanAllVar = behavioralSpaceStructure.meanAllVar;
%                     stdAllVar = behavioralSpaceStructure.stdAllVar;
%                     COEFF = behavioralSpaceStructure.COEFF;
%                     maxKinPars = behavioralSpaceStructure.maxKinPars;
%                     indKinPars = behavioralSpaceStructure.indKinPars;
%                     meanPCASpace = behavioralSpaceStructure.meanPCASpace;
% 
%                     numbOfK = finalClustering.numbOfK;
%                     distanceMethod = finalClustering.distanceMethod;
% 
%                     boutMapData = finalClustering.dataToDoMap;
% 
%                     mapAssignment = finalClustering.clusterAssignmentInMap;
% 
%                     numDim =  size(boutMapData,2);
%                     
%                     clear behavioralSpaceStructure finalClustering

rawDataFolder='F:\Pedro_WallE\VirtualOpenLoop_OMR\Tu\6dpf\P1\matfiles';
basename='OMR_Ontogeny_VOL_26_02_19_Tu_Tank3764_C06_06dpf_P1_59_50_walle000';
debugFile=matfile(fullfile(rawDataFolder,strcat(basename,'FLAGS_AND_DEBUG.mat')));
mainFile=matfile(fullfile(rawDataFolder,strcat(basename,'MAIN.mat')));
nTailPoints=size(tailAngles,2);

lagNumber=debugFile.alllagvals;     
tailAngles=mainFile.tailAngles;
tailValues=debugFile.tailValues;
bodyAngles=mainFile.bodyAngle;
xPos=mainFile.fishXposition;
yPos=mainFile.fishYposition;

indLag = find(lagNumber > 190);
lagVector = lagNumber*0;
lagVector(indLag) = 1;

[realBodyAngles,bodyAnglesVelocity] = FishAngleCalculatorFast(bodyAngles,3*pi/2);%result comes in rads
         
errorRealBodyAngles=realBodyAngles-sgolayfilt(realBodyAngles,2,7,[],1);
smoothRealBodyAngles=sgolayfilt(realBodyAngles,2,7,[],1);
         
%         figure
%         plot(errorRealBodyAngles*180/pi,'.')
    
[fixedSegmentAngles,errorListAllMitakes,errorListNotFixedMistakes] = FixTailTrackingGaps_3(tailAngles,10);

% trueTailAngles=tailAngles;
% trueTailAngles(find(tailAngles<-10))=0;
% trueTailCumAngles=cumsum(trueTailAngles')';
% fixedTailCumAngles=cumsum(fixedSegmentAngles')';
% hold off
% plot(trueTailCumAngles(:,8),'r')
% hold on
% plot(fixedTailCumAngles(:,8),'b')
% plot(errorListAllMitakes(:,8)+5)

[fixedSegmentAngles, errorListObends,totalNumberFrames] = FixObendGaps_2(fixedSegmentAngles,tailValues);

% 
% fixedTailCumAngles=cumsum(fixedSegmentAngles')';
% fixedTailCumAngles2=cumsum(fixedSegmentAngles2')';
% hold off
% plot(fixedTailCumAngles(:,8),'r')
% hold on
% plot(fixedTailCumAngles2(:,8),'b')
% plot(errorListObends+5)

        smoothdata=sgolayfilt(fixedSegmentAngles,2,5,[],1);
        residuals=abs(cumsum(smoothdata')'-cumsum(fixedSegmentAngles')');
%         plot(cumsum(fixedSegmentAngles')','b')
        fixedSegmentAngles=smoothdata;
%         figure
%         plot(residuals(:,7)*180/pi)
     cumsumInterpFixedSegmentAngles=cumsum(fixedSegmentAngles')';
     
smoothedCumsumInterpFixedSegmentAngles=conv2(cumsumInterpFixedSegmentAngles,ones(1,3)/3,'same');
smoothedCumsumInterpFixedSegmentAngles(:,1)=cumsumInterpFixedSegmentAngles(:,1);
smoothedCumsumInterpFixedSegmentAngles(:,end)=cumsumInterpFixedSegmentAngles(:,end);

interpFixedSegmentAngles = [zeros(nTailPoints,1)'; diff(smoothedCumsumInterpFixedSegmentAngles)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%Filter angles%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bcsize=10;
lsize=100;

filteredSegmentAngle=conv2(interpFixedSegmentAngles,boxcarf(bcsize),'same');

cumFilteredSegmentAngle = cumsum(filteredSegmentAngle')';

superCumSegAngle = cumsum(abs(cumFilteredSegmentAngle)')';

bcFilt=10;
seg2use=10;
tailCurveMeasure = conv(superCumSegAngle(:,seg2use),boxcarf(bcFilt),'same');

%max filter transforms max into plateau
smootherTailCurveMeasure = maxFilterFast(tailCurveMeasure,20) - minFilterFast(tailCurveMeasure,400);

[distanceNoFilter] = calculateDistance(xPos,yPos);
dfilt = 10;
filteredDistance = conv(distanceNoFilter,boxcarf(dfilt),'same'); 


        
figure
% for n=1:99
    threshold=prctile(smootherTailCurveMeasure,50)
    minimuminterboutinterval=30;
    minimumboutlength=30;
    startshift=14;
    endThresh=0.025;
[allboutstarts, allboutends, indRealEnds] = BoutDetectorCurvatureFunction20190926v1(smootherTailCurveMeasure,filteredDistance,threshold,minimuminterboutinterval,minimumboutlength,startshift,endThresh);
numbouts(n)=length(allboutstarts);
% plot(numbouts)
% drawnow;
% end
            

            % % %test starts and ends of bouts
% startBout=smootherTailCurveMeasure(indStartBoutDetection:indEndBoutDetection)*0;
% startBout(allboutstarts)=1;
% endBout=smootherTailCurveMeasure(indStartBoutDetection:indEndBoutDetection)*0;
% endBout(allboutends)=1;
% realEndBout=smootherTailCurveMeasure(indStartBoutDetection:indEndBoutDetection)*0;
% realEndBout(indRealEnds)=1;
% 
% 
% figure
%  plot(cumsumInterpFixedSegmentAngles(indStartBoutDetection:indEndBoutDetection,1:8))
% hold on
% plot(smootherTailCurveMeasure(indStartBoutDetection:indEndBoutDetection), '.', 'color', 'black')
% hold on
% plot(startBout,'o','color', 'green','LineWidth',2)
% hold on
% plot(endBout, 'o', 'color', 'red','LineWidth',2) 
% hold on
% plot(realEndBout, 'o', 'color', 'magenta','LineWidth',2) 
% pause(0.1)
%     
    
    %     % %test bout detector -  it goes through each bout and plots: the bout, the
%     %detection curve and the begining and ending of bout.
%     
%     for zzzz = 1 : length(allboutstarts) 
% 
%         hold off
%         plot(cumsumInterpFixedSegmentAngles((allboutstarts(zzzz)-100) : (indRealEnds(zzzz) + 100),2:8));
%         hold on
%         plot(smootherTailCurveMeasure((allboutstarts(zzzz)-100) : (indRealEnds(zzzz) + 100)))
%         
%         boutStart = cumsumInterpFixedSegmentAngles((allboutstarts(zzzz)-100) : (indRealEnds(zzzz) + 100),1)*0;
%         boutEnd = cumsumInterpFixedSegmentAngles((allboutstarts(zzzz)-100) : (indRealEnds(zzzz) + 100),1)*0;
% 
%         boutStart(100) = 1;
%         boutEnd(end-100) =1; 
% 
%         plot(boutStart, 'go')
%         plot(boutEnd, 'ro')
%         pause
%         
%     end
        
BeatKinematicParameters = [];
BeatInf = [];

BoutKinematicParameters = [];
BoutInf = [];

if ~isempty(allboutstarts)%if stim block does not have bouts

    
    %%
    %%%%%%%%%%%%%%%%%% Determine bout stim %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    boutStim = stim(allboutstarts);
    
    boutStim2 = stim(allboutends);
    
    %put -100 in bouts that are in trasition of 2 stims
    indBoutsInStimTransition = find( boutStim ~= boutStim2);
    boutsInStimTransition = zeros(1, length(boutStim));
    boutsInStimTransition(indBoutsInStimTransition) = -100;
     
    
     
    
   
    %%
    %%%%%%%%%%%%%%%%%%%%% Determine bout order in collision%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    sequenceOfBoutsInNoCollision = zeros(1, length(allboutstarts));
    

    
    for y  = 1 : length(indNoCollisionStartAll )%loop through no collisions
    
        %get bouts that happened in this no collision
        indBoutsStartThisNoCollision = find(allboutstarts >= indNoCollisionStartAll(y) & allboutstarts <= indNoCollisionEndAll(y));
        
        if ~isempty(indBoutsStartThisNoCollision)%make sure there are bouts in no collision
        
        numberOfBoutInNoCollision = indBoutsStartThisNoCollision - indBoutsStartThisNoCollision(1) + 1;
        sequenceOfBoutsInNoCollision(indBoutsStartThisNoCollision) = numberOfBoutInNoCollision;    
            
        end%make sure there are bouts in no collision
        
    end%loop through no collisions
    

    
        
        
        
        

    
    
    %%
    %%%%%%%%%%%%%%%%%%%%% Determine bout order in square%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
    %!!! not implemented - probably not important
    
    
    
    
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% bout loop %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%loops through every detected bout and stores its basic information

%save start and end of bouts by beat detector
indRealBoutStartAll = zeros(1,length(allboutstarts));
indRealBoutEndAll = zeros(1,length(allboutstarts));
sequenceOfBoutsInStim = zeros(1,length(allboutstarts));
sequenceOfBoutsInStim2 = zeros(1,length(allboutstarts));
latencyOfBoutAfterStim = zeros(1,length(allboutstarts));
latencyOfBoutAfterStim2 = zeros(1,length(allboutstarts));
stimUniqueNumberBoutVector = zeros(1,length(allboutstarts));
stimUniqueNumberBoutVector2 = zeros(1,length(allboutstarts));

eyeConvAvr = zeros(1,length(allboutstarts));

currentStimInd = 1;
numberOfBoutInStim = 1;

% currentStimInd2 = 1;
% numberOfBoutInStim2 = 1;


for zzzz = 1 : length(allboutstarts)

%  zzzz  
   % !!!!!
   if mod(zzzz,100) == 0
       
      disp(strcat(num2str(zzzz), ' out of_',num2str(length(allboutstarts))));
      
   end
    
    
%%
    
%get this bout tail inf
cumsumAngleByBout = cumsumInterpFixedSegmentAngles(allboutstarts(zzzz) : indRealEnds(zzzz),1:lastMeasuredSegment);
    

   
    %%
        %%%%%%% Interpolate data 10 times for beat kin %%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %interpolate data in time domain
        
        %create x - frames in real data
        x = 1:1:length(cumsumAngleByBout);

        %create xi - parts of frames to interpolate
        xi = 1:0.1:length(cumsumAngleByBout);

        %interpolate tail data
        cumsumInterpolatedAngleByBout = zeros(length(xi), size(cumsumAngleByBout,2));

        for f = 1:size(cumsumAngleByBout,2)

    
        cumsumInterpolatedAngleByBout(:,f) = interp1(x,cumsumAngleByBout(:,f),xi, '*spline')';


        end

      
        %interpolate bodyAngle data        
        interpBodyAngles = interp1(x,realBodyAngles((allboutstarts(zzzz)) : (indRealEnds(zzzz))),xi,'*spline');% in rads 
        interpBoutBodyAngle = interp1(x,bodyAngles((allboutstarts(zzzz)) : (indRealEnds(zzzz))),xi,'*spline');% in rads

        %interpolate position data
        interpPosX = interp1(x,xPos((allboutstarts(zzzz)) : (indRealEnds(zzzz))),xi,'*spline');
        interpPosY = interp1(x,yPos((allboutstarts(zzzz)) : (indRealEnds(zzzz))),xi,'*spline');

        %interpolate smootherTailCurveMeasure
        interpSmootherTailCurveMeasure = interp1(x,smootherTailCurveMeasure((allboutstarts(zzzz)) : (indRealEnds(zzzz))),xi,'*spline');
        
        
        
        
        
        
 
        %%
        %%%%%%%%%%%Calculate Curvature parameters%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %!!!! probably not useful or wrong!!!
        [boutCurvature] = TailCurvatureCalculator_13(cumsumInterpolatedAngleByBout,tailMM);
         
 
    
%         %test tail curvature
%         plot(cumsumInterpolatedAngleByBout)
%         hold on
%         plot(boutCurvature, 'o', 'color','k')
        

        %%
        %%%%%%%%%%%%%%%%%%%%%%%%%Detect beats%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %this data comes interpolated 10 times in time to calcualte beat and bout kinematic parameters
        makeplot = 0;
%      %my half beat detector
%          [cumsum2DInterpolatedAngles,cumsumInterpolatedAnglesGray,indRealBoutStart,indRealBoutEnd,halfBeatStructure,numbBeats,halfBeatPosInt,halfBeatMag] = BeatDetector_25(cumsumInterpolatedAngleByBout,lastMeasuredSegment,makeplot);
%         
     %mike's half beat detector   
%        [cumsum2DInterpolatedAngles,cumsumInterpolatedAnglesGray,indRealBoutStart,indRealBoutEnd,halfBeatStructure,numbBeats,halfBeatPosInt,halfBeatMag] = BeatDetector_24Mike4(cumsumInterpolatedAngleByBout,lastMeasuredSegment,makeplot);
%    clf
 [cumsum2DInterpolatedAngles,cumsumInterpolatedAnglesGray,indRealBoutStart,indRealBoutEnd,halfBeatStructure,numbBeats,halfBeatPosInt,halfBeatMag]...
     = BeatDetector_26(cumsumInterpolatedAngleByBout,lastMeasuredSegment,makeplot);
 
%    clf
        
%      pause
        


        %%
        %%%%%%%%%%%%%%%%%%%% exclude 1st beats that are super small %%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         %not sure what is the best parameter to erase this beats
%         if halfBeatStructure(1).beatDuration < 50
%             
%             halfBeatStructure(1) = [];
%             numbBeats = numbBeats -1;
%         
%         end
        
        
        
        if ~isnan(indRealBoutStart) && ~isempty(indRealBoutStart)%case where there is no detected bout
            
       %%
       %%%%%%%%%%%%%%%%% get pos and mag of beats for this bout %%%%%%%%%%%
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       
       if length(halfBeatPosInt) <= 50
        
        %transform int pos into pos - raw data will not be int
        halfBeatPos = round(halfBeatPosInt/10);
        
        %create nan tag - to make variables 50 length in total
        nanTag = nan(1,(50 - length(halfBeatPosInt)));
        
        % concatenate nanTag with variables
        halfBeatPosWithNaNTag = [halfBeatPos nanTag];
        halfBeatMagWithNaNTag = [halfBeatMag nanTag];
        
        
        
       else
        
       %transform int pos into pos - raw data will not be int
       halfBeatPos = round(halfBeatPosInt/10);
        
       halfBeatPosWithNaNTag =  halfBeatPos(1:50); 
       halfBeatMagWithNaNTag = halfBeatMag(1:50);  
       
       end

        %%
        %%%%%%%%%%%%%%%%%% calculate correct starts and ends of bouts %%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
         %make vector with real start and ends of bout
        indRealBoutStartAll(zzzz) = allboutstarts(zzzz) + floor(indRealBoutStart/10);%need to round indRealBoutStart because is from interpolated data
        indRealBoutEndAll(zzzz) = allboutstarts(zzzz) + floor(indRealBoutEnd/10);
     
%         %test more acurate start of bout
%          hold on
%          plot(indRealBoutStartAll, 'color','green')
%          plot(allboutstarts, '.')
%          plot(indRealBoutStartAll, 'g.')
%          plot(indRealBoutEndAll, 'r.')
        
        
        %calculate index of bout end by distance
        indDistanceBoutEnd = size(cumsumInterpolatedAngleByBout,1);%end of bout determined by velocity - it is in pixels;
        
        
%         %test start and end of bouts
%         plot(cumsumInterpFixedSegmentAngles(:,1:lastMeasuredSegment), 'color', 'k')
%         hold on
%         %rought bout start
%         plot(allboutstarts,(zeros(1,length(allboutstarts))+1),'o','color',[0 100 0]/255, 'linewidth',3)
%         %accurate bout start
%         plot(indRealBoutStartAll,(zeros(1,length(indRealBoutStartAll))+1),'og', 'linewidth',3)
%         %rough bout end
%         plot(allboutends,(zeros(1,length(allboutends))+1),'or', 'linewidth',3)
%         %accurate bout end
%         plot(indRealBoutEndAll,(zeros(1,length(indRealBoutEndAll))+1),'om', 'linewidth',3)
%         %bout end by distance
%         plot(indRealEnds,(zeros(1,length(indRealEnds))+1),'o','color',[128 	0 	0]/255, 'linewidth',3)
        
        

%% interpolate body angles to avoid strange bug of bout max anglular velocity %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 %create x - frames in real data
        x = 1:1:length(realBodyAngles((indRealBoutStartAll(zzzz)) : (indRealBoutEndAll(zzzz))));

        %create xi - parts of frames to interpolate - 10 times
        xi = 1:0.1:length(realBodyAngles((indRealBoutStartAll(zzzz)) : (indRealBoutEndAll(zzzz))));


        interpBodyAngles2 = interp1(x,realBodyAngles((indRealBoutStartAll(zzzz)) : (indRealBoutEndAll(zzzz))),xi,'*spline');% in rads 
       
        
        %%
        %%%%%%%%%%%%%%%%%%%%% identify stim of this bout %%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %use accurate bout start to determine which stim bout belongs to
        
        [thisBoutStimInd,~,~] = findnearest(indRealBoutStartAll(zzzz),indStimStartAll,-1);
        
        
        %check if this bout is in previous stim
        if zzzz == 1%first bout of stim
            
            numberOfBoutInStim = 1;%is always 1
            
            %updade currentStimInd 
            currentStimInd = thisBoutStimInd;%case where 1st bout is not in 1st stim - currentStimInd should be updated
            
        else%bouts that are not the first
            
            if thisBoutStimInd == currentStimInd%case where it is the same as previous bout

                %calculate sequence of bouts in stim
                numberOfBoutInStim = numberOfBoutInStim + 1;%update bout in sequence


            else%if bout is different stimulus than the last bout

                %restart sequence of bouts if stim is new 
                numberOfBoutInStim = 1;%restart counting bouts

                %updade currentStimInd 
                currentStimInd = thisBoutStimInd;%updade currentStimInd

            end
        end
        
        %calculate latency of bout from stim
        latencyOfBoutAfterStim(zzzz) = (indRealBoutStartAll(zzzz) - indStimStartAll(currentStimInd))/0.7;%latency in ms
        
        
        %save variables
        sequenceOfBoutsInStim(zzzz) = numberOfBoutInStim;
        
        stimUniqueNumberBoutVector(zzzz) = stimUniqueNumber(currentStimInd);
        
        
        %%
%         %%%%%%%%%%%%%%%%%%%%% identify stim of this bout %%%%%%%%%%%%%%%%%%
%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         %use rough bout start to determine which stim bout belongs to -
%         %probably not used for anything
%         
%         [thisBoutStimInd2,~,~] = findnearest(allboutstarts(zzzz),indStimStartAll,-1);
%         
%         %check if this bout is in previous stim
%         if thisBoutStimInd2 == currentStimInd2
%             
%             %calculate sequence of bouts  in stim
%             numberOfBoutInStim2 = numberOfBoutInStim2 + 1;
%             
%             
%             
%         else
%             
%             %restart sequence of bouts if dtim is new 
%             numberOfBoutInStim2 = 1;
%             
%             %reset currentStimInd 
%             currentStimInd2 = thisBoutStimInd2;
%             
%         end
%         
%         latencyOfBoutAfterStim2(zzzz) = (allboutstarts(zzzz) - indStimStartAll(currentStimInd2))/0.7;%latency in ms
%         
%         
%         
% %         %check latencies
% %         if latencyOfBoutAfterStim(zzzz) < 20
% %            figure
% %         plot(cumsumInterpFixedSegmentAngles(:,1:lastMeasuredSegment), 'color', 'k')
% %         hold on
% %         %rought bout start
% %         plot(allboutstarts(zzzz),(zeros(1,length(allboutstarts(zzzz)))+1),'o','color',[0 100 0]/255, 'linewidth',3)
% %         %accurate bout start
% %         plot(indRealBoutStartAll(zzzz),(zeros(1,length(indRealBoutStartAll(zzzz)))+1),'og', 'linewidth',3)
% %         %rough bout end
% %         plot(allboutends(zzzz),(zeros(1,length(allboutends(zzzz)))+1),'or', 'linewidth',3)
% %         %accurate bout end
% %         plot(indRealBoutEndAll(zzzz),(zeros(1,length(indRealBoutEndAll(zzzz)))+1),'om', 'linewidth',3)
% %         %bout end by distance
% %         plot(indRealEnds(zzzz),(zeros(1,length(indRealEnds(zzzz)))+1),'o','color',[128 	0 	0]/255, 'linewidth',3)
% %         plot(indStimStartAll(currentStimInd), 1, 'ob','linewidth',3)
% %         title( latencyOfBoutAfterStim(zzzz))
% %         pause
% %         end
% %         
        
 %%
    %%%%%%%%%%%%%%determine bout previous stims%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
%      plot(cumsumAngleByBout)
%      hold on
%      plot(1:1:length(cumsumAngleByBout),)
    
    
    boutStim(zzzz) = stim(indRealBoutStartAll(zzzz));
    thisBoutStim = boutStim(zzzz);
    
    %cut boutStim array
    cutBoutStim = fliplr(boutStim(1:zzzz));
    [indBoutPreviousStim,~] = find(cutBoutStim ~= thisBoutStim, 1, 'first');
    boutPreviousStim = cutBoutStim(indBoutPreviousStim);

    %when there is no previous stim
    if isempty(boutPreviousStim)
        
        boutPreviousStim = NaN;
        
    end
    
    
        
%     %test
%     plot(boutStim, '.')
%     hold on
%     plot(zzzz,thisBoutStim, 'go') 


        %%
        %%%%%%%%%%%%%Extrapolate missing segments%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %!!!this kin parameters don't seem to be useful - don't know why 
        %if you want to use extrpolated data use BeatPropagationCalculatorExtrapolation_2.m, if not use BeatPropagationCalculator_2.m
        
%         %not extrapolating missing segments
         %[halfBeatExtrapolatedStructure,velocityBeatPropagationStructure] = BeatPropagationCalculator_2(halfBeatStructure,tailMM,cumsum2DInterpolatedAngles);
         
        %extrapolating missing segments
        [halfBeatExtrapolatedStructure,velocityBeatPropagationStructure] = halfBeatTailWaveExtrapolation_3(halfBeatStructure,tailMM,cumsum2DInterpolatedAngles);
 
        %%
        %%%%%%%%%%%%%%%%%%%%%%beat loop%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %to save inds of beats
        indBeatStartAll = zeros(1,length(halfBeatExtrapolatedStructure));
        indBeatEndAll = zeros(1,length(halfBeatExtrapolatedStructure));
        
        BeatKinematicParametersThisBout = zeros(length(halfBeatExtrapolatedStructure),50);
        
        BeatInfThisBout = zeros(length(halfBeatExtrapolatedStructure),32);
        
        
        for j = 1: length(halfBeatExtrapolatedStructure) %beat loop
%           j =2  
            %%%%%%%%%%%%get start and end of beat in bout%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            indBeatStart = halfBeatExtrapolatedStructure(j).indBeatStart;
            indBeatStartNotInt = round(indBeatStart/10);
            indBeatStartNotIntAllData = allboutstarts(zzzz) + indBeatStartNotInt;

            indBeatEnd = halfBeatExtrapolatedStructure(j).indBeatEnd;
            indBeatEndNotInt = round(indBeatEnd/10);
            indBeatEndNotIntAllData = allboutstarts(zzzz) + indBeatEndNotInt;
            
            %%avoid case where beat start ind is zero or negative
            if indBeatStartNotInt < 1
                
                indBeatStartNotInt = 1;
                
            end
            
            
            %save beat position to calculate bout frequency
            indBeatStartAll(j) = indBeatStartNotInt;
            indBeatEndAll(j) = indBeatEndNotInt;
            
            %%
%             %%%%%%%%%%%%%%%%%%%%%%%%%%%%plot each beat%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             %!!! broken don't know why
%             beatImage = cumsum2DInterpolatedAngles;
%             
%             beatImage(halfBeatExtrapolatedStructure(j).halfBeatLinearInd) = 1;
%             
%             
%             subplot(1,2,1)
%             imagesc(cumsum2DInterpolatedAngles)
%             colormap gray
%             
%             subplot(1,2,2)
%             imagesc(beatImage)
%             colormap gray
%             pause
        


%%
        %%%%%%%%%%%%%%%%%Calculate beat kin parameters%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        [BeatKinematicParametersThisBeat, ~,~] = BeatParametersCalculator_12(j,tailMM, halfBeatExtrapolatedStructure, ...
            velocityBeatPropagationStructure, interpBodyAngles, indRealBoutStart,  interpPosX, interpPosY, pixelSize,...
            interpSmootherTailCurveMeasure,boutCurvature);
        
        %%
        %%%%%%%% calculate max angular speed with less smoothing %%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        realBodyAnglesLessSmoothThis = realBodyAnglesLessSmooth(indRealBoutStartAll(zzzz):indRealBoutEndAll(zzzz));

       
        if j == 1 
            
        indStart = round(min([halfBeatExtrapolatedStructure.indBeatStart])/10)-1;

        
        end
        
        [BeatKinematicParametersThisBeat] = BeatMaxAngularSpeedCorr_1(realBodyAnglesLessSmoothThis,indStart,indBeatStartNotInt,indBeatEndNotInt,BeatKinematicParametersThisBeat);
     
%         disp('here')
%         pause
        %%
        %%%%%%%%%%%%%%%%%% concatenate beat kin parameters %%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        BeatKinematicParametersThisBout(j,:) = BeatKinematicParametersThisBeat;
       
        
%         pause
        %%
        %%%%%%%%%%%% make BeatInf array %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        [BeatInfThisBeat] = beatInfMaker_5(zzzz,xPos,yPos,dataSetNumber,protocolNumber,fishUniqueNumber,thisBoutStim, ...
            boutPreviousStim,pixelSize,headToBladderLength,tailSegmentLength,distanceEyesToBlob,lastMeasuredSegment,fishAge,boutUniqueNumber,...
            lagVector,allboutstarts,sequenceOfBoutsInStim,uniqueFileNumber,beatUniqueNumber,indBeatStartNotIntAllData,indBeatEndNotIntAllData,...
            indBeatStartNotInt,indBeatEndNotInt,indBeatStart,indBeatEnd,sequenceOfBoutsInNoCollision,latencyOfBoutAfterStim,...
            stimUniqueNumberBoutVector,realFishUniqueNumber,trueUniqueFishNumber,wellLetter);

        BeatInfThisBout(j,:) = BeatInfThisBeat;
        
        beatUniqueNumber = beatUniqueNumber + 1;
           
        end %beat loop

        BeatInf = [BeatInf' BeatInfThisBout']';
        BeatKinematicParameters = [BeatKinematicParameters'  BeatKinematicParametersThisBout']';
        
        %%
        %%%%%%%%%%%%%%%%%% find C1 and C2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [C1Angle,C1Duration,C1MaxAngularSpeed,C2Angle,C2Duration,C2MaxAngularSpeed] = C1C2Calculator_5(halfBeatStructure,interpBodyAngles);
        
       
        
        %%
        %%%%%%%%%%%%%%% Calcualte eye convergence mean by bout %%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

       
%         if max(find(isnan(convergengePar)))
        eyeConvAvrThisBout = nanmean(convergengePar(indRealBoutStartAll(zzzz):indRealBoutEndAll(zzzz)));
        
%         else
%             
%          eyeConvAvrThisBout = NaN;   
%         
%         end
        %%
        %%%%%%%%%%%%%%% calcualte eyeConvDiff %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        bouSpace = 20;
      
        
        if  eyesNotTrakedTag == 1
            
            [eyeConvDiffThisBout] = eyeConvDiffCalculator_1(convergengePar,bouSpace,indRealBoutStartAll(zzzz),indRealBoutEndAll(zzzz));
            
            [eyeConv20FramesBeforeBoutThisBout,eyeConv20FramesAfterBoutThisBout,eyeConvDiff2ThisBout] = eyeKinParCalculator_1...
                (convergengePar,bouSpace,indRealBoutStartAll(zzzz),indRealBoutEndAll(zzzz));

            
            
            
        else
            
            eyeConvDiffThisBout = nan;
            eyeConv20FramesBeforeBoutThisBout = nan;
            eyeConv20FramesAfterBoutThisBout = nan;
            eyeConvDiff2ThisBout = nan;
            
        end
        
        
       
        %%
        %%%%%%%%%%%%%%%%%Calculate bout kin parameters%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
%         pause
        
         [BoutKinematicParametersThisBout] = BoutParametersCalculator_44(BeatKinematicParametersThisBout,indRealBoutEnd,indRealBoutStart,...
            indDistanceBoutEnd,interpBodyAngles,interpBodyAngles2,interpPosX,interpPosY,pixelSize,cumsumInterpolatedAngleByBout,C1Angle,...
            C1Duration,C1MaxAngularSpeed,C2Angle,C2Duration,C2MaxAngularSpeed,eyeConvAvrThisBout,eyeConv20FramesBeforeBoutThisBout,...
            eyeConv20FramesAfterBoutThisBout,eyeConvDiff2ThisBout);

        
        %%
        %%%%%%%%%%%%%%%%%Calculate bout max angular speed less smothing %%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
        
        [BoutKinematicParametersThisBout] = BoutParametersCalculatorMaxAngularSpeed_1(BoutKinematicParametersThisBout,...
            BeatKinematicParametersThisBout,realBodyAnglesLessSmoothThis);

       
        
        
        BoutKinematicParameters = [BoutKinematicParameters' BoutKinematicParametersThisBout']';
       
        %%
        %%%%%%%%%%%%%%%%%%% find broken bouts %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
       boutMaxAngleRatioThisBout = BoutKinematicParametersThisBout(:,EnumeratorBoutKinPar.boutMaxAngleRatio);
       boutMaxAngularSpeedThisBout = BoutKinematicParametersThisBout(:,EnumeratorBoutKinPar.boutMaxAngularSpeed);
        
       if ~isnan(boutMaxAngleRatioThisBout) && ~isnan(boutMaxAngularSpeedThisBout)
       
           if boutMaxAngleRatioThisBout >= 33 || boutMaxAngularSpeedThisBout > 43
               
           brokenBouts = 1;
           
           else
                if boutMaxAngleRatioThisBout >= 0.5 && boutMaxAngularSpeedThisBout > 10
           
                    brokenBouts = 1;
           
                else
           
                    brokenBouts = 0;
           
                end
           end
       else 
           
           brokenBouts = 1;
           
       end
       
       %%
       %%%%%%%%%%%%%%%% find double bouts %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       minBoutFreqCorrThisBout = BoutKinematicParametersThisBout(:,EnumeratorBoutKinPar.minBoutFreqCorr);
       
       if ~isnan(minBoutFreqCorrThisBout)
           
           if minBoutFreqCorrThisBout < 15
               
               doubleBout = 1;
               
           else 
               
               doubleBout = 0;
               
           end
           
           
       else
           
           doubleBout = 1;
           
       end
       
       
       %%
       %%%%%%%%%%%%%%%% determine number of trail mistakes  %%%%%%%%%%%%%%%
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       %bout wiothout mistake will be 1000
       %bout with mistake will have the number of the most rostral segment
       
       errorListNotFixedMistakesThisBout = errorListNotFixedMistakes(indRealBoutStartAll(zzzz) : indRealBoutEndAll(zzzz),:); 
       errorListAllMitakesThisBout =  errorListAllMitakes(indRealBoutStartAll(zzzz) : indRealBoutEndAll(zzzz),:);
        
       firstSegmentWithNotfixedMistakes = find(sum(errorListNotFixedMistakesThisBout,1) < 0,1,'first');
       firstSegmentWithAnyMistake = find(sum(errorListAllMitakesThisBout,1) < 0,1,'first');
       
       
       
%        if firstSegmentWithMistakes<5
%           pause 
%        end
       
       if isempty(firstSegmentWithNotfixedMistakes)
           
       firstSegmentWithNotfixedMistakes = 1000;
       
       end
       
       if isempty(firstSegmentWithAnyMistake)
           
       firstSegmentWithAnyMistake = 1000;
       
       end
       
%        %test tailFailing array
%        clf
%        plot(tailFailingThisBout,'linewidth',3)
%        hold on
%        plot(cumsumInterpFixedSegmentAngles(indRealBoutStartAll(zzzz) : indRealBoutEndAll(zzzz),:),'color','k');
%        pause

       
       
     
        %%
        %%%%%%%%%%%% make BoutInf array %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      [BoutInfThisBout] = boutInfMaker_21(zzzz,xPos,yPos,dataSetNumber,protocolNumber,fishUniqueNumber,thisBoutStim, boutPreviousStim,pixelSize,headToBladderLength,tailSegmentLength,...
          distanceEyesToBlob,lastMeasuredSegment,fishAge,boutUniqueNumber,lagVector,indRealBoutStartAll,indRealBoutEndAll,sequenceOfBoutsInStim,uniqueFileNumber,...
          sequenceOfBoutsInNoCollision,latencyOfBoutAfterStim,stimUniqueNumberBoutVector,brokenBouts,halfBeatPosWithNaNTag,halfBeatMagWithNaNTag,doubleBout,...
          firstSegmentWithNotfixedMistakes,firstSegmentWithAnyMistake,realFishUniqueNumber,trueUniqueFishNumber,wellLetter,eyeConvDiffThisBout);

      
       
      
      
      %%
      %%%%%%%%%%%%%%% concatenate  BoutInfArray %%%%%%%%%%%%%%%%%%%%%%%%%%%
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
        BoutInf = [BoutInf' BoutInfThisBout']';
        
       
    %update bout number
    boutUniqueNumber = boutUniqueNumber + 1;

        end%no bouts
     
end%bout loop   


%%
%%%%%%%%%%%%% Cat bout Type %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%cats bouts and measures how far they are from centrer. - distance to center meaure may be broken!!!

%only cats bouts
% [BoutInf] = boutCatFunction_1(BoutInf,BoutKinematicParameters,meanAllVar,stdAllVar,COEFF,maxKinPars,indKinPars,meanPCASpace,numbOfK,boutMapData,distanceMethod,mapAssignment);



%cat 13 bout types solution
boutCatInd = EnumeratorBoutInf.boutCat;
distToClusterCenterInd =EnumeratorBoutInf.distToClusterCenter;
mistakeTag = 1;
boutDataPCASampleThis = [];%if empty it will put bouts in kin space

[BoutInf,boutDataPCASampleThis] = boutCatFunctionWithDistanceToCenterCalculator_5(BoutInf,BoutKinematicParameters,meanAllVar,stdAllVar,COEFF,maxKinPars,...
    indKinPars,meanPCASpace,numbOfK,boutMapData,distanceMethod,mapAssignment,indClosestBoutToTheCenter,boutDataPCASampleThis,boutCatInd,distToClusterCenterInd,mistakeTag);

%cat 14 bout types solution
boutCatInd2 = EnumeratorBoutInf.boutCat2;
distToClusterCenterInd2 =EnumeratorBoutInf.distToClusterCenter2;
mistakeTag2 = 0;

[BoutInf,~] = boutCatFunctionWithDistanceToCenterCalculator_5(BoutInf,BoutKinematicParameters,meanAllVar2,stdAllVar2,COEFF2,maxKinPars2,...
    indKinPars2,meanPCASpace2,numbOfK2,boutMapData2,distanceMethod2,mapAssignment2,indClosestBoutToTheCenter2,boutDataPCASampleThis,boutCatInd2,distToClusterCenterInd2,mistakeTag2);


%%
%%%%%%%%%%%%% make structure to save data %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%fishData
allBoutStructure(r).FishData = allFishStructure(r).FishData;


%FishInf
allBoutStructure(r).FishInf = allFishStructure(r).FishInf;

%half beats
allBoutStructure(r).BeatKinematicParameters = BeatKinematicParameters;
allBoutStructure(r).BeatInf = BeatInf;

%bouts
allBoutStructure(r).BoutKinematicParameters = BoutKinematicParameters;
allBoutStructure(r).BoutInf = BoutInf;


end%if stim block does not have bouts
         end%multifish loop       
        
         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  save data  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    pathfileName = char(strcat(dataPathFinal, '\', fileName));

       save(pathfileName, 'allBoutStructure', 'FileInfStructure','-v7.3')       
   toc
   
   
%update stim unique number
stimUniqueNumber = stimUniqueNumber(end) + 1;

%%
%%%%%%%%%%%%%%%% make fish map %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fishMap(allFileNumb).dataSetNumber =  dataSetNumber;
fishMap(allFileNumb).fishPath = pathfileName;   
fishMap(allFileNumb).fishAge = fishAge;   
fishMap(allFileNumb).uniqueFileNumber = uniqueFileNumber;   

%update index of fish
allFileNumb = allFileNumb +1;   


         
         
clearvars  allBoutStructure a allFishStructure;        
         
         
        end%loop through each file
            
    
    
end%loop trough each data set



%%
  %%%%%%%%%%%%%%% save fish map %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
  dataSetNumberAll = max(dataSetNumber);
  fishUniqueNumberAll = max(fishUniqueNumber);
  uniqueFileNumberAll = max(uniqueFileNumber);
  realFishUniqueNumberAll = max(realFishUniqueNumber);
  trueUniqueFishNumberAll = max(trueUniqueFishNumber);
  boutUniqueNumberAll = max(boutUniqueNumber);
  beatUniqueNumberAll = max(beatUniqueNumber);
  stimUniqueNumberAll = max(stimUniqueNumber);
  
  
  pathfileFishMap = strcat(dataPathBoutFolder, '\', 'fishMapWithUniqueNumbersNew.mat');
  
  save(char(pathfileFishMap),'fishMap', 'dataSetNumberAll', 'fishUniqueNumberAll','uniqueFileNumberAll','realFishUniqueNumberAll','trueUniqueFishNumberAll','boutUniqueNumberAll','beatUniqueNumberAll','stimUniqueNumberAll') 
 
 
  


