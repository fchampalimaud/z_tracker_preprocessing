function []=MikeKinAnalysisAndBoutCat20190925v3DEBUG(boutMapFolder,rawDataFolder,basename)


% boutMapFolder='F:\FreelyMovingBehaviorAnalysis_16\assignBoutTypesUsingMap\BoutMap';
% rawDataFolder='F:\Pedro_WallE\VirtualOpenLoop_OMR\Giant_Danio\6dpf\P1\matfiles';
% basename='OMR_Ontogeny_VOL_01_03_19_Giant_Tank2_C10_06dpf_P1_78_66_walle000';
%% initialize parameters
params.lagflag=190;
params.boutdetect.bcfilt1=10;
params.boutdetect.bcfilt2=10;
params.boutdetect.lastseg=10;
params.boutdetect.maxFilt=20;
params.boutdetect.minFilt=400;
params.boutdetect.distFilt=10;
params.boutdetect.minimuminterboutinterval=10;
params.boutdetect.minimumboutlength=30;
params.boutdetect.startshift=14;
params.boutdetect.endThresh=0.05;
params.errorfix.trackgap=10;


%% load raw data
boutMapFilename = 'BoutMapCenters_kNN4_74Kins4dims_1.75Smooth_slow_3000_auto_4roc_merged11.mat';

% basename='OMR_Ontogeny_VOL_26_02_19_Tu_Tank3764_C06_06dpf_P1_59_50_walle000';
debugFile=matfile(fullfile(rawDataFolder,strcat(basename,'FLAGS_AND_DEBUG.mat')));
mainFile=matfile(fullfile(rawDataFolder,strcat(basename,'MAIN.mat')));

lagNumber=debugFile.alllagvals;     
tailAngles=mainFile.tailAngles;
tailValues=debugFile.tailValues;
bodyAngles=mainFile.bodyAngle;
xPos=mainFile.fishXposition;
yPos=mainFile.fishYposition;
nTailPoints=size(tailAngles,2);

indLag = find(lagNumber > params.lagflag);
lagVector = lagNumber*0;
lagVector(indLag) = 1;
lagVector=sparse(lagVector);
clear indLag

% [realBodyAngles,bodyAnglesVelocity] = FishAngleCalculatorFast(bodyAngles,3*pi/2);%result comes in rads
[realBodyAngles,~] = FishAngleCalculatorFast(bodyAngles,3*pi/2);%result comes in rads
clear bodyAngles
% errorRealBodyAngles=realBodyAngles-sgolayfilt(realBodyAngles,2,7,[],1);
smoothRealBodyAngles=sgolayfilt(realBodyAngles,2,7,[],1);
extrasmoothRealBodyAngles=sgolayfilt(realBodyAngles,2,13,[],1);
        
%% fix tracking errors
[fixedSegmentAngles,errorListAllMitakes,errorListNotFixedMistakes] = FixTailTrackingGaps_3(tailAngles,params.errorfix.trackgap);
clear tailAngles
[fixedSegmentAngles, errorListObends,totalNumberFrames] = FixObendGaps_2(fixedSegmentAngles,tailValues);
clear tailValues

errorListAllMitakes = sparse(errorListAllMitakes);
errorListNotFixedMistakes = sparse(errorListNotFixedMistakes);
errorListObends = sparse(errorListObends);

fixedSegmentAngles=sgolayfilt(fixedSegmentAngles,2,5,[],1);

cumsumInterpFixedSegmentAngles=cumsum(fixedSegmentAngles')';

%% Prepare curvature function for bout detection

smoothedCumsumInterpFixedSegmentAngles=mikesmoothv2(cumsumInterpFixedSegmentAngles',3)';
diffFixedSegmentAngles = [zeros(nTailPoints,1)'; diff(smoothedCumsumInterpFixedSegmentAngles)];

filteredSegmentAngle=conv2(diffFixedSegmentAngles,boxcarf(params.boutdetect.bcfilt1),'same');
cumFilteredSegmentAngle = cumsum(filteredSegmentAngle')';
superCumSegAngle = cumsum(abs(cumFilteredSegmentAngle)')';
tailCurveMeasure = conv(superCumSegAngle(:,params.boutdetect.lastseg),boxcarf(params.boutdetect.bcfilt2),'same');

%max filter transforms max into plateau
smootherTailCurveMeasure = maxFilterFast(tailCurveMeasure,params.boutdetect.maxFilt) - minFilterFast(tailCurveMeasure,params.boutdetect.minFilt);

[distanceNoFilter] = calculateDistance(xPos,yPos);
filteredDistance = conv(distanceNoFilter,boxcarf(params.boutdetect.distFilt),'same'); 

% estimate bout detection threshold
% use the histogram of curve values, estimate stdev of noise using half
% maximum of left part of curve, and take 2 st dev above the peak

    [nums,bins]=hist(log(smootherTailCurveMeasure),-5:0.1:5);
    [y,i]=max(nums(2:30))
    j=find(nums(2:30)>y/2,1,'first')
    hwhm=i-j;
    thresholdindex=i+2*2*hwhm/2.355;
    threshold=exp(bins(round(thresholdindex)+1))
 
    minimuminterboutinterval=params.boutdetect.minimuminterboutinterval;
    minimumboutlength=params.boutdetect.minimumboutlength;
    startshift=params.boutdetect.startshift;
    endThresh=params.boutdetect.endThresh;
    
[allboutstarts, allboutends, indRealEnds,fusedBouts,shortBouts] = BoutDetectorCurvatureFunction20190926v1(smootherTailCurveMeasure,filteredDistance,threshold,minimuminterboutinterval,minimumboutlength,endThresh);
shiftedallboutstarts=allboutstarts;
shiftedallboutstarts(shiftedallboutstarts<startshift)=1;
shiftedallboutstarts(shiftedallboutstarts>startshift)=shiftedallboutstarts(shiftedallboutstarts>startshift)-startshift;

% % LOOK AT THE BOUTS YOU SHOULD DO THIS!
% figure
% for n=1:floor(length(smootherTailCurveMeasure)/5000)
%     
% hold off
%  plot([1:5000]+5000*(n-1),smoothedCumsumInterpFixedSegmentAngles([1:5000]+5000*(n-1),8))
% hold on
% usefulbouts=find(allboutstarts>5000*(n-1)+1,1,'first'):find(allboutends<5000*(n-1)+5000,1,'last')
% for m=usefulbouts
% line([allboutstarts(m) allboutstarts(m)],[-1 1],'Color',[1 0 0])
% line([allboutends(m) allboutends(m)],[-1 1],'Color',[0 1 0])
% line([indRealEnds(m) indRealEnds(m)],[-1 1],'Color',[0 1 1])
% end
% drawnow;
% pause
% end

BeatKinematicParameters = [];
BeatInf = [];

BoutKinematicParameters = [];
BoutInf = [];

% if ~isempty(allboutstarts)%if stim block does not have bouts
rejectedBouts=zeros(size(allboutstarts));
lastMeasuredSegment=10;
for zzzz = 1: length(allboutstarts)
%     tic
%     disp('start')
%     disp('zz')
% zzzz=1
%  zzzz  
   % !!!!!
   if mod(zzzz,100) == 0
       
      disp(strcat(num2str(zzzz), ' out of_',num2str(length(allboutstarts))));
      
   end
    
    
%%
    
%get this bout tail inf
cumsumAngleByBout = cumsumInterpFixedSegmentAngles(allboutstarts(zzzz) : indRealEnds(zzzz),1:lastMeasuredSegment);
    

   
    %%
        %%%%%%% Interpolate data 10 times for beat kin %%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %interpolate data in time domain
        
        %create x - frames in real data
        x = 1:1:length(cumsumAngleByBout);

        %create xi - parts of frames to interpolate
        xi = 1:0.1:length(cumsumAngleByBout);

        %interpolate tail data
        cumsumInterpolatedAngleByBout = zeros(length(xi), size(cumsumAngleByBout,2));

        for f = 1:size(cumsumAngleByBout,2)

    
        cumsumInterpolatedAngleByBout(:,f) = interp1(x,cumsumAngleByBout(:,f),xi, '*spline')';


        end
% disp(2)
% disp(toc)
      
        %interpolate bodyAngle data        
        interpBodyAngles = interp1(x,realBodyAngles((allboutstarts(zzzz)) : (indRealEnds(zzzz))),xi,'*spline');% in rads 
%         interpBoutBodyAngle = interp1(x,bodyAngles((allboutstarts(zzzz)) : (indRealEnds(zzzz))),xi,'*spline');% in rads

        %interpolate position data
        interpPosX = interp1(x,xPos((allboutstarts(zzzz)) : (indRealEnds(zzzz))),xi,'*spline');
        interpPosY = interp1(x,yPos((allboutstarts(zzzz)) : (indRealEnds(zzzz))),xi,'*spline');

        %interpolate smootherTailCurveMeasure
        interpSmootherTailCurveMeasure = interp1(x,smootherTailCurveMeasure((allboutstarts(zzzz)) : (indRealEnds(zzzz))),xi,'*spline');
        
        
        %%
        %%%%%%%%%%%Calculate Curvature parameters%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %!!!! probably not useful or wrong!!!
        tailMM = 310:310:3100;
        [boutCurvature] = TailCurvatureCalculator_20191001(cumsumInterpolatedAngleByBout,tailMM);
        
 
% disp(3)
% disp(toc)
%     
%         %test tail curvature
%         plot(cumsumInterpolatedAngleByBout)
%         hold on
%         plot(boutCurvature, 'o', 'color','k')
%         

        %%
        %%%%%%%%%%%%%%%%%%%%%%%%%Detect beats%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %this data comes interpolated 10 times in time to calcualte beat and bout kinematic parameters
        makeplot = 0;
%      %my half beat detector
%          [cumsum2DInterpolatedAngles,cumsumInterpolatedAnglesGray,indRealBoutStart,indRealBoutEnd,halfBeatStructure,numbBeats,halfBeatPosInt,halfBeatMag] = BeatDetector_25(cumsumInterpolatedAngleByBout,lastMeasuredSegment,makeplot);
%         
     %mike's half beat detector   
%        [cumsum2DInterpolatedAngles,cumsumInterpolatedAnglesGray,indRealBoutStart,indRealBoutEnd,halfBeatStructure,numbBeats,halfBeatPosInt,halfBeatMag] = BeatDetector_24Mike4(cumsumInterpolatedAngleByBout,lastMeasuredSegment,makeplot);
%    clf
% disp('a')
% disp(toc)
 [cumsum2DInterpolatedAngles,cumsumInterpolatedAnglesGray,indRealBoutStart,indRealBoutEnd,halfBeatStructure,numbBeats,halfBeatPosInt,halfBeatMag]...
     = BeatDetector_20191001(cumsumInterpolatedAngleByBout,lastMeasuredSegment,makeplot);
 
% disp('b')
% disp(toc)
 if isnan(indRealBoutStart) 
     rejectedBouts(zzzz)=1;
 end
% disp('c')
% disp(toc)
%  pause
% end
% %    clf
%         figure
% hold off
% plot(cumsumInterpolatedAngleByBout)
% hold on
% plot(halfBeatPosInt,halfBeatMag,'ok')
%      pause
        
% disp(4)
% disp(toc)


        %%
        %%%%%%%%%%%%%%%%%%%% exclude 1st beats that are super small %%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         %not sure what is the best parameter to erase this beats
%         if halfBeatStructure(1).beatDuration < 50
%             
%             halfBeatStructure(1) = [];
%             numbBeats = numbBeats -1;
%         
%         end
        
        
        
        if ~isnan(indRealBoutStart) && ~isempty(indRealBoutStart)%case where there is no detected bout
            
       %%
       %%%%%%%%%%%%%%%%% get pos and mag of beats for this bout %%%%%%%%%%%
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       
               if length(halfBeatPosInt) <= 50

                %transform int pos into pos - raw data will not be int
                halfBeatPos = round(halfBeatPosInt/10);

                %create nan tag - to make variables 50 length in total
                nanTag = nan(1,(50 - length(halfBeatPosInt)));

                % concatenate nanTag with variables
                halfBeatPosWithNaNTag = [halfBeatPos nanTag];
                halfBeatMagWithNaNTag = [halfBeatMag nanTag];



               else

               %transform int pos into pos - raw data will not be int
               halfBeatPos = round(halfBeatPosInt/10);

               halfBeatPosWithNaNTag =  halfBeatPos(1:50); 
               halfBeatMagWithNaNTag = halfBeatMag(1:50);  

               end

        %%
        %%%%%%%%%%%%%%%%%% calculate correct starts and ends of bouts %%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
         %make vector with real start and ends of bout
        indRealBoutStartAll(zzzz) = allboutstarts(zzzz) + floor(indRealBoutStart/10);%need to round indRealBoutStart because is from interpolated data
        indRealBoutEndAll(zzzz) = allboutstarts(zzzz) + floor(indRealBoutEnd/10);
     
%         %test more acurate start of bout
%          hold on
%          plot(indRealBoutStartAll, 'color','green')
%          plot(allboutstarts, '.')
%          plot(indRealBoutStartAll, 'g.')
%          plot(indRealBoutEndAll, 'r.')
        
        
        %calculate index of bout end by distance
        indDistanceBoutEnd = size(cumsumInterpolatedAngleByBout,1);%end of bout determined by velocity - it is in pixels;
        
        
%         %test start and end of bouts
%         plot(cumsumInterpFixedSegmentAngles(:,1:lastMeasuredSegment), 'color', 'k')
%         hold on
%         %rought bout start
%         plot(allboutstarts,(zeros(1,length(allboutstarts))+1),'o','color',[0 100 0]/255, 'linewidth',3)
%         %accurate bout start
%         plot(indRealBoutStartAll,(zeros(1,length(indRealBoutStartAll))+1),'og', 'linewidth',3)
%         %rough bout end
%         plot(allboutends,(zeros(1,length(allboutends))+1),'or', 'linewidth',3)
%         %accurate bout end
%         plot(indRealBoutEndAll,(zeros(1,length(indRealBoutEndAll))+1),'om', 'linewidth',3)
%         %bout end by distance
%         plot(indRealEnds,(zeros(1,length(indRealEnds))+1),'o','color',[128 	0 	0]/255, 'linewidth',3)
        
        

%% interpolate body angles to avoid strange bug of bout max anglular velocity %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 %create x - frames in real data
        x = 1:1:length(realBodyAngles((indRealBoutStartAll(zzzz)) : (indRealBoutEndAll(zzzz))));

        %create xi - parts of frames to interpolate - 10 times
        xi = 1:0.1:length(realBodyAngles((indRealBoutStartAll(zzzz)) : (indRealBoutEndAll(zzzz))));


        interpBodyAngles2 = interp1(x,realBodyAngles((indRealBoutStartAll(zzzz)) : (indRealBoutEndAll(zzzz))),xi,'*spline');% in rads 
       

    %%%%%%%%%%%%%%determine bout previous stims%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    

        %%
        %%%%%%%%%%%%%Extrapolate missing segments%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %!!!this kin parameters don't seem to be useful - don't know why 
        %if you want to use extrpolated data use BeatPropagationCalculatorExtrapolation_2.m, if not use BeatPropagationCalculator_2.m
        
%         %not extrapolating missing segments
         %[halfBeatExtrapolatedStructure,velocityBeatPropagationStructure] = BeatPropagationCalculator_2(halfBeatStructure,tailMM,cumsum2DInterpolatedAngles);
         
        %extrapolating missing segments
% disp(4)
% disp(toc)
        [halfBeatExtrapolatedStructure,velocityBeatPropagationStructure] = halfBeatTailWaveExtrapolation_20190926(halfBeatStructure,tailMM,cumsum2DInterpolatedAngles);
 
% disp(5)
% disp(toc)
        %%
        %%%%%%%%%%%%%%%%%%%%%%beat loop%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %to save inds of beats
        indBeatStartAll = zeros(1,length(halfBeatExtrapolatedStructure));
        indBeatEndAll = zeros(1,length(halfBeatExtrapolatedStructure));
        
        BeatKinematicParametersThisBout = zeros(length(halfBeatExtrapolatedStructure),50);
        
        BeatInfThisBout = zeros(length(halfBeatExtrapolatedStructure),32);
        
        
                for j = 1: length(halfBeatExtrapolatedStructure) %beat loop
        %             j=1
        %           j =2  
                    %%%%%%%%%%%%get start and end of beat in bout%%%%%%%%%%%%%%
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    indBeatStart = halfBeatExtrapolatedStructure(j).indBeatStart;
                    indBeatStartNotInt = round(indBeatStart/10);
                    indBeatStartNotIntAllData = allboutstarts(zzzz) + indBeatStartNotInt;

                    indBeatEnd = halfBeatExtrapolatedStructure(j).indBeatEnd;
                    indBeatEndNotInt = round(indBeatEnd/10);
                    indBeatEndNotIntAllData = allboutstarts(zzzz) + indBeatEndNotInt;

                    %%avoid case where beat start ind is zero or negative
                    if indBeatStartNotInt < 1

                        indBeatStartNotInt = 1;

                    end


                    %save beat position to calculate bout frequency
                    indBeatStartAll(j) = indBeatStartNotInt;
                    indBeatEndAll(j) = indBeatEndNotInt;

                    %%
        %             %%%%%%%%%%%%%%%%%%%%%%%%%%%%plot each beat%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %             %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %             %!!! broken don't know why
        %             beatImage = cumsum2DInterpolatedAngles;
        %             
        %             beatImage(halfBeatExtrapolatedStructure(j).halfBeatLinearInd) = 1;
        %             
        %             
        %             subplot(1,2,1)
        %             imagesc(cumsum2DInterpolatedAngles)
        %             colormap gray
        %             
        %             subplot(1,2,2)
        %             imagesc(beatImage)
        %             colormap gray
        %             pause



        %%
                %%%%%%%%%%%%%%%%%Calculate beat kin parameters%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        pixelSize=70;
                [BeatKinematicParametersThisBeat, ~,~] = BeatParametersCalculator_20190926(j,tailMM, halfBeatExtrapolatedStructure, ...
                    velocityBeatPropagationStructure, interpBodyAngles, indRealBoutStart,  interpPosX, interpPosY, pixelSize,...
                    interpSmootherTailCurveMeasure,boutCurvature);

                %%
                %%%%%%%% calculate max angular speed with less smoothing %%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %         realBodyAnglesLessSmoothThis = realBodyAnglesLessSmooth(indRealBoutStartAll(zzzz):indRealBoutEndAll(zzzz));

               realBodyAnglesLessSmoothThis = realBodyAngles(indRealBoutStartAll(zzzz):indRealBoutEndAll(zzzz));
                            if j == 1 

                            indStart = round(min([halfBeatExtrapolatedStructure.indBeatStart])/10)-1;


                            end

                [BeatKinematicParametersThisBeat] = BeatMaxAngularSpeedCorr_20190926(realBodyAnglesLessSmoothThis,indStart,indBeatStartNotInt,indBeatEndNotInt,BeatKinematicParametersThisBeat);

        %         disp('here')
        %         pause
                %%
                %%%%%%%%%%%%%%%%%% concatenate beat kin parameters %%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                BeatKinematicParametersThisBout(j,:) = BeatKinematicParametersThisBeat;


% disp(6)
% disp(toc)
        %         pause
                %%
                %%%%%%%%%%%% make BeatInf array %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


        %         [BeatInfThisBeat] = beatInfMaker_5(zzzz,xPos,yPos,dataSetNumber,protocolNumber,fishUniqueNumber,thisBoutStim, ...
        %             boutPreviousStim,pixelSize,headToBladderLength,tailSegmentLength,distanceEyesToBlob,lastMeasuredSegment,fishAge,boutUniqueNumber,...
        %             lagVector,allboutstarts,sequenceOfBoutsInStim,uniqueFileNumber,beatUniqueNumber,indBeatStartNotIntAllData,indBeatEndNotIntAllData,...
        %             indBeatStartNotInt,indBeatEndNotInt,indBeatStart,indBeatEnd,sequenceOfBoutsInNoCollision,latencyOfBoutAfterStim,...
        %             stimUniqueNumberBoutVector,realFishUniqueNumber,trueUniqueFishNumber,wellLetter);
        % 
        %         BeatInfThisBout(j,:) = BeatInfThisBeat;
        %         
        %         beatUniqueNumber = beatUniqueNumber + 1;

                end %beat loop

        BeatInf = [BeatInf' BeatInfThisBout']';
        BeatKinematicParameters = [BeatKinematicParameters'  BeatKinematicParametersThisBout']';
        
        %%
        %%%%%%%%%%%%%%%%%% find C1 and C2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [C1Angle,C1Duration,C1MaxAngularSpeed,C2Angle,C2Duration,C2MaxAngularSpeed] = C1C2Calculator_20190926(halfBeatStructure,interpBodyAngles);
        
       
        
        %%
        %%%%%%%%%%%%%%% Calcualte eye convergence mean by bout %%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

       
        %%%%%%%%%%%%%%% calcualte eyeConvDiff %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        bouSpace = 20;
      
        
       
        
        
       
        %%
        %%%%%%%%%%%%%%%%%Calculate bout kin parameters%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
%         pause
        
         [BoutKinematicParametersThisBout] = BoutParametersCalculator_20190926(BeatKinematicParametersThisBout,indRealBoutEnd,indRealBoutStart,...
            indDistanceBoutEnd,interpBodyAngles,interpBodyAngles2,interpPosX,interpPosY,pixelSize,cumsumInterpolatedAngleByBout,C1Angle,...
            C1Duration,C1MaxAngularSpeed,C2Angle,C2Duration,C2MaxAngularSpeed);

        
        %%
        %%%%%%%%%%%%%%%%%Calculate bout max angular speed less smothing %%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
        
        [BoutKinematicParametersThisBout] = BoutParametersCalculatorMaxAngularSpeed_20190926(BoutKinematicParametersThisBout,...
            BeatKinematicParametersThisBout,realBodyAnglesLessSmoothThis);

       
        
        
        BoutKinematicParameters = [BoutKinematicParameters' BoutKinematicParametersThisBout']';
       
        %%
        %%%%%%%%%%%%%%%%%%% find broken bouts %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
       boutMaxAngleRatioThisBout = BoutKinematicParametersThisBout(:,EnumeratorBoutKinPar.boutMaxAngleRatio);
       boutMaxAngularSpeedThisBout = BoutKinematicParametersThisBout(:,EnumeratorBoutKinPar.boutMaxAngularSpeed);
        
               if ~isnan(boutMaxAngleRatioThisBout) && ~isnan(boutMaxAngularSpeedThisBout)

                   if boutMaxAngleRatioThisBout >= 33 || boutMaxAngularSpeedThisBout > 43

                   brokenBouts = 1;

                   else
                        if boutMaxAngleRatioThisBout >= 0.5 && boutMaxAngularSpeedThisBout > 10

                            brokenBouts = 1;

                        else

                            brokenBouts = 0;

                        end
                   end
               else 

                   brokenBouts = 1;

               end
       
       %%
       %%%%%%%%%%%%%%%% find double bouts %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       minBoutFreqCorrThisBout = BoutKinematicParametersThisBout(:,EnumeratorBoutKinPar.minBoutFreqCorr);
       
                   if ~isnan(minBoutFreqCorrThisBout)

                       if minBoutFreqCorrThisBout < 15

                           doubleBout = 1;

                       else 

                           doubleBout = 0;

                       end


                   else

                       doubleBout = 1;

                   end
       
       
       %%
       %%%%%%%%%%%%%%%% determine number of trail mistakes  %%%%%%%%%%%%%%%
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       %bout wiothout mistake will be 1000
       %bout with mistake will have the number of the most rostral segment
       
       errorListNotFixedMistakesThisBout = errorListNotFixedMistakes(indRealBoutStartAll(zzzz) : indRealBoutEndAll(zzzz),:); 
       errorListAllMitakesThisBout =  errorListAllMitakes(indRealBoutStartAll(zzzz) : indRealBoutEndAll(zzzz),:);
        
       firstSegmentWithNotfixedMistakes = find(sum(errorListNotFixedMistakesThisBout,1) < 0,1,'first');
       firstSegmentWithAnyMistake = find(sum(errorListAllMitakesThisBout,1) < 0,1,'first');
       
    
               if isempty(firstSegmentWithNotfixedMistakes)

               firstSegmentWithNotfixedMistakes = 1000;

               end
       
               if isempty(firstSegmentWithAnyMistake)

               firstSegmentWithAnyMistake = 1000;

               end
  
% disp(7)
% disp(toc)
      

        end%no bouts
     
end%bout loop   

save(fullfile(rawDataFolder,'tempBout2.mat'),'rejectedBouts','allboutstarts','allboutends','indRealEnds','smootherTailCurveMeasure','smoothedCumsumInterpFixedSegmentAngles','realBodyAngles','BoutKinematicParameters')
% %cat 13 bout types solution
% boutCatInd = EnumeratorBoutInf.boutCat;
% distToClusterCenterInd =EnumeratorBoutInf.distToClusterCenter;
% mistakeTag = 1;
% boutDataPCASampleThis = [];%if empty it will put bouts in kin space
% 
% [BoutInf,boutDataPCASampleThis] = boutCatFunctionWithDistanceToCenterCalculator_5(BoutInf,BoutKinematicParameters,meanAllVar,stdAllVar,COEFF,maxKinPars,...
%     indKinPars,meanPCASpace,numbOfK,boutMapData,distanceMethod,mapAssignment,indClosestBoutToTheCenter,boutDataPCASampleThis,boutCatInd,distToClusterCenterInd,mistakeTag);
