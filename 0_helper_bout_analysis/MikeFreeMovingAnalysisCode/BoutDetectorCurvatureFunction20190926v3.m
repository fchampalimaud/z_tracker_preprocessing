function [allboutstarts,allboutends,indRealEnds,fusedBouts,shortBouts] = BoutDetectorCurvatureFunction20190926v3(smootherTailCurveMeasure,filteredDistance,threshold,minimuminterboutinterval,minimumboutlength,startThresh,endThresh)

% maxData=max(smootherTailCurveMeasure);

%threshold on filtered movement for end of bout detection
% endThresh=0.025;

allBouts=find(diff(smootherTailCurveMeasure>threshold));
fusedCount=1;


%if array starts in the middle of a bout
if (smootherTailCurveMeasure(1)>threshold)
    allboutstarts=allBouts(2:2:end);
    allboutends=allBouts(3:2:end);
    allboutstarts=allboutstarts(1:length(allboutends));
else
%if array does not start in the middle of bout
    allboutstarts=allBouts(1:2:end);
    allboutends=allBouts(2:2:end);
    allboutstarts=allboutstarts(1:length(allboutends));
end

%%%%%%%%%%%%%%%%%%%%%%%%add bouts that are near%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% minimumboutlength=30;

newallboutstarts=allboutstarts(find((allboutends-allboutstarts)>minimumboutlength));
newallboutends=allboutends(find((allboutends-allboutstarts)>minimumboutlength));
shortBouts(:,1)=allboutstarts(find((allboutends-allboutstarts)<=minimumboutlength));
shortBouts(:,2)=allboutends(find((allboutends-allboutstarts)<=minimumboutlength));
allboutstarts=newallboutstarts;
allboutends=newallboutends;

%adjustboutstarts
dCurve=diff(smootherTailCurveMeasure);
truestarts=zeros(size(allboutstarts));
paddallboutends=[0 allboutends];
for n=1:length(allboutstarts)
   belowthresh=find(smootherTailCurveMeasure(1:allboutstarts(n))<startThresh,1,'last'); 
       if ((allboutstarts(n)-belowthresh<120)&(belowthresh>=paddallboutends(n)))
   truestarts(n)=belowthresh+1;
       else
           
    thisstart=find(dCurve(1:allboutstarts(n))<0,1,'last'); 
    if (~isempty(thisstart))
   truestarts(n)=thisstart+1;
    else
        truestarts(n)=allboutstarts(n); 
    end
       end
%     thisstart=find(dCurve(1:allboutstarts(n))<0,1,'last'); 
%     if (~isempty(thisstart))
%    if (belowthresh>thisstart)
   
%    else
%        if (allboutstarts(n)-belowthresh<minimumboutlength)
%            truestarts(n)=belowthresh+1;
%        else
%         truestarts(n)=thisstart+1;
%        end
%    end
%     else 
%        
%    truestarts(n)=allboutstarts(n); 
%     end

end
disp(strcat('maxadjust',num2str(max(allboutstarts-truestarts))));

allboutstarts=truestarts;

% minimuminterboutinterval=30;
for n=1:length(allboutstarts)-1
   allboutmaxes(n)= max(smootherTailCurveMeasure(allboutstarts(n):allboutends(n)));
end

fusedBouts=[];
while (1==1)
 allinterboutlengths=allboutstarts(2:end)-allboutends(1:end-1);%calculate inter bout lengths
 if ~isempty(find((allinterboutlengths<minimuminterboutinterval)&(allboutmaxes<threshold*10), 1))%if there are bouts that are near
    shortinterval= find(allinterboutlengths<minimuminterboutinterval,1);
    
    fusedBouts(fusedCount,1)=shortinterval;
    fusedBouts(fusedCount,2)=allinterboutlengths(shortinterval);
    fusedBouts(fusedCount,3)=allboutmaxes(shortinterval);
    fusedCount=fusedCount+1;
    
    if (shortinterval<length(allboutstarts)-1)
    allboutstarts=[allboutstarts(1:shortinterval) allboutstarts(shortinterval+2:end)];
    allboutmaxes=[allboutmaxes(1:shortinterval) allboutmaxes(shortinterval+2:end)];
  
    else
    allboutstarts=allboutstarts(1:shortinterval);
    allboutmaxes=allboutmaxes(1:shortinterval-1);
  
    end
    
    
    if (shortinterval>1)
    allboutends=[allboutends(1:shortinterval-1) allboutends(shortinterval+1:end)];
    
    else
        allboutends=allboutends(2:end);
  
    end
    
 else
     break
 end
    
end
% size(allboutstarts)
% size(allboutends)
% size(allboutmaxes)







%this seems to be a correction to select a bit ahead of the bout
% allboutstarts(allboutstarts>startshift)=allboutstarts(allboutstarts>startshift)-startshift;
% allboutstarts(allboutstarts<startshift+1)=1;


%%%%%%%%%%%%%%%%%%%erase bouts with small curvature%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~isempty(allboutstarts)%avoid cases when it does not pick any bout

%%%%%%%%%%%%%%%%%Pick end of bout by min distance%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
indRealEnds=allboutstarts*0;

for n=1:length(allboutstarts)-1
   boutregion= filteredDistance(allboutstarts(n):allboutends(n));
   interboutregion= filteredDistance(allboutends(n):allboutstarts(n+1)-1);
   [Y,I] = min(interboutregion);
%    interboutregion
   [Ymax,Imax] = max(boutregion);
   thisThresh=Ymax*endThresh;
   
   I2=find(interboutregion<thisThresh,1,'first');
   
   if (~isempty(I2))
           indRealEnds(n)=allboutends(n)+min(I,I2)-1;
   else
%        I
           indRealEnds(n)=allboutends(n)+I-1;
       
   end
   
end

   boutregion= filteredDistance(allboutstarts(end):allboutends(end));
 interboutregion= filteredDistance(allboutends(end):end);
   [Y,I] = min(interboutregion);
   
   [Ymax,Imax] = max(boutregion);
   thisThresh=Ymax*endThresh;
   
   I2=find(interboutregion<thisThresh,1,'first');
   if (~isempty(I2))
           indRealEnds(length(allboutstarts))=allboutends(length(allboutstarts))+min(I,I2)-1;
   else

           indRealEnds(length(allboutstarts))=allboutends(length(allboutstarts))+I-1;
   end
else
    indRealEnds=[];
end          
% allboutstarts=allboutstarts;
% allboutends=allboutends;
% indRealEnds=indRealEnds;


%test starts and ends of bouts
% startBout=smootherTailCurveMeasure(indThisStimTypeStart2:indThisStimTypeEnd2)*0;
% startBout(allboutstarts)=1;
% endBout=smootherTailCurveMeasure(indThisStimTypeStart2:indThisStimTypeEnd2)*0;
% endBout(allboutends)=1;
% realEndBout=smootherTailCurveMeasure(indThisStimTypeStart2:indThisStimTypeEnd2)*0;
% realEndBout(indRealEnds)=1;

 

% figure
% %plot(cumFilteredSegmentAngle(indThisStimTypeStart2:indThisStimTypeEnd2))
% hold on
% plot(smootherTailCurveMeasure(indThisStimTypeStart2:indThisStimTypeEnd2),  'color', 'black')
% hold on
% plot(startBout,  'color', 'green','LineWidth',2) 
% hold on
% plot(endBout,  'color', 'red','LineWidth',2) 
% hold on
% plot(realEndBout, 'color', 'magenta','LineWidth',2) 
% length(allboutstarts)
% % pause



end