%% load raw data\folderToLoad
folderToLoad='F:\FreelyMovingBehaviorAnalysis_16\assignBoutTypesUsingMap\BoutMap'
boutMapFilename = 'BoutMapCenters_kNN4_74Kins4dims_1.75Smooth_slow_3000_auto_4roc_merged11.mat';

load(fullfile(folderToLoad, boutMapFilename),'behavioralSpaceStructure','finalClustering','indClosestBoutToTheCenter')
% indClosestBoutToTheCenter = indClosestBoutToTheCenter;
meanAllVar = behavioralSpaceStructure.meanAllVar;
stdAllVar = behavioralSpaceStructure.stdAllVar;
COEFF = behavioralSpaceStructure.COEFF;
maxKinPars = behavioralSpaceStructure.maxKinPars;
indKinPars = behavioralSpaceStructure.indKinPars;
meanPCASpace = behavioralSpaceStructure.meanPCASpace;

 inputDataThisKins =  BoutKinematicParameters(:,indKinPars);
 
kinData = abs(inputDataThisKins);

for i = 1 : size(kinData,2)
    
    
    kinData(:,i) = kinData(:,i) + maxKinPars(i);
    
end

kinData = log(kinData);

%%
%%%%%%%%%%%%%%% reconstruct z-score %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

zScoreThisData = zeros(size(kinData,1),size(kinData,2));

for ii = 1 : size(kinData,2)
 
     kinDataThis = kinData(:,ii);

    zScoreThisData(:,ii) = (kinDataThis  -  meanAllVar(ii))/(stdAllVar(ii));
  
end

%erase nans
zScoreThisData(isnan(zScoreThisData)) = 0;  


%%
%%%%%%%%%%%%%% reconstruct PCA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 newScore = (bsxfun(@minus,zScoreThisData,meanPCASpace))*COEFF;

 figure
plot(newScore(:,1),newScore(:,2),'.')
hold on
plot(finalClustering.dataToDoMap(:,1),finalClustering.dataToDoMap(:,2),'r.')

for n=25
   thiskp=indKinPars(n);
   figure
   title(strcat(num2str(n),'xxx',num2str(thiskp)))
   plot(BoutKinematicParameters(:,thiskp),'.')
   hold on
   
   plot(newBoutStructure.BoutKinematicParameters(:,thiskp),'r.')
end
   