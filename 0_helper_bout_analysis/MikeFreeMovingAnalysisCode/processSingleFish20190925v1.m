
   
    [distanceNoFilter] = calculateDistance(xPos,yPos);

    dfilt = 10;
    filteredDistance = conv(distanceNoFilter,ones(dfilt,1)/dfilt,'same');
    
%     %test distance
%     hold off
%     plot(distanceNoFilter)
%     hold on
%     plot(filteredDistance,'color','red')
%     pause

%%
%%%%%%%%%%%%% smooth data of normal rig %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%have to add new dat


%do this only to normal rig data
if isnan(blobVal)

realBodyAnglesSmooth = realBodyAngles;
cumsumInterpFixedSegmentAnglesSmooth = cumsumInterpFixedSegmentAngles;
smootherTailCurveMeasureSmooth = smootherTailCurveMeasure';

else
    
realBodyAnglesSmooth = realBodyAngles;
cumsumInterpFixedSegmentAnglesSmooth = cumsumInterpFixedSegmentAngles;
smootherTailCurveMeasureSmooth = smootherTailCurveMeasure';    
    
    
% realBodyAnglesSmooth = mikesmooth(realBodyAngles,5);
% cumsumInterpFixedSegmentAnglesSmooth = mikesmooth(cumsumInterpFixedSegmentAngles,5);   
% smootherTailCurveMeasureSmooth = mikesmooth(smootherTailCurveMeasure',5);   

end

%%
%%%%%%%%%%%%%%%%%%%%%%%%% Make fish Inf structure %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%this structutre saves information about the fish that is a string or more than one number 
if r == 1

FileInfStructure.fileName = nan;
FileInfStructure.strain = strain;
FileInfStructure.stimOrder = stimNumbers;
FileInfStructure.indStimStartAll = indStimStartAll;
FileInfStructure.indStimEndAll = indStimEndAll;
FileInfStructure.tailMM = tailMM;
FileInfStructure.indNoCollisionStartAll = indNoCollisionStartAll;
FileInfStructure.indNoCollisionEndAll = indNoCollisionEndAll;
FileInfStructure.indFishInSquareStartAll = indFishInSquareStartAll;
FileInfStructure.indFishInSquareEndAll = indFishInSquareEndAll;
FileInfStructure.percEyeConvBouts = NaN;

end
%%
%%%%%%%%%%%%%%%%%%%%%%%%% Make fish Inf array %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%this array saves one number information
% FishInf(1) = arenaX;
% FishInf(2) = arenaY; 
% FishInf(3) = dataSetNumber;
% FishInf(4) = protocolNumb;
% FishInf(5) = fishUniqueNumber;
% FishInf(6) = pixelSize./1000;%in mm 
% FishInf(7) = headToBladderLength;%in pixel
% FishInf(8)  = tailSegmentLength;%in pixel
% FishInf(9) = distanceEyesToBlob./1000;%in mm
% FishInf(10) = lastMeasuredSegment;
% FishInf(11) = fishAge;
% FishInf(12) = uniqueFileNumber;
% 
% if isnan(leftEyeAngle) 
%     
%     FishInf(13) = 0;
%     
% else
%     
%     FishInf(13) = 1;
%     
% end
% 
% FishInf(14) = r;%fish number in file

allFishStructure(r).FishInf(1) = arenaX;
allFishStructure(r).FishInf(2) = arenaY; 
allFishStructure(r).FishInf(3) = dataSetNumber;
allFishStructure(r).FishInf(4) = protocolNumb;
allFishStructure(r).FishInf(5) = fishUniqueNumber;
allFishStructure(r).FishInf(6) = pixelSize./1000;%in mm 
allFishStructure(r).FishInf(7) = headToBladderLength;%in pixel
allFishStructure(r).FishInf(8)  = tailSegmentLength;%in pixel
allFishStructure(r).FishInf(9) = distanceEyesToBlob./1000;%in mm
allFishStructure(r).FishInf(10) = lastMeasuredSegment;
allFishStructure(r).FishInf(11) = fishAge;
allFishStructure(r).FishInf(12) = uniqueFileNumber;

if isnan(leftEyeAngle) 
    
    allFishStructure(r).FishInf(13) = 0;
    
else
    
    allFishStructure(r).FishInf(13) = 1;
    
end

allFishStructure(r).FishInf(14) = r;%fish number in file
allFishStructure(r).FishInf(15) = realFishUniqueNumber;
allFishStructure(r).FishInf(16) = trueUniqueFishNumber;

%save average pos of fish in arena to identify fish in multiple well arenas
allFishStructure(r).FishInf(17) = nanmean(xPos);
allFishStructure(r).FishInf(18) = nanmean(yPos);

%%
%%%%%%%%%%%%%%%%%%% make array with fish processed information%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%open variable
allFishStructure(r).FishData = zeros(length(bodyAngles),67);
allFishStructure(r).FishData(:,1) = bodyAngles;
allFishStructure(r).FishData(:,2) = realBodyAnglesSmooth;
allFishStructure(r).FishData(:,3:12) = cumsumInterpFixedSegmentAnglesSmooth;
allFishStructure(r).FishData(:,13) = smootherTailCurveMeasureSmooth;
allFishStructure(r).FishData(:,14) = filteredDistance;
% allFishStructure(r).FishData(:,15) = lagVector;
allFishStructure(r).FishData(:,15) = lagNumber;

if isnan(leftEyeAngle) 
    
    %make nan vector to fill array on eye data
    nanVector = zeros(length(bodyAngles),1);
    nanVector(:,:) = NaN;
    
    nanVector2 = zeros(length(bodyAngles),31);
    nanVector2(:,:) = NaN;
    
allFishStructure(r).FishData(:,16) = nanVector;
allFishStructure(r).FishData(:,17) = nanVector;
allFishStructure(r).FishData(:,18) = nanVector;
allFishStructure(r).FishData(:,19:49) = nanVector2;

    
else
    
allFishStructure(r).FishData(:,16) = leftEyeAngle;
allFishStructure(r).FishData(:,17) = rightEyeAngle;
allFishStructure(r).FishData(:,18) = eyeConvergence;
allFishStructure(r).FishData(:,19:49) = parameciaCounter;

end

allFishStructure(r).FishData(:,50) = xPos;
allFishStructure(r).FishData(:,51) = yPos;
allFishStructure(r).FishData(:,52) = stim;

allFishStructure(r).FishData(:,53) = noCollision';
allFishStructure(r).FishData(:,54) = fishInSquare';
allFishStructure(r).FishData(:,55) = (frameNumber2')/700;


allFishStructure(r).FishData(:,56:65) = errorListAllMitakes;
allFishStructure(r).FishData(:,66:75) = errorListNotFixedMistakes;

allFishStructure(r).FishData(:,76:85) = chosenSegmentValues;
allFishStructure(r).FishData(:,86) =  collisionflag(:,r);
allFishStructure(r).FishData(:,87) =  errorListObends;

if isnan(leftEyeAngle) 
    
  allFishStructure(r).FishData(:,88) =  NaN;
  allFishStructure(r).FishData(:,89) = NaN;
else 
  allFishStructure(r).FishData(:,88) =  leftEyeAngleMod;
  allFishStructure(r).FishData(:,89) = rightEyeAngleMod;
end
  allFishStructure(r).FishData(:,90) = residuals(:,7);
  allFishStructure(r).FishData(:,91) = errorRealBodyAngles;
  

%%
%%%%%%%%%%%%% put multi fish in structure %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% allFishStructure = [allFishStructure FishStructure];
% thisFileName
% realFishUniqueNumber
% fishUniqueNumber
% uniqueFileNumber
% pause

        
        
        
        end %multi fish file loop

         
   
% toc





%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%% Save fish data %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%         
% pathfileName = strcat(dataPathSubfolder, '\', fileName);
% 
% save(char(pathfileName), 'allFishStructure','FileInfStructure','-v7.3')  
% 
%         
% % uniqueFileNumber = uniqueFileNumber + 1;


%        toc 
%        end%loop through each file
%     pause
%   end%loop through each dataset

%%
% %%%%%%%%%%%%%%%%%% make unique number tracker %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% dataSetNumberAll = dataSetNumber; 
% fishUniqueNumberAll = max(fishUniqueNumber);
% uniqueFileNumberAll = max(uniqueFileNumber);
% realFishUniqueNumberAll = max(fishUniqueNumber);
% trueUniqueFishNumberAll = max(trueUniqueFishNumber);
% 
% 
%  %%
%  %%%%%%%%%%%%%% save uniqueNumberCounterProMat %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  
%  pathfileName = strcat(dataPath2, '\', 'uniqueNumberCounterProMat.mat');
% 
%  save(char(pathfileName), 'dataSetNumberAll', 'fishUniqueNumberAll','uniqueFileNumberAll','realFishUniqueNumberAll','trueUniqueFishNumberAll') 

       