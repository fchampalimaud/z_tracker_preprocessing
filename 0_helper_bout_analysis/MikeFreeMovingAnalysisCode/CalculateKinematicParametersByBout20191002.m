function []=CalculateKinematicParametersByBout20191002(cumsumAngleByBout,realBodyAnglesByBout,xPosByBout, YPosByBout,extrasmoothRealBodyAnglesByBout,smootherTailCurveMeasureByBoutmakeplot)

x = 1:1:length(cumsumAngleByBout);
xi = 1:0.1:length(cumsumAngleByBout);
lastMeasuredSegment=size(cumsumAngleByBout,2);

        %interpolate tail data
        cumsumInterpolatedAngleByBout = zeros(length(xi), size(cumsumAngleByBout,2));

        for f = 1:size(cumsumAngleByBout,2)
        cumsumInterpolatedAngleByBout(:,f) = interp1(x,cumsumAngleByBout(:,f),xi, '*spline')';
        end
        
        
         [cumsum2DInterpolatedAngles,cumsumInterpolatedAnglesGray,indRealBoutStart,indRealBoutEnd,halfBeatStructure,numbBeats,halfBeatPosInt,halfBeatMag]...
     = BeatDetector_20191001(cumsumInterpolatedAngleByBout,lastMeasuredSegment,makeplot);
 
 
%% calculate parameters
        if ~isnan(indRealBoutStart) && ~isempty(indRealBoutStart)%case where there is no detected bout
            
                %%
        %%%%%%%%%%%Calculate Curvature parameters%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %!!!! probably not useful or wrong!!!
        tailMM = 310:310:3100;
        [boutCurvature] = TailCurvatureCalculator_20191001(cumsumInterpolatedAngleByBout,tailMM);
        
        %% interpolate bodyAngle data        
        interpBodyAngles = interp1(x,realBodyAnglesByBout,xi,'*spline');% in rads 
        interpSmoothBodyAngles = interp1(x,extrasmoothRealBodyAnglesByBout,xi,'*spline');% in rads 
        interpSmoothBodyAngles2=interpSmoothBodyAngles(indRealBoutStart:indRealBoutEnd);
%         interpBoutBodyAngle = interp1(x,bodyAngles((allboutstarts(zzzz)) : (indRealEnds(zzzz))),xi,'*spline');% in rads
        %interpolate position data
        interpPosX = interp1(x,xPosByBout,xi,'*spline');
        interpPosY = interp1(x,yPosByBout,xi,'*spline');

        %interpolate smootherTailCurveMeasure
        interpSmootherTailCurveMeasure = interp1(x,smootherTailCurveMeasureByBout,xi,'*spline');
        
        
            
       %%
       %%%%%%%%%%%%%%%%% get pos and mag of beats for this bout %%%%%%%%%%%
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       % for now only allows for up to 50 beats (maybe change?)
               if length(halfBeatPosInt) <= 50

                %transform interpolated pos into pos - raw data will not be int
                halfBeatPos = round(halfBeatPosInt/10);
                %create nan tag - to make variables 50 length in total
                nanTag = nan(1,(50 - length(halfBeatPosInt)));
                % concatenate nanTag with variables
                halfBeatPosWithNaNTag = [halfBeatPos nanTag];
                halfBeatMagWithNaNTag = [halfBeatMag nanTag];

               else

               %transform int pos into pos - raw data will not be int
               halfBeatPos = round(halfBeatPosInt/10);
               halfBeatPosWithNaNTag =  halfBeatPos(1:50); 
               halfBeatMagWithNaNTag = halfBeatMag(1:50);  

               end

        %%
        %%%%%%%%%%%%%%%%%% calculate correct starts and ends of bouts %%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %calculate index of bout end by distance
        indDistanceBoutEnd = size(cumsumInterpolatedAngleByBout,1);%end of bout determined by velocity - it is in pixels;
        

        [halfBeatExtrapolatedStructure,velocityBeatPropagationStructure] = halfBeatTailWaveExtrapolation_20190926(halfBeatStructure,tailMM,cumsum2DInterpolatedAngles);
 

        %%
        %%%%%%%%%%%%%%%%%%%%%%beat loop%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %to save inds of beats
        indBeatStartAll = zeros(1,length(halfBeatExtrapolatedStructure));
        indBeatEndAll = zeros(1,length(halfBeatExtrapolatedStructure));
        
        BeatKinematicParametersThisBout = zeros(length(halfBeatExtrapolatedStructure),50);
        
        BeatInfThisBout = zeros(length(halfBeatExtrapolatedStructure),32);
        
        
                for j = 1: length(halfBeatExtrapolatedStructure) %beat loop

                    %%%%%%%%%%%%get start and end of beat in bout%%%%%%%%%%%%%%
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    indBeatStart = halfBeatExtrapolatedStructure(j).indBeatStart;
                    indBeatStartNotInt = round(indBeatStart/10);
                    

                    indBeatEnd = halfBeatExtrapolatedStructure(j).indBeatEnd;
                    indBeatEndNotInt = round(indBeatEnd/10);
                    
                    %%avoid case where beat start ind is zero or negative
                    if indBeatStartNotInt < 1

                        indBeatStartNotInt = 1;

                    end


                    %save beat position to calculate bout frequency
                    indBeatStartAll(j) = indBeatStartNotInt;
                    indBeatEndAll(j) = indBeatEndNotInt;

       
        %%
                %%%%%%%%%%%%%%%%%Calculate beat kin parameters%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        pixelSize=70;
                [BeatKinematicParametersThisBeat, ~,~] = BeatParametersCalculator_20191002(j,tailMM, halfBeatExtrapolatedStructure, ...
                    velocityBeatPropagationStructure, interpSmoothBodyAngles , interpBodyAngles, indRealBoutStart,  interpPosX, interpPosY, pixelSize,...
                    interpSmootherTailCurveMeasure,boutCurvature);

              
                %%
                %%%%%%%%%%%%%%%%%% concatenate beat kin parameters %%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                BeatKinematicParametersThisBout(j,:) = BeatKinematicParametersThisBeat;

                end %beat loop

        BeatKinematicParameters = [BeatKinematicParameters'  BeatKinematicParametersThisBout']';
        
        %%
        %%%%%%%%%%%%%%%%%% find C1 and C2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [C1Angle,C1Duration,C1MaxAngularSpeed,C2Angle,C2Duration,C2MaxAngularSpeed] = C1C2Calculator_20190926(halfBeatStructure,interpSmoothBodyAngles);
        
       
        %%
        %%%%%%%%%%%%%%%%%Calculate bout kin parameters%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
%         pause
        
         [BoutKinematicParametersThisBout] = BoutParametersCalculator_20191001(BeatKinematicParametersThisBout,indRealBoutEnd,indRealBoutStart,...
            indDistanceBoutEnd,interpSmoothBodyAngles,interpSmoothBodyAngles2,interpPosX,interpPosY,pixelSize,cumsumInterpolatedAngleByBout,C1Angle,...
            C1Duration,C1MaxAngularSpeed,C2Angle,C2Duration,C2MaxAngularSpeed,realBodyAnglesByBout(ceil(indRealBoutStart/10):ceil(indRealBoutEnd/10)));

        
       
        
        BoutKinematicParameters = [BoutKinematicParameters' BoutKinematicParametersThisBout']';
       
        %%
        %%%%%%%%%%%%%%%%%%% find broken bouts %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
       boutMaxAngleRatioThisBout = BoutKinematicParametersThisBout(:,EnumeratorBoutKinPar.boutMaxAngleRatio);
       boutMaxAngularSpeedThisBout = BoutKinematicParametersThisBout(:,EnumeratorBoutKinPar.boutMaxAngularSpeed);
        
               if ~isnan(boutMaxAngleRatioThisBout) && ~isnan(boutMaxAngularSpeedThisBout)

                   if boutMaxAngleRatioThisBout >= 33 || boutMaxAngularSpeedThisBout > 43

                   brokenBouts = 1;

                   else
                        if boutMaxAngleRatioThisBout >= 0.5 && boutMaxAngularSpeedThisBout > 10

                            brokenBouts = 1;

                        else

                            brokenBouts = 0;

                        end
                   end
               else 

                   brokenBouts = 1;

               end
       
       %%
       %%%%%%%%%%%%%%%% find double bouts %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       minBoutFreqCorrThisBout = BoutKinematicParametersThisBout(:,EnumeratorBoutKinPar.minBoutFreqCorr);
       
                   if ~isnan(minBoutFreqCorrThisBout)

                       if minBoutFreqCorrThisBout < 15

                           doubleBout = 1;

                       else 

                           doubleBout = 0;

                       end


                   else

                       doubleBout = 1;

                   end
       
       
       %%
       %%%%%%%%%%%%%%%% determine number of trail mistakes  %%%%%%%%%%%%%%%
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       %bout wiothout mistake will be 1000
       %bout with mistake will have the number of the most rostral segment
       
       errorListNotFixedMistakesThisBout = errorListNotFixedMistakes(indRealBoutStartAll(zzzz) : indRealBoutEndAll(zzzz),:); 
       errorListAllMitakesThisBout =  errorListAllMitakes(indRealBoutStartAll(zzzz) : indRealBoutEndAll(zzzz),:);
        
       firstSegmentWithNotfixedMistakes = find(sum(errorListNotFixedMistakesThisBout,1) < 0,1,'first');
       firstSegmentWithAnyMistake = find(sum(errorListAllMitakesThisBout,1) < 0,1,'first');
       
    
               if isempty(firstSegmentWithNotfixedMistakes)

               firstSegmentWithNotfixedMistakes = 1000;

               end
       
               if isempty(firstSegmentWithAnyMistake)

               firstSegmentWithAnyMistake = 1000;

               end
  
% disp(7)
% disp(toc)
      

        end%no bouts