function convertPedroData2Mat20190925(dataPath,dataFile,savePath);

if (~exist(savePath))
mkdir(savePath)
end

a = txt2mat(fullfile(dataPath,dataFile));
% a=importdata(fullfile(dataPath,DataFiles(i).name)); 
% 
% alllagframes=find(a(:,38)>10);  % all Frames where there is a processing lag greater than 1/70 second
% alllagframevals=a(alllagframes,38);  % the lag in those frames (in frames)

% alllagframes=find(a(:,38)>10);  % all Frames where there is a processing lag greater than 1/70 second
alllagvals=a(:,38);  % the lag in those frames (in frames)

fishXposition=a(:,2);
fishYposition=a(:,3);

startBufferNumber=a(1,1);  % frame number in the ring buffer


blobMaxValue=a(:,7);  % size and max pixel value of the 'blob' used to find the fish centroid. Should be fairly constant
blobSize=a(:,8);

startTimeHours=a(1,11); %experiment start time
startTimeMinutes=a(1,12);

tailValues=a(:,15:24);  %tail segment pixel values (to find how well the tail was tracked)

bodyAngle=a(:,25);  %body Angle (DEFINE 0 and DIRECTION)
tailAngles=a(:,26:35);  %angle changes for 10 segments

reSearchFish=find(a(:,36)==1); %was the fish Lost and refound at any point?

% save(strrep(fullfile(dataPath,DataFiles(i).name), '.txt', '.mat'), 'a','-v7.3') 
save(strrep(fullfile(savePath,dataFile), '.txt', 'MAIN.mat'),'fishXposition','fishYposition','bodyAngle','tailAngles')
% save(strrep(fullfile(savePath,dataFile), '.txt', 'FLAGS_AND_DEBUG.mat'),'alllagframes','alllagframevals','startBufferNumber'...
save(strrep(fullfile(savePath,dataFile), '.txt', 'FLAGS_AND_DEBUG.mat'),'alllagvals','startBufferNumber'...
    ,'blobMaxValue','blobSize','startTimeHours','startTimeMinutes','tailValues','reSearchFish')

end

