function [clusterAssignmentAllData,distToClusterCenter] = boutCatFunctionWithDistanceToCenterCalculator_20191015(boutDataPCASample, boutMapPath)


% folderToLoad='F:\FreelyMovingBehaviorAnalysis_16\assignBoutTypesUsingMap\BoutMap'
% boutMapFilename = 'BoutMapCenters_kNN4_74Kins4dims_1.75Smooth_slow_3000_auto_4roc_merged11.mat';

load(boutMapPath,'behavioralSpaceStructure','finalClustering','indClosestBoutToTheCenter')
% indClosestBoutToTheCenter = indClosestBoutToTheCenter;
meanAllVar = behavioralSpaceStructure.meanAllVar;
stdAllVar = behavioralSpaceStructure.stdAllVar;
COEFF = behavioralSpaceStructure.COEFF;
maxKinPars = behavioralSpaceStructure.maxKinPars;
indKinPars = behavioralSpaceStructure.indKinPars;
meanPCASpace = behavioralSpaceStructure.meanPCASpace;
boutMapData = finalClustering.dataToDoMap;
boutDataPCASample = boutDataPCASample(:,1:size(boutMapData,2));
%%
%%%%%%%%%%%% put bout in PCA space %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% if isempty(boutDataPCASample)
%     
% [boutDataPCASample] = placeDataIntoFixedBehavioralSpace_1(BoutKinematicParameters,indKinPars,maxKinPars,meanAllVar,stdAllVar,meanPCASpace,COEFF);
% 
% boutDataPCASample = boutDataPCASample(:,1:size(boutMapData,2));
% 
% end
%%
%%%%%%%%%%%%%%%% lable bouts with PCA space mistake %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% if  mistakeTag ==1
% mistakesIn74KinSpace = zeros(1,size(BoutInf,1));
% 
% indMistakesIn74KinSpace = find(boutDataPCASample(:,3) > 10);
% 
% 
% 
% mistakesIn74KinSpace(indMistakesIn74KinSpace) = 1;
% end
%%
%%%%%%%%%%%%%% cat new data by k neightbour %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

numbOfK = 50;
distanceMethod = 'euclidean';
% tic
 [IDX,~] = knnsearch(boutMapData,boutDataPCASample,'k',numbOfK,'Distance',distanceMethod);
% toc


mapAssignment = finalClustering.clusterAssignmentInMap;
knnLabels = mapAssignment(IDX);

%assing points by the most frequent label
 clusterAssignmentAllData = mode(knnLabels');
% dataAssign = knnLabels;


%%
%%%%%%%%%%%%%%%%%%% calcualte distances to center bout %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


distToClusterCenter = zeros(1,length(clusterAssignmentAllData));

for n  = 1 : length(clusterAssignmentAllData)%loop through each bout

%get ind of center bout of this type    
indClosestBoutToTheCenterThisBout = indClosestBoutToTheCenter(clusterAssignmentAllData(n));
    
    
centerBoutThis = boutMapData(indClosestBoutToTheCenterThisBout,:);
distToClusterCenter(n) = pdist2(centerBoutThis,boutDataPCASample(n,1:size(centerBoutThis,2)));



end



%%
%%%%%%%%%%%%%%%% update BoutInf array %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% BoutInf(:,boutCatInd) = clusterAssignmentAllData;
% BoutInf(:,distToClusterCenterInd) = distToClusterCenter;
% 
% 
% if mistakeTag == 1
% BoutInf(:,EnumeratorBoutInf.mistakesIn74KinSpace) = mistakesIn74KinSpace;
% end
% 
% 
% 
% 

