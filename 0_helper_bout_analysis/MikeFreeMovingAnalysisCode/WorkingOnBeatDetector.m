
nframes=size(cumsumInterpolatedAnglesGray,1);
nsegs=size(cumsumInterpolatedAnglesGray,2);

binaryPos = (cumsumInterpolatedAnglesGray > binaryImageUpThres);
binaryNeg = (cumsumInterpolatedAnglesGray < binaryImageDownThres);


%case where it does nor detect anything
if isempty(find(binaryPos)) || isempty(find(binaryNeg))%threshold detects nothing
    
    %!!! don't know what ot put here; maybe nothing?
    
else%threshold detects something
  

%find ind of events
indStartPos = find([zeros(nsegs,1) diff(binaryPos,[],1)] == 1);
indStartNeg = find([zeros(nsegs,1) diff(binaryNeg,[],1)] == 1);
indEndPos = find([zeros(nsegs,1) diff(binaryPos,[],1)] == -1);
indEndNeg = find([zeros(nsegs,1) diff(binaryNeg,[],1)] == -1);

%find lengths events
lengthPos = indEndPos - indStartPos;
lengthNeg = indEndNeg - indStartNeg;

%find events that are smaller than 20 frames - becuse data is interpolated 10 times it corresponds to beats of 2.8ms - way to short
indindFakeBeatPos =  find(lengthPos < 40);
indindFakeBeatNeg = find(lengthNeg < 40);


%erase events that are smaller than 20 frames
fakeIndsPos = indStartPos(indindFakeBeatPos):indEndPos(indindFakeBeatPos);
indStartPos(indindFakeBeatPos) = [];
indEndPos(indindFakeBeatPos) = [];

fakeIndsNeg = indStartNeg(indindFakeBeatNeg):indEndNeg(indindFakeBeatNeg);
indStartNeg(indindFakeBeatNeg) = [];
indEndNeg(indindFakeBeatNeg) = [];

%remake ind binary array
binaryPos2 = zeros(size(cumsumInterpolatedAnglesGray));
binaryNeg2 = binaryPos2;
startPos = binaryPos2;
startNeg = binaryPos2;
endPos = binaryPos2;
endNeg = binaryPos2;

for ii = 1:length(indStartPos)
    
binaryPos2(indStartPos(ii) : indEndPos(ii)-1) = 1;

end

for iii = 1:length(indStartNeg)
    
binaryNeg2(indStartNeg(iii) : indEndNeg(iii)-1) = 1;

end

startPos(indStartPos) = 1;
startNeg(indStartNeg) = 1;
endPos(indEndPos) = 1;
endNeg(indEndNeg) = 1;
 

%%
%%%%%%%%%%%%%%first and last beat must have a certain amplitude%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%find start and end values
startAmp = cumsumInterpolatedAnglesGray(1,:);
endAmp = cumsumInterpolatedAnglesGray(end,:);

%case where it does not detect neg or pos beats
if isempty(indStartPos) || isempty(indStartNeg)%case where it does not detect neg or pos beats

     %!!! don't know what ot put here; maybe nothing?
    
else%case where it does not detect neg or pos beats

    %erase first beat if amp is small
if indStartPos(1) < indStartNeg(1)
    
    firstBeatAmp = max(abs(cumsumInterpolatedAnglesGray(indStartPos(1):indEndPos(1),i)));
    
    if firstBeatAmp < startAmp*1.1%(firstBeatAmp - abs(startAmp)) < (firstBeatAmp*0.1)
        
        indStartPos(1) = [];
        indEndPos(1) = [];
        
        %remake ind binary array
        indBinaryPos2 = cumsumInterpolatedAnglesGray.*0;
        
        for ii = 1:length(indStartPos)

        indBinaryPos2(indStartPos(ii) : indEndPos(ii)-1) = 1;%don't know why -1 in ends...

        end

      
        indBinaryPos2 = find(indBinaryPos2);
        

        
        
    end

else
   
    firstBeatAmp = min(abs(cumsumInterpolatedAnglesGray(indStartNeg(1):indEndNeg(1),i)));
    
    if firstBeatAmp > startAmp*0.9 %(firstBeatAmp - abs(startAmp)) < (firstBeatAmp*0.1)
        
        indStartNeg(1) = [];
        indEndNeg(1) = [];
        
        %remake ind binary array
        indBinaryNeg2 = cumsumInterpolatedAnglesGray.*0;
        
        for ii = 1:length(indStartNeg)

        indBinaryNeg2(indStartNeg(ii) : indEndNeg(ii)-1) = 1;%don't know why -1 in ends...

        end

      
        indBinaryNeg2 = find(indBinaryNeg2);
        
    end
    
end
  
%make binary image
binaryBoutPos(indBinaryPos2,i) = 1;
binaryBoutNeg(indBinaryNeg2,i) = 1;

binaryImage(indBinaryPos2,i) = 1;
binaryImage(indBinaryNeg2,i) = -1;



%test erase 1st and last beat if they've small amplitude
startPos = cumsumInterpolatedAnglesGray*0;
startNeg = cumsumInterpolatedAnglesGray*0;
endPos = cumsumInterpolatedAnglesGray*0;
endNeg = cumsumInterpolatedAnglesGray*0;

binaryPos2 = cumsumInterpolatedAnglesGray(:,i)*0;
binaryNeg2 = cumsumInterpolatedAnglesGray(:,i)*0;

binaryPos2(indBinaryPos2) = 1;
binaryNeg2(indBinaryNeg2) = 1;


startPos(indStartPos) = 1;
startNeg(indStartNeg) = 1;

endPos(indEndPos) = 1;
endNeg(indEndNeg) = 1;
 
% figure
% hold off
% plot(cumsumInterpolatedAnglesGray(:,i), 'k.')
% hold on
% 
% % plot(binaryPos, 'color','b', 'linewidth', 4)
% % plot(binaryPos2, 'color','c', 'linewidth', 4)
% % plot(startPos, 'go','linewidth', 4)
% % plot(endPos, 'ro','linewidth', 4)
% 
% plot(binaryNeg, 'color','b','linewidth', 4)
% plot(binaryNeg2, 'color','c', 'linewidth', 4)
% plot(startNeg, 'go','linewidth', 4)
% plot(endNeg, 'ro','linewidth', 4)




end%case where it does not detect neg or pos beats

    
end%threshold detects nothing

    
end%loop through each segment
