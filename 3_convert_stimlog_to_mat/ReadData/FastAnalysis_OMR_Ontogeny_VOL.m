%% LOAD Tracking Log:

clear all;close all;clc
Folder='C:\Users\Adrien\Desktop\Pedro_Ontogeny_OMR_VirtualOpenLoop_Giant_Tu\Tu\4dpf\P3\'
filename='OMR_Ontogeny_VCL_24_02_19_Tu_Tank3764_C06_04dpf_P3_58_50_Atlas000'
load([Folder,filename,'.mat'])

Folder='C:\Users\Adrien\Desktop\Pedro_Ontogeny_OMR_VirtualOpenLoop_Giant_Tu\Tu\4dpf\P3\'
filename='stimlog_2019_02_24_13_00_17'

StimLog2Mat([Folder,filename,'.txt'])


load([Folder,filename,'.mat'])




%%


BufferId = a(:,1);
Pos = a(:,2:3);

figure;set(gcf,'WindowStyle','docked');set(gcf,'color','w');
h=line(Pos(:,1),Pos(:,2))
h.Color(4)=0.4;

BodyAngle = a(:,6);
MaxValue = a(:,7);
BlobValue = a(:,8);


midEyeX = a(:,9);
midEyeY = a(:,10);
hour=a(:,11);
minute=a(:,12);

StimuliIncr = a(:,13);
CumSumTail = a(:,14);

TailValues = a(:,15:24);

TailAngles = a(:,26:34);

FirstOrNot = a(:,35);

TrackEye = a(:,36);


TimerAcq = a(:,37);
lag = a(:,38);


%%

Tail=cumsum(TailAngles(:,1:8),2);

% Tail=(Tail-nanmean(Tail))/nanstd(Tail);

figure;set(gcf,'WindowStyle','docked');set(gcf,'color','w');
hold on
plot(Tail)
xlim([2.365*10^6,2.395*10^6])

%% Extract Value From StimLog:

for i=1:numel(StimLog.Names)
    
    tmp=cell2mat(StimLog.Names(i))
    assignin('base', tmp, StimLog.Values{i}.values);
end
FrameNumber=Id;

%% Check response to Stimuli:

idSt=find(diff(TimeCam)<0);
numel(idSt)
idSt=idSt(find((idSt+5)<numel(TimeCam)));
numel(idSt)

OrientationRange=unique(Orientation);
SpeedRange=unique(Speed_mm);
StimMat_orientation=zeros(1,numel(idSt));
StimMat_speed=zeros(1,numel(idSt));
for i=1:numel(idSt)
    StimMat_orientation(i)=Orientation(idSt(i));
    StimMat_speed(i)=Speed_mm(idSt(i));
end

%% Display Angle Trajectory
close all
clc

color_a=hsv(numel(OrientationRange));
for s=1:numel(SpeedRange)
    figure;set(gcf,'WindowStyle','docked');set(gcf,'color','w');
    subplot(1,5,[1 2 3 4])
    for a=1:numel(OrientationRange)
        ResponseMat=0;
        
        id_s=find(StimMat_speed==SpeedRange(s));
        id_a=find(StimMat_orientation==OrientationRange(a));
        id=intersect(id_s,id_a);
        numel(id)
        for i=1:numel(id)
            idCamSt=find(BufferId==FrameNumber(id(i)));
            FrCamLog=BufferId(idCamSt:idCamSt+700*5-1);
            theta=BodyAngle(FrCamLog);
            theta=unwrap(theta)*180/pi;
            if numel(theta)>0
                ResponseMat = [ResponseMat,ResponseMat(end)+theta'-theta(1)];
            end
        end
        plot((1:numel(ResponseMat))/700,ResponseMat,'Color',color_a(a,:))
        hold on
    end
    
    
    cmap = colormap(color_a) ; %Create Colormap
    cbh = colorbar ; %Create Colorbar
    cbh.Ticks = linspace(0,1,numel(OrientationRange)); %Create 8 ticks from zero to 1
    cbh.TickLabels = num2cell(OrientationRange) ;    %Replace the labels o'
    
    colorTitleHandle = get(cbh,'Title');
    titleString = 'OMR Orientation';
    set(colorTitleHandle ,'String',titleString);
    
    title(['Turning at ',int2str(SpeedRange(s)), 'mm/s'])
    
    xlabel('Time (mm/s)')
    ylabel('Angle (deg)')
    
    subplot(1,5,5)
    
    r = linspace(0,1,10);
    theta = linspace(0, 2*pi, 100);
    [rg, thg] = meshgrid(r,theta);
    [x,y] = pol2cart(thg,rg);
    pcolor(x,y,thg);
    xticks([])
    yticks([])
    colormap(hsv);
    %shading flat;
    axis equal;
    
    
end

%%
close all

color_a=hsv(numel(OrientationRange));
figure;set(gcf,'WindowStyle','docked');set(gcf,'color','w');

for a=1:numel(OrientationRange)
    
    ResponseMat=0;
    
    for s=1:numel(SpeedRange)
        
        id_s=find(StimMat_speed==SpeedRange(s));
        id_a=find(StimMat_orientation==OrientationRange(a));
        id=intersect(id_s,id_a);
        for i=1:numel(id)
            idCamSt=find(BufferId==FrameNumber(id(i)));
            FrCamLog=BufferId(idCamSt:idCamSt+700*5-1);
            theta=BodyAngle(FrCamLog);
            theta=unwrap(theta)*180/pi;
            if numel(theta)>0
                ResponseMat = [ResponseMat,ResponseMat(end)+theta'-theta(1)];
            end
        end
        
    end
    subplot(1,5,[1 2 3 4])
    
    plot((1:numel(ResponseMat))/700,ResponseMat,'Color',color_a(a,:))
    hold on
%     
%     cmap = colormap(color_a) ; %Create Colormap
%     cbh = colorbar ; %Create Colorbar
%     cbh.Ticks = linspace(0,1,numel(OrientationRange)); %Create 8 ticks from zero to 1
%     cbh.TickLabels = num2cell(OrientationRange) ;    %Replace the labels o'
%     
%     colorTitleHandle = get(cbh,'Title');
%     titleString = 'OMR Orientation';
%     set(colorTitleHandle ,'String',titleString);
%     
%     title(['Turning at ',int2str(SpeedRange(s)), 'mm/s'])
%     
%     xlabel('Time (mm/s)')
%     ylabel('Angle (deg)')
    
    subplot(1,5,5)
    
    r = linspace(0,1,10);
    theta = linspace(0, 2*pi, 100);
    [rg, thg] = meshgrid(r,theta);
    [x,y] = pol2cart(thg,rg);
    pcolor(x,y,thg);
    xticks([])
    yticks([])
    colormap(hsv);
    shading flat;
    axis equal;
    
end

%% Cumulative Angular Trajectory:

close all

% Loop for each stim and compute cumulative trajectory:


figure;set(gcf,'WindowStyle','docked');set(gcf,'color','w');
hold on;axis square

color_s=parula(numel(SpeedRange))

for s=1:numel(SpeedRange)
    TrialAverage=nan(numel(OrientationRange),14);
    
    for a=1:numel(OrientationRange)
        
        id=find(Orientation(idSt+10)==OrientationRange(a));
        id=intersect(id,find(Speed_mm(idSt+10)==speedval(s)));
        CumulAngle=[0];

        for i=1:numel(id)
            f=idSt(id(i)):idSt(id(i))+5*60;
            theta=Orientation(f)-Orientation(f(1));
            theta = unwrap(theta*pi/180)';
            theta=theta+CumulAngle(end);            
            TrialAverage(a,i)=theta(end);
        end
        
    end
    plot(Angle,nanmean(TrialAverage'),'-*','Color',color_s(s,:))
    
end


cmap = colormap(color_s) ; %Create Colormap
cbh = colorbar ; %Create Colorbar
cbh.Ticks = linspace(0,1,numel(SpeedRange)); %Create 8 ticks from zero to 1
cbh.TickLabels = num2cell(SpeedRange) ;    %Replace the labels o'

colorTitleHandle = get(cbh,'Title');
titleString = 'OMR Orientation';
set(colorTitleHandle ,'String',titleString);

title(['Turning at ',int2str(SpeedRange(s)), 'mm/s'])

xlabel('Time (mm/s)')
ylabel('Angle (deg)')





%%

figure
color=jet(size(OMRStimMat,1));

for a=1:numel(a)
    ResponseMat=0;
    
    for i=1:size(OMRStimMat,2)
        
        id=OMRStimMat(st,i);
        if ~isnan(id) && id>0
            if FrameNumber(id)>min(BufferId)
                idCamSt=find(BufferId==FrameNumber(id));
                theta=PursuitL(idCamSt:idCamSt+700*1-1);
                theta=unwrap(theta);
                %     FrCamLog=BufferId(idCamSt:idCamSt+700*3-1);
                %     FrStimLog=FrameNumber(id:id+60*3);
                %     AngleStimLog=FOrient(id:id+60*3);
                if numel(theta)>0
                    ResponseMat = [ResponseMat,ResponseMat(end)+theta'-theta(1)];
                end
            end
        end
    end
    plot(ResponseMat,'Color',color(st,:))
    hold on
end

%% Check OMR Response:

for i=1:numel(idSt)
    id=idSt(i)+5;
    if OMR(id)==1
        Orient=OMROrientation(id);
        j=find(Orientation==Orient);
        NumRep(j)=NumRep(j)+1;
        OMRStimMat(j, NumRep(j))=idSt(i)+1;
    end
end
imagesc(OMRStimMat)

%%


figure
color=hsv(size(OMRStimMat,1));

for st=1:size(OMRStimMat,1)
    ResponseMat=0;
    
    for i=1:size(OMRStimMat,2)
        
        id=OMRStimMat(st,i);
        if ~isnan(id) && id>0
            if FrameNumber(id)>min(BufferId)
                idCamSt=find(BufferId==FrameNumber(id));
                theta=PursuitL(idCamSt:idCamSt+700*1-1);
                theta=unwrap(theta);
                %     FrCamLog=BufferId(idCamSt:idCamSt+700*3-1);
                %     FrStimLog=FrameNumber(id:id+60*3);
                %     AngleStimLog=FOrient(id:id+60*3);
                if numel(theta)>0
                    ResponseMat = [ResponseMat,ResponseMat(end)+theta'-theta(1)];
                end
            end
        end
    end
    plot(ResponseMat,'Color',color(st,:))
    hold on
end
%%

figure
color=jet(size(OMRStimMat,1));

for st=1:size(OMRStimMat,1)
    ResponseMat=0;
    
    for i=1:size(OMRStimMat,2)
        
        id=OMRStimMat(st,i);
        if ~isnan(id) && id>0
            if  FrameNumber(id)>min(BufferId)
                idCamSt=find(BufferId==FrameNumber(id));
                theta=Drift(idCamSt:idCamSt+700*2-1);
                theta(diff(theta)==0)=[];
                %theta=unwrap(theta);
                %theta=theta(isnan(theta)==0);
                if numel(theta)>0
                    ResponseMat = [ResponseMat,ResponseMat(end)+theta'-theta(1)];
                end
            end
        end
    end
    plot(ResponseMat,'Color',color(st,:))
    hold on
end






%% 





%%




Speed=unique(OKRSpeed)

OMRStimMat=nan(numel(Speed),1);
NumRep=zeros(numel(Speed),1);

for i=1:numel(idSt)
    id=idSt(i)+5;
    if OKR(id)==1
        sp=OKRSpeed(id);
        j=find(Speed==sp);
        NumRep(j)=NumRep(j)+1;
        OKRStimMat(j, NumRep(j))=idSt(i)+1;
    end
end



%%
figure
color=jet(size(OKRStimMat,1));

for st=1:100%size(OKRStimMat,1)
    st
    ResponseMat=0;
    
    for i=1:size(OKRStimMat,2)
        
        id=OKRStimMat(st,i);
        if ~isnan(id) && id>0
            if  FrameNumber(id)>min(BufferId)
                idCamSt=find(BufferId==FrameNumber(id));
                theta=BodyAngle(idCamSt:idCamSt+700*0.5-1);
                theta=unwrap(theta);
                %     FrCamLog=BufferId(idCamSt:idCamSt+700*3-1);
                %     FrStimLog=FrameNumber(id:id+60*3);
                %     AngleStimLog=FOrient(id:id+60*3);
                ResponseMat = [ResponseMat,ResponseMat(end)+theta'-theta(1)];
            end
        end
    end
    plot(ResponseMat,'Color',color(st,:))
    hold on
end
%%





%% Check Eye and Tail Synchronisation

close all
for c=5
    figure(c)
    set(gcf,'WindowStyle','docked');
    hold on
    CategoryBout=allBoutStructure.BoutInf(:,EnumeratorBoutInf.boutCat);
    idBout=find(CategoryBout==c);
    
    for i=1:min(4000,numel(idBout))
        idSt=allBoutStructure.BoutInf(idBout(i),EnumeratorBoutInf.indBoutStartAllData);
        idSt=idSt-100;
        idEd=idSt+299;%allBoutStructure.BoutInf(idBout(i),EnumeratorBoutInf.indBoutEndAllData);
        if size(allBoutStructure.FishData,1)>idEd
            TailCurv=allBoutStructure.FishData(idSt:idEd,EnumeratorFishData.cumsumInterpFixedSegmentAngles(7));
            TailCurv=TailCurv-TailCurv(1);
            Sg=sign(allBoutStructure.BoutKinematicParameters(idBout(i),EnumeratorBoutKinPar.boutAngle));
            %TailCurv=TailCurv*Sg;
            if Sg>0
                %plot(TailCurv,'k','Linewidth',0.1)
                
                plt=scatter(sLeye(idSt),sLeye(idEd),1,'MarkerFaceColor','r','MarkerEdgeColor','r'); 
                
                %plt=line([0,1],[sLeye(idSt),sLeye(idEd)],'Color','r');
                %plt=plot((sLeye(idSt:idEd)-sLeye(idSt+100))*0.1,'r');
                %plt=plot((sLeye(idSt:idEd))*0.1,'r');
                
                plt=scatter(sReye(idSt),sReye(idEd),1,'MarkerFaceColor','b','MarkerEdgeColor','b'); 
              
                %plt=line([0,1],[sReye(idSt),sReye(idEd)],'Color','b');
                %plt=plot((sReye(idSt:idEd)-sReye(idSt+100))*0.1,'b');
                %plt=plot((sReye(idSt:idEd))*0.1,'b');
                
            end
        end
    end
end

%% Create 2D to 2D Function:

close all
c=5;

figure
set(gcf,'WindowStyle','docked');
hold on
CategoryBout=allBoutStructure.BoutInf(:,EnumeratorBoutInf.boutCat);
idBout=find(CategoryBout==c);

for i=1:min(4000,numel(idBout))
    idSt=allBoutStructure.BoutInf(idBout(i),EnumeratorBoutInf.indBoutStartAllData);
    idSt=idSt-100;
    idEd=idSt+299;%allBoutStructure.BoutInf(idBout(i),EnumeratorBoutInf.indBoutEndAllData);
    if size(allBoutStructure.FishData,1)>idEd
        Sg=sign(allBoutStructure.BoutKinematicParameters(idBout(i),EnumeratorBoutKinPar.boutAngle));
        if Sg<0
            U=[sReye(idSt),sLeye(idSt)];
            V=[sReye(idEd),sLeye(idEd)];
            plt=line([U(1),V(1)],[U(2),V(2)]);
            plt.Color(4) = 0.3;
            plt=scatter(U(1),U(2),10,'MarkerFaceColor','r','MarkerEdgeColor','r');
            plt=scatter(V(1),V(2),10,'MarkerFaceColor','b','MarkerEdgeColor','b');
        end
    end
end
xlim([-50,50])
ylim([-50,50])


figure
set(gcf,'WindowStyle','docked');
hold on
CategoryBout=allBoutStructure.BoutInf(:,EnumeratorBoutInf.boutCat);
idBout=find(CategoryBout==c);

for i=1:min(4000,numel(idBout))
    idSt=allBoutStructure.BoutInf(idBout(i),EnumeratorBoutInf.indBoutStartAllData);
    idSt=idSt-100;
    idEd=idSt+299;%allBoutStructure.BoutInf(idBout(i),EnumeratorBoutInf.indBoutEndAllData);
    if size(allBoutStructure.FishData,1)>idEd
        Sg=sign(allBoutStructure.BoutKinematicParameters(idBout(i),EnumeratorBoutKinPar.boutAngle));
        if Sg>0
            U=[sReye(idSt),sLeye(idSt)];
            V=[sReye(idEd),sLeye(idEd)];
            plt=line([U(1),V(1)],[U(2),V(2)]);
            plt.Color(4) = 0.3;
            plt=scatter(U(1),U(2),10,'MarkerFaceColor','r','MarkerEdgeColor','r');
            plt=scatter(V(1),V(2),10,'MarkerFaceColor','b','MarkerEdgeColor','b');
        end
    end
end
xlim([-50,50])
ylim([-50,50])

%% 

figure;
set(gcf,'WindowStyle','docked');
hold on
CategoryBout=allBoutStructure.BoutInf(:,EnumeratorBoutInf.boutCat);
idBout=find(CategoryBout==c);

for i=1:min(4000,numel(idBout))
    idSt=allBoutStructure.BoutInf(idBout(i),EnumeratorBoutInf.indBoutStartAllData);
    idSt=idSt-100;
    idEd=idSt+299;%allBoutStructure.BoutInf(idBout(i),EnumeratorBoutInf.indBoutEndAllData);
    if size(allBoutStructure.FishData,1)>idEd
        Sg=sign(allBoutStructure.BoutKinematicParameters(idBout(i),EnumeratorBoutKinPar.boutAngle));
        U=[sReye(idSt),sLeye(idSt)];
        if Sg>0
            plt=scatter(U(1),U(2),10,'MarkerFaceColor','r','MarkerEdgeColor','r');
        else    
            plt=scatter(U(1),U(2),10,'MarkerFaceColor','b','MarkerEdgeColor','b');
        end
    end
end
xlim([-50,50])
ylim([-50,50])
%%
c=5
figure;
set(gcf,'WindowStyle','docked');
hold on
CategoryBout=allBoutStructure.BoutInf(:,EnumeratorBoutInf.boutCat);
idBout=find(CategoryBout==c);

for i=1:min(4000,numel(idBout))
    idSt=allBoutStructure.BoutInf(idBout(i),EnumeratorBoutInf.indBoutStartAllData);
    idSt=idSt-100;
    idEd=idSt+299;%allBoutStructure.BoutInf(idBout(i),EnumeratorBoutInf.indBoutEndAllData);
    if size(allBoutStructure.FishData,1)>idEd
        Sg=sign(allBoutStructure.BoutKinematicParameters(idBout(i),EnumeratorBoutKinPar.boutAngle));
        U=[sReye(idEd),sLeye(idEd)];
        if Sg>0
            plt=scatter(U(1),U(2),10,'MarkerFaceColor','r','MarkerEdgeColor','r');
        else    
            plt=scatter(U(1),U(2),10,'MarkerFaceColor','b','MarkerEdgeColor','b');
        end
    end
end
xlim([-50,50])
ylim([-50,50])





%% Check Eye Angle Response

figure
color=jet(size(OKRStimMat,1));

for st=1:size(OKRStimMat,1)
    ResponseMat=0;
    
    for i=1:size(OKRStimMat,2)-1
        
        id=OKRStimMat(st,i);
        if ~isnan(id) && id>0
            if  FrameNumber(id)>min(BufferId)
                idCamSt=find(BufferId==FrameNumber(id));
                theta=PursuitL(idCamSt:idCamSt+700*2-1);
                theta(diff(theta)==0)=[];
                %theta=unwrap(theta);
                %theta=theta(isnan(theta)==0);
                if numel(theta)>0
                    ResponseMat = [ResponseMat,ResponseMat(end)+theta'-theta(1)];
                end
            end
        end
    end
    plot(ResponseMat,'Color',color(st,:))
    hold on
end

TotTime=7*10^4/700;
Slope=40000/TotTime

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%  EYE TAIL AND STIMULI %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


figure
color=jet(size(OKRStimMat,1));

idStartBouts=allBoutStructure.BoutInf(:,EnumeratorBoutInf.indBoutStartAllData);
tmp=unwrap(BodyAngle)*180/pi;

for st=1:size(OKRStimMat,1)
    ResponseMat=0;
    TurnAngleBouts=[];
    EyeTurnL=[];
    EyeTurnR=[];
    
    for i=1:size(OKRStimMat,2)
        
        id=OKRStimMat(st,i);
        if ~isnan(id) && id>0
            if  FrameNumber(id)>min(BufferId)
                
                idCamSt=find(BufferId==FrameNumber(id));
                
                % Find First Bouts:
                j=find(idStartBouts>idCamSt,1,'first');
                
                idSt = idStartBouts(j)-30;
                
                idEd=idSt+199;
                TurnAngleBouts=[TurnAngleBouts,tmp(idEd)-tmp(idSt)];
                EyeTurnL=[EyeTurnL,sLeye(idEd)-sLeye(idSt)];
                EyeTurnR=[EyeTurnR,sReye(idEd)-sReye(idSt)];
                % Compute Turn Angle and Saccade for the Bouts:      
            end
        end
    end
    hold on
    scatter(TurnAngleBouts,EyeTurnL,30,color(st,:),'filled')

end

xlim([-90,90])
ylim([-20,20])            



%%
TurnAngleBouts=zeros(1,size(allBoutStructure.BoutInf,1));
EyeTurnL=zeros(1,size(allBoutStructure.BoutInf,1));
EyeTurnR=zeros(1,size(allBoutStructure.BoutInf,1));
tmp=unwrap(BodyAngle)*180/pi;

for i=1:size(allBoutStructure.BoutInf,1)
    i
    idSt=allBoutStructure.BoutInf(i,EnumeratorBoutInf.indBoutStartAllData);
    idSt=idSt-30
    idEd=idSt+199;
    TurnAngleBouts(i)=tmp(idEd)-tmp(idSt);
    EyeTurnL(i)=sLeye(idEd)-sLeye(idSt);
    EyeTurnR(i)=sReye(idEd)-sReye(idSt);
    
end


for st=1:size(OKRStimMat,1)
    ResponseMat=0;
    
    for i=1:size(OKRStimMat,2)
        
        id=OKRStimMat(st,i);
        if ~isnan(id) && id>0
            if  FrameNumber(id)>min(BufferId)
                idCamSt=find(BufferId==FrameNumber(id));
                theta=PursuitR(idCamSt:idCamSt+700*2-1);
                theta(diff(theta)==0)=[];
                %theta=unwrap(theta);
                %theta=theta(isnan(theta)==0);
                if numel(theta)>0
                    ResponseMat = [ResponseMat,ResponseMat(end)+theta'-theta(1)];
                end
            end
        end
    end
    plot(ResponseMat,'Color',color(st,:))
    hold on
end

TotTime=7*10^4/700;
Slope=500/TotTime
%%




%%
CategoryBout=allBoutStructure.BoutInf(:,EnumeratorBoutInf.boutCat);
idBout=find(CategoryBout==c);
for i=1:min(100,numel(idBout))
    idSt=allBoutStructure.BoutInf(idBout(i),EnumeratorBoutInf.indBoutStartAllData);
    idEd=idSt+199;%allBoutStructure.BoutInf(idBout(i),EnumeratorBoutInf.indBoutEndAllData);
    if size(allBoutStructure.FishData,1)>idEd
        TailCurv=allBoutStructure.FishData(idSt:idEd,EnumeratorFishData.cumsumInterpFixedSegmentAngles(7));
        TailCurv=TailCurv-TailCurv(1);
        Sg=sign(allBoutStructure.BoutKinematicParameters(idBout(i),EnumeratorBoutKinPar.boutAngle));
        TailCurv=TailCurv*Sg;
        MatTailMvt=[MatTailMvt;TailCurv'];
        IdTailMvt=[IdTailMvt,c];
    end
end



%% Compute OMR Orientation:
if Large==1
    FOrientOrigin=StimLog.Values{13}.values;
    FOrientUpdate=StimLog.Values{10}.values;
end

id=find(OMR==1);
id=intersect(id,find(OMRSpeed>0));

OMROrientation=OMROrientation(1:numel(FOrientUpdate));
AngleWithBar=OMROrientation*pi/180+(FOrientUpdate-FOrientOrigin);
AngleWithBar=atan2(sin(AngleWithBar),cos(AngleWithBar));
Turn=unwrap(FOrientUpdate);
Turn=smooth(Turn,200);
Turn=[0;diff(Turn)];

scatter(AngleWithBar,Turn,1,'filled')

%% Find Bouts:






%%





%
% %%
%
% scatter(Pos(:,1),Pos(:,2),1,'b','filled','MarkerEdgeColor',[0,0,1])
%
%
% %%
% idSt=find(diff(GlobalTime)<0);
%
% idCamSt=nan(size(idSt));
%
% for i=1:numel(idSt)
%     if find(FrameNumber==idSt(i))>0
%         idCamSt(i)=find(BufferId==FrameNumber(idSt(i)));
%     end
% end
%
% scatter(FishPos(idCamSt,1),FishPos(idCamSt,2),2,'b','filled','MarkerEdgeColor',[0,0,1])