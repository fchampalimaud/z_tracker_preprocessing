%% Stimuli Player MATLAB Interface
% OrgerLab 2018
% @author: Alexandre Laborde
% @version: 1.0.3

% ----- Changes -----
% 24/Oct/2017 - v1.0.1
%   - Added GetShader Command.
%   - Changed Get command to GetUniform.
% 25/Oct/2017 - v1.0.2
%   - Reenabled starting external C# player from the constructor.
%   - Added bool to constructor to open C# player or not.
%   - Fixed typo on the GetUniform command.
%   - Fixed typo on the GetShader command.
% 16/Mar/2018 - v1.0.3
%   - Added error message -42 for missing tags in the protocol.


% ----- ----- -----

classdef StimuliPlayerSession
    %StimuliPlayerSession Interface for the C# Stimuli Player
    
    %% Properties
    properties (Access = private)
        IP = '127.0.0.1';
        PORT = 13000;
        SIZE_OF_MSG_SIZE = 4; %number of bytes that contain the size of the message payload
        tcp;
    end
    
    properties (Constant)
        PATH_TO_APPREF = fullfile(getenv('AppData'),'Microsoft\\Windows\\Start Menu\\Programs\\Orger Lab\\Stimuli Player.appref-ms');
        START_PATH = fullfile(getenv('LocalAppData'), 'Apps\\2.0');
    end
    
    %% Private methods
    methods (Access = private)
        
        function [] = SendData(obj,data)
            fwrite(obj.tcp,data);
        end
        
        function data = ReceiveData(obj,number_of_bytes)
            %RECEIVEDATA Receives number_of_bytes bytes from the tcp socket.
            %returns a uint8 array with the read data
            
            data = zeros([number_of_bytes,1],'uint8');
            read_bytes = 0;
            
            buffer_size = obj.tcp.InputBufferSize;
            
            while read_bytes < number_of_bytes
                b = min( number_of_bytes - read_bytes, buffer_size);
                
                bytes = fread( obj.tcp,b,'uint8' );
                n = read_bytes + length(bytes);
                data(read_bytes + 1 : n ) = bytes;
                read_bytes = n ;
            end
        end
        
        function data = ReceiveResponse(obj)
            %RECEIVERESPONSE Receives the response and raises errors if error message is received.
            %returns the unparsed message as uint8 array.
            
            raw_message_length = obj.ReceiveData(obj.SIZE_OF_MSG_SIZE);
            
            message_length = typecast(raw_message_length,'int32');
            
            if message_length <= 0
                obj.DisplayError(message_length);
            end
            
            data = obj.ReceiveData(double(message_length));
        end
        
        function [] = DisplayError( obj,  error_code)
            %DISPLAYERROR Converts the error code to a meaningful error message.
            
            switch error_code
                % GENERIC
                case 0
                    disp('Success.');
                case -1
                    error('Unknown error.');
                    % COMMAND PARSING
                case -10
                    error('Invalid command.');
                case -11
                    error('Invalid number of parameters.');
                    
                    % START CONDITION VALIDATION
                case -20
                    error('Starting frame number not an integer.');
                case -21
                    error('Starting time not a number.');
                case -22
                    error('Starting time is larger than the maximum allowed.');
                case -23
                    error('Start unit is unknown.');
                    
                    % STOP CONDITION VALIDATION
                case -30
                    error('Stopping frame number not an integer.');
                case -31
                    error('Stopping time not a number.');
                case -32
                    error('Stopping unit is unknown.');
                case -33
                    error('Stopping time is out of bonds.');
                case -34
                    error('Active shader changed in the selected interval.');
                    
                    % LOAD COMMAND
                case -40
                    error('File not found.');
                case -41
                    error('Protocol not loaded. Load protocol first with command LOAD.');
                case -42
                    error('No shaders were found. Please be sure your protocol has the @shaders and @log tags in the proper locations.');
                    
                    % RENDER COMMAND
                case -50
                    error('Invalid color scheme encoding detected.');
            end
        end
    end
    
    methods (Static , Access = private)
        
        function parsed_data = ParseGetUniformResponse(data)
            %PARSEGETUNIFORMRESPONSE Parses the response form the get command
            %returns the uniforms values over time
            
            data = uint8(data);
            sizeof_int32 = 4; % Size of a int32 in bytes;
            sizeof_float = 4; % Size of a float in bytes;
            uniform_name_terminator = 10; % char \n
            number_of_timepoints = typecast(data(1:sizeof_int32), 'int32');
            u = 1;
            parsed_data = struct;
            index = sizeof_int32+1;
            
            while index < length(data)
                start = index;
                
                %% Parse Name
                while data(index) ~= uniform_name_terminator; index = index + 1; end
                
                
                parsed_data(u).name = char(data(start:index-1)');
                
                index = index + 1;
                
                %% Parse values
                k = 1;
                values = zeros([number_of_timepoints,1],'single');
                for index = index : sizeof_float : index + sizeof_float * ( number_of_timepoints - 1 )
                    values(k) = typecast(data(index:index + sizeof_float-1), 'single');
                    k = k + 1;
                end
                
                index = index + sizeof_float;
                
                parsed_data(u).values =  values;
                u = u + 1;
                
            end
        end
        
        function parsed_data = ParseRenderResponse(image_data,image_size,color_channels)
            %PARSERENDERRESPONSE Parses the response for the render command
            %returns a 4D - [ T X Y C] matrix with the rendered images
            
            number_of_images = length(image_data) / (sum(color_channels)*(image_size^2));
            parsed_data =  zeros([image_size,image_size, sum(color_channels) , number_of_images ],'uint8');
            
            for c  = 1 : sum(color_channels)
                parsed_data(:,:,c,:) = reshape(...
                    image_data(c:sum(color_channels):end),...
                    image_size,...
                    image_size,...
                    1,...
                    number_of_images);
            end
            
            parsed_data = permute(parsed_data,[4 1 2 3]);
            
            for k = 1:number_of_images
                parsed_data(k,:,:,:) = rot90(squeeze(parsed_data(k,:,:,:)));
            end
        end
        
        function [shader_names, active_shader_index] = ParseGetShaderResponse(data)
            %PARSEGETSHADEERRESPONSE Parses the response form the get shader command
            %returns the names of the active shaders over time
            data = uint8(data);
            terminator = 10; % char \n
            number_of_shaders = data(1);
            
            shader_names = cell(number_of_shaders,1);
            
            shaders_found = 0;
            index = 2;
            while shaders_found < number_of_shaders
                name = '';
                
                while data(index) ~= terminator;
                    name = strcat(name,char(data(index)));
                    index = index + 1;
                end
                index = index + 1;
                
                shaders_found = shaders_found + 1;
                shader_names{shaders_found} = name;
            end
            
            active_shader_index = data(index:end);
            
        end
        
        % RDIR Stuff
        function [varargout] = rdir(rootdir,varargin)
            if ~exist('rootdir','var'),
                rootdir = '*';
            end
            
            prepath = '';       % the path before the wild card
            wildpath = '';      % the path wild card
            postpath = rootdir; % the path after the wild card
            I = find(rootdir==filesep,1,'last');
            
            % Directory separator for current platform
            if filesep == '\'
                % On PC, filesep is '\'
                anti_filesep = '/';
            else
                % On UNIX system, filesep is '/'
                anti_filesep = '\';
            end
            
            if isempty(I) && ~isempty(strfind(rootdir, anti_filesep))
                error([mfilename, ':FileSep'],...
                    'Use correct directory separator "%s".', filesep)
            end
            
            if ~isempty(I),
                prepath = rootdir(1:I);
                postpath = rootdir(I+1:end);
                I = find(prepath=='*',1,'first');
                if ~isempty(I),
                    postpath = [prepath(I:end) postpath];
                    prepath = prepath(1:I-1);
                    I = find(prepath==filesep,1,'last');
                    if ~isempty(I),
                        wildpath = prepath(I+1:end);
                        prepath = prepath(1:I);
                    end;
                    I = find(postpath==filesep,1,'first');
                    if ~isempty(I),
                        wildpath = [wildpath postpath(1:I-1)];
                        postpath = postpath(I:end);
                    end;
                end;
            end;
            
            if isempty(wildpath)
                % If no directory wildcards then just get files and directories list
                
                D = dir([prepath postpath]);
                
                % Exclude ".", ".." and ".svn" directories from the list
                excl = StimuliPlayerSession.isdotdir(D) | StimuliPlayerSession.issvndir(D);
                D(excl) = [];
                
                if isdir([prepath postpath]);
                    fullpath = [prepath postpath];
                else
                    fullpath = prepath;
                end
                
                % Place directories on the top of the list
                is_dir = [D.isdir]';
                D = [D(is_dir); D(~is_dir)];
                
                % Add path before name
                for ii = 1:length(D)
                    D(ii).name = fullfile(fullpath, D(ii).name);
                end
                
                % disp(sprintf('Scanning "%s"   %g files found',[prepath postpath],length(D)));
                
            elseif strcmp(wildpath,'**')
                % A double wildcards directory means recurs down into sub directories
                
                % first look for files in the current directory (remove extra filesep)
                D = StimuliPlayerSession.rdir([prepath postpath(2:end)]);
                
                % then look for sub directories
                D_sd = dir([prepath '*']);
                
                % Exclude ".", "..", ".svn" directories and files from the list
                excl = StimuliPlayerSession.isdotdir(D_sd) | StimuliPlayerSession.issvndir(D_sd) | ~([D_sd.isdir]');
                D_sd(excl) = [];
                
                % Process each sub directory found
                % Performance tweak: avoid growing array within loop (X. Mo)
                c_D = arrayfun(@(x) StimuliPlayerSession.rdir([prepath x.name filesep wildpath postpath]),...
                    D_sd, 'UniformOutput', false);
                
                D = [D; cell2mat( c_D ) ];
                
            else
                % Process directory wild card looking for sub directories that match
                
                D_sd = dir([prepath wildpath]);
                
                % Exclude ".", "..", ".svn" directories and files from the list
                excl = StimuliPlayerSession.isdotdir(D_sd) | StimuliPlayerSession.issvndir(D_sd) | ~([D_sd.isdir]');
                D_sd(excl) = [];
                
                if ~isdir(prepath) || ( numel(D_sd)==1 && strcmp(D_sd.name, prepath))
                    prepath = '';
                end
                
                % Process each directory found
                Dt = dir('');
                
                c_D = arrayfun(@(x) StimuliPlayerSession.rdir([prepath x.name postpath]),...
                    D_sd, 'UniformOutput', false);
                
                D = [Dt; cell2mat( c_D ) ];
                
            end
            
            nb_before_filt = length(D);
            warning_msg = '';
            
            if (nargin>=2 && ~isempty(varargin{1})),
                try
                    if isa(varargin{1}, 'function_handle')
                        test_tf = arrayfun(varargin{1}, D);
                    else
                        test_tf = StimuliPlayerSession.evaluate(D, varargin{1});
                    end
                    
                    D = D(test_tf);
                    
                catch
                    if isa(varargin{1}, 'function_handle')
                        test_expr = func2str(varargin{1});
                    else
                        test_expr = varargin{1};
                    end
                    
                    warning_msg = sprintf('Invalid TEST "%s" : %s', test_expr, lasterr);
                end
            end
            
            
            common_path = '';
            if (nargin>=3 && ~isempty(varargin{2})),
                
                arg2 = varargin{2};
                if ischar(arg2)
                    common_path = arg2;
                elseif (isnumeric(arg2) || islogical(arg2)) && arg2
                    common_path = prepath;
                end
                
                rm_path = regexptranslate('escape', common_path);
                
                % Check that path is common to all
                start = regexp({D.name}', ['^', rm_path]);
                
                % Convert to a logical.
                is_common = not( cellfun(@isempty, start) );
                
                if all(is_common)
                    for k = 1:length(D)
                        D(k).name = regexprep(D(k).name, ['^', rm_path], '');
                    end
                    
                else
                    common_path = '';
                end
                
                
            end
            
            
            nout = nargout;
            
            if nout == 0
                if isempty(D)
                    if nb_before_filt == 0
                        fprintf('%s not found.\n', rootdir)
                    else
                        fprintf('No item matching filter.\n')
                    end
                else
                    
                    if ~isempty(common_path)
                        fprintf('All in : %s\n', common_path)
                    end
                    
                    pp = {'' 'k' 'M' 'G' 'T'};
                    for ii = 1:length(D)
                        if D(ii).isdir
                            % Directory item : display name
                            disp(sprintf(' %29s %-64s','',D(ii).name));
                        else
                            % File item : display size, modification date and name
                            sz = D(ii).bytes;
                            if sz > 0
                                ss = min(4,floor(log2(sz)/10));
                            else
                                ss = 0;
                            end
                            disp(sprintf('%4.0f %1sb  %20s  %-64s ',...
                                sz/1024^ss, pp{ss+1}, datestr(D(ii).datenum, 0), D(ii).name));
                        end
                    end
                end
            elseif nout == 1
                % send list out
                varargout{1} = D;
            else
                % send list and common path out
                varargout{1} = D;
                varargout{2} = common_path;
            end;
            
            if ~isempty(warning_msg)
                warning([mfilename, ':InvalidTest'],...
                    warning_msg); % ap aff
            end
        end
        function tf = issvndir(d)
            is_dir = [d.isdir]';
            
            is_svn = strcmp({d.name}, '.svn')';
            %is_svn = false; % uncomment to disable ".svn" filtering
            
            tf = (is_dir & is_svn);
        end
        function tf = isdotdir(d)
            % True for "." and ".." directories.
            % d is a structure returned by "dir"
            %
            
            is_dir = [d.isdir]';
            
            is_dot = strcmp({d.name}, '.')';
            is_dotdot = strcmp({d.name}, '..')';
            
            tf = (is_dir & (is_dot | is_dotdot) );
        end
        function tf = evaluate(d, expr)
            name = {d.name}'; %#ok<NASGU>
            date = {d.date}'; %#ok<NASGU>
            datenum = [d.datenum]'; %#ok<NASGU>
            bytes = [d.bytes]'; %#ok<NASGU>
            isdir = [d.isdir]'; %#ok<NASGU>
            
            tf = eval(expr); % low risk since done in a dedicated subfunction.
            
            
            if iscell(tf)
                tf = not( cellfun(@isempty, tf) );
            end
        end
        % END RDIR Stuff
        
        function path = FindApplicationPath()
            fID = fopen(StimuliPlayerSession.PATH_TO_APPREF);
            text = fread(fID, inf, 'uint16=>char')';
            fclose(fID);
            
            match = regexp(text, '(PublicKeyToken=)([^,]*)','tokens');
            public_key_token = match{1}{2};
            
            possible_dirs = StimuliPlayerSession.rdir([StimuliPlayerSession.START_PATH '\**\'],'isdir');
            for k = 1:length(possible_dirs)
                if ~isempty(regexp(possible_dirs(k).name, public_key_token, 'once'))
                    if isempty(regexp(possible_dirs(k).name, 'Apps\\2.0\\Data', 'once'))
                        if isempty(regexp(possible_dirs(k).name, '_none_', 'once'))
                            player_directory = possible_dirs(k).name;
                            break
                        end
                    end
                end
            end
            assert(exist('player_directory','var') == 1, 'Stimuli Player software not found.');
            
            path = fullfile(player_directory, 'Stimuli Player.exe');
        end
    end
    
    %% Public Methods
    methods(Static, Access=public)
        function path = FindPath()
            path = StimuliPlayerSession.FindApplicationPath();
        end
    end
    
    methods (Access = public)
        
        function obj = StimuliPlayerSession(start_player)
            %STIMULIPLAYERSESSION Creates a StimuliPlayerSession object with default settings.

            if start_player
            system([StimuliPlayerSession.FindApplicationPath() '&']);
        end
            
            obj.tcp = tcpip(obj.IP,obj.PORT,'InputBufferSize',8096);
            fopen(obj.tcp);
        end
        
        function [] = Load(obj,path)
            %LOAD Sends the load command to the C# server.
            
            message = strcat({'load '}, path);
            obj.SendData(message{1});
            obj.ReceiveResponse();
            
        end
        
        function parsed_data = GetUniform(obj,startTimeSpan,start,stopTimeSpan,stop,uniforms)
            %GETUNIFORM Sends the get command to the C# server.
            %returns the parsed data for the values of uniforms for a given time span.
            
            if ~isa(startTimeSpan,'TimeSpan')
                error('startTimeSpan must be a TimeSpan.');
            end
            
            if ~isa(stopTimeSpan,'TimeSpan')
                error('stopTimeSpan must be a TimeSpan.');
            end
            
            if ~isnumeric(start)
                error('start must be a number.');
            end
            
            if ~isnumeric(stop)
                error('stop must be a number.');
            end
            
            if exist('uniforms','var') && ~isa(uniforms,'cell')
                error('uniforms must be a cell array.');
            end
            
            message = {'get_uniform '};
            if exist('uniforms','var')
                for n = 1:length(uniforms)
                    message = strcat(message,uniforms{n},{' '});
                end
            end
            
            message = strcat(message,{char(uint8(startTimeSpan))},{' '},...
                {num2str(start)} ,{' '},...
                {char(uint8(stopTimeSpan))},{' '},...
                {num2str(stop)});
            
            obj.SendData(message{1});
            uniform_data = obj.ReceiveResponse();
            parsed_data = obj.ParseGetUniformResponse(uniform_data);
        end
        
        function [shader_names, active_shader_index] = GetShader(obj,startTimeSpan,start,stopTimeSpan,stop)
            %GETSHADER Sends the get command to the C# server.
            %returns the parsed data for the values of uniforms for a given time span.
            message = {'get_shader'};
            
            if exist('startTimeSpan','var') && exist('start','var') && ...
                    exist('stopTimeSpan','var') && exist('stop','var')
                
                if ~isa(startTimeSpan,'TimeSpan')
                    error('startTimeSpan must be a TimeSpan.');
                end
                
                if ~isa(stopTimeSpan,'TimeSpan')
                    error('stopTimeSpan must be a TimeSpan.');
                end
                
                if ~isnumeric(start)
                    error('start must be a number.');
                end
                
                if ~isnumeric(stop)
                    error('stop must be a number.');
                end
                
                if exist('uniforms','var') && ~isa(uniforms,'cell')
                    error('uniforms must be a cell array.');
                end
                
                message = strcat(message,{' '}, {char(uint8(startTimeSpan))},{' '},...
                    {num2str(start)} ,{' '},...
                    {char(uint8(stopTimeSpan))},{' '},...
                    {num2str(stop)});
            else
                message = strcat(message);
            end
            
            obj.SendData(message{1});
            uniform_data = obj.ReceiveResponse();
            [shader_names, active_shader_index] = obj.ParseGetShaderResponse(uniform_data);
        end
        
        
        function parsed_data = Render(obj,startTimeSpan,start,stopTimeSpan,stop,color_channels)
            %RENDER Sends the render command to the C# server.
            %returns the parsed data for the values of uniforms for a given time span.
            
            if ~isa(startTimeSpan,'TimeSpan')
                error('startTimeSpan must be a TimeSpan.');
            end
            
            if ~isa(stopTimeSpan,'TimeSpan')
                error('stopTimeSpan must be a TimeSpan.');
            end
            
            if ~isnumeric(start)
                error('start must be a number.');
            end
            
            if ~isnumeric(stop)
                error('stop must be a number.');
            end
            
            if ~isnumeric(color_channels) && length(color_channels) ~= 3
                error('colors must be a size 3 numeric array.');
            end
            
            image_size = 500;
            message = strcat({'render '},{num2str(image_size)} ,{' '},...
                {char(uint8(startTimeSpan))},{' '},...
                {num2str(start)} ,{' '},...
                {char(uint8(stopTimeSpan))},{' '},...
                {num2str(stop)},{' '},...
                {num2str(bi2de(color_channels,'left-msb'))});
            
            obj.SendData(message{1});
            image_data = obj.ReceiveResponse();
            parsed_data = obj.ParseRenderResponse(image_data,image_size,color_channels);
        end
    end
end