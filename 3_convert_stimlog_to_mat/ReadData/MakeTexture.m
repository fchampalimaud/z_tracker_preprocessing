function MakeTexture(image, filename)

image = uint32(image);

imageSize = size(image);
totalSize = prod(imageSize);
a=fopen(filename, 'w');
fprintf(a, '#version 430\n\nin vec2 fragCoord;\nout vec4 outputColor;\n\nuniform float iGlobalTime;\n\nvec2 imageSize = vec2(%d, %d);\n', ...
        imageSize(1), imageSize(2));

numberOfRegions = ceil(totalSize / 15876);

for r = 1:numberOfRegions
    if (r == numberOfRegions)
        currentSize = totalSize - (numberOfRegions-1)*15876;
    else
        currentSize = 15876;
    end
    fprintf(a, 'uint texture%d[%d] = {', r-1, ceil(currentSize/4));
    
    for k = 1:4:currentSize

        b1 = image(k+(r-1)*15876);
        if (k+1 <= currentSize)
            b2 = bitshift(image(k+1+(r-1)*15876),8);
            if (k+2 <= currentSize)
                b3 = bitshift(image(k+2+(r-1)*15876),16);
                if (k+3 <= currentSize)
                    b4 = bitshift(image(k+3+(r-1)*15876),24);
                else
                    b4 = 0;
                end
            else
                b3 = 0; b4 = 0;
            end
        else
            b2 = 0; b3 = 0; b4 = 0;
        end

        fprintf(a, '%d,', b1+b2+b3+b4);
    end
    fprintf(a,'};\n\n');
end





fprintf(a, ['const uint  bitShifts[4]= {255, 65280, 16711680, 4278190080};\n\n\n' ...
'uint ReturnTextureValue(int arrayPosition)\n{\n\tint textureNumber = arrayPosition / 3969;\n' ...
'\tint position = arrayPosition - textureNumber*3969;\n\tuint value;\n\tswitch (textureNumber)\n\t{\n']);
for r = 1:numberOfRegions
    fprintf(a, '\t\tcase %d:\n', r-1);
    fprintf(a, '\t\t\tvalue = texture%d[position];\n\t\t\tbreak;\n', r-1);
end
fprintf(a, ['\t}\n\treturn value;\n}\n\n' ...
'float ReturnPixelColor(vec2 position)\n{\n\tint arrayPosition = int(position.y + position.x*imageSize.y);\n' ...
'\tint bitPosition = arrayPosition - (arrayPosition/4)*4;\n\tarrayPosition = arrayPosition / 4;\n\n' ...
'\tuint color = (ReturnTextureValue(arrayPosition) & bitShifts[bitPosition]) >> (8*bitPosition);\n' ...
'\treturn float(color)/255.;\n}\n\n' ...
'void main()\n{\n\tvec2 position = floor(imageSize * (vec2(fragCoord.x, -fragCoord.y)+ 1.0) / 2.);\n' ...
'\tfloat pixelColor = ReturnPixelColor(position);\n' ...
'\toutputColor = vec4(pixelColor, pixelColor, pixelColor, 1.0);\n}\n']);

fclose(a);