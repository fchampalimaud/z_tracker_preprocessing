%% Convert StimData to .mat
function StimLog2Mat(filename)
%

%filename='D:\ExperimentalData\DataPreyCaptureRig\OMR_Grating_Center\OMR_Grating_Center_1100002_171208_111101_7_700_28_Tu_1_39_1_stimlog_2017_12_08_14_13_30.txt';
%filename='D:\ExperimentalData\OMR_Grating_Center\OMR_Grating_Center_1100002_171208_111101_7_700_28_Tu_1_39_2_stimlog_2017_12_08_15_40_32.txt'
sps=StimuliPlayerSession(1);

sps.Load(filename)

% Getting the length of the shader log:
[~,activeSh] = sps.GetShader();

LogLength=numel(activeSh)
% Getting the uniforms names
UniformsNames=[];

uniforms = sps.GetUniform(TimeSpan.Frame,LogLength-1, TimeSpan.Frame,LogLength);

for i=1:numel(uniforms)
    UniformsNames{i}=uniforms(i).name
end

% Getting the uniforms values
for i=1:numel(uniforms)
    UniformsValues{i} = sps.GetUniform(TimeSpan.Frame,0, TimeSpan.Frame,LogLength,{UniformsNames{i}});
end

StimLog.Length=LogLength;
StimLog.Names=UniformsNames;
StimLog.Values=UniformsValues;

save(strrep(filename, '.txt', '.mat'), 'StimLog')%,'-v7.3')

