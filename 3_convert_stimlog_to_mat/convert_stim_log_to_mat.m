function convert_stim_log_to_mat(Table)

    addpath('./ReadData')

    
    for i=1:size(Table,1)
        i
        
        folder=cell2mat(Table.fish_folder_local(i));
        filename=cell2mat(Table.fish_stimlog_filename(i));
        if exist([folder,'\',filename,'.mat'])~=2
            StimLog2Mat([folder,'\',filename,'.txt'])
        end
    end

end




