############################
###### Library Import ######
############################

# Data Wrangling

import pandas as pd

import numpy as np
import numpy_indexed as npi

from collections import Counter

# Displaying

import matplotlib.pyplot as plt
from matplotlib.colors import Normalize
from matplotlib import cm
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.gridspec as gridspec
from matplotlib import colors

import seaborn as sns
sns.set_style("whitegrid")
sns.set_style("ticks")

from IPython.display import clear_output, Image, display

from tqdm import tqdm_notebook as tqdm

# Os :
import os
import h5py


from pathlib import Path
import scipy.io as sio
import warnings
warnings.filterwarnings('ignore')
# Timing
from dateutil.parser import parse
import time

# Math
import random

# Statistics

from scipy.stats import gaussian_kde as kde
from scipy import signal
import statsmodels.stats.diagnostic as sm


# Option for Panda:

# To Display the large number of Colums present:
pd.options.display.max_columns = 99
pd.options.display.max_rows =20


###########################
#### Reading Files ########
###########################

## Convert filename from Local Ubuntu bash to windows and back:
def ConvertFilenameWindows2Linux(filename):
    filename=filename.replace('\\','/')
    Disk=filename[0]
    filename=filename[2:]
    filename='/mnt/'+Disk.lower()+filename
    return filename

def ConvertFilenameLinux2Windows(filename):
    id=filename[len('/mnt/')]
    Disk=filename[len('/mnt/')].upper()+':'+r'\\'
    filename=Disk+filename[len('/mnt/')+2:]
    filename=filename.replace('/',r'\\')
    return filename

def reduce_mem_usage(props):
    start_mem_usg = props.memory_usage().sum() / 1024**2 
    #print("Memory usage of properties dataframe is :",start_mem_usg," MB")
    NAlist = [] # Keeps track of columns that have missing values filled in. 
    for col in props.columns:
        if props[col].dtype != object:  # Exclude strings
            
            # Print current column type
            #print("******************************")
            #print("Column: ",col)
            #print("dtype before: ",props[col].dtype)
            
            # make variables for Int, max and min
            IsInt = False
            mx = props[col].max()
            mn = props[col].min()
            '''
            # Integer does not support NA, therefore, NA needs to be filled
            if not np.isfinite(props[col]).all(): 
                NAlist.append(col)
                props[col].fillna(mn-1,inplace=True)  
                   
            # test if column can be converted to an integer
            asint = props[col].fillna(0).astype(np.int64)
            result = (props[col] - asint)
            result = result.sum()
            if result > -0.01 and result < 0.01:
                IsInt = True
            '''
            
            # Make Integer/unsigned Integer datatypes
            if IsInt:
                if mn >= 0:
                    if mx < 255:
                        props[col] = props[col].astype(np.uint8)
                    elif mx < 65535:
                        props[col] = props[col].astype(np.uint16)
                    elif mx < 4294967295:
                        props[col] = props[col].astype(np.uint32)
                    else:
                        props[col] = props[col].astype(np.uint64)
                else:
                    if mn > np.iinfo(np.int8).min and mx < np.iinfo(np.int8).max:
                        props[col] = props[col].astype(np.int8)
                    elif mn > np.iinfo(np.int16).min and mx < np.iinfo(np.int16).max:
                        props[col] = props[col].astype(np.int16)
                    elif mn > np.iinfo(np.int32).min and mx < np.iinfo(np.int32).max:
                        props[col] = props[col].astype(np.int32)
                    elif mn > np.iinfo(np.int64).min and mx < np.iinfo(np.int64).max:
                        props[col] = props[col].astype(np.int64)    
            
            # Make float datatypes 32 bit
            else:
                props[col] = props[col].astype(np.float32)
            
            # Print new column type
            #print("dtype after: ",props[col].dtype)
            #print("******************************")
    
    # Print final result
    print("___MEMORY USAGE AFTER COMPLETION:___")
    mem_usg = props.memory_usage().sum() / 1024**2 
    print("Memory usage is: ",mem_usg," MB")
    print("This is ",100*mem_usg/start_mem_usg,"% of the initial size")
    return props, NAlist

###################################
###### Processing Trajectory ######
###################################


def findAnglesBetweenTwoVectors1(v1, v2):
    dot = np.einsum('ijk,ijk->ij',[v1,v1,v2],[v2,v1,v2])
    cos_= dot[0,:]
    sin_= np.cross(v1,v2)
    angle_= np.arctan2(sin_,cos_)
    return angle_

def Cam2CartBody(x_cam,y_cam,body_angle_cam):
    return y_cam,947-x_cam,body_angle_cam

def Rotate_Center_Trajectory(x,y,body_angle):
    Pos=np.array([x-x[0],y-y[0]])
    theta=-body_angle[0]
    body_angle_rotated=body_angle-body_angle[0]
    RotMat=np.array([[np.cos(theta),-np.sin(theta)],[np.sin(theta),np.cos(theta)]])
    PosRot=np.dot(RotMat,Pos)
    
    return PosRot[0,:],PosRot[1,:],body_angle_rotated

# Function required to fit the border
def compute_distance2edge(x,y,circle):
    dist2center=np.sqrt(  np.power(x-circle[0],2)  +  np.power(y-circle[1],2)  )
    return (circle[2]-dist2center)
%run smallestenclosingcircle_test.py

# Find Position Outliers:
def FindOutlierTrajectory(Log,MaxAngularSpeed,MaxSpeed,SmoothingWin=10):
    angular_speed=180/np.pi*np.abs(np.diff(np.unwrap(Log.body_angle)))
    id_angular_speed=np.where(angular_speed>MaxAngularSpeed)[0]

    speed=np.sqrt(np.diff(Log.x_pos)**2+np.diff(Log.y_pos)**2)
    id_speed=np.where(speed>MaxSpeed)[0]

    ind=np.union1d(id_speed,id_angular_speed)
    tracking_fail_body=np.zeros(len(Log))
    tracking_fail_body[ind]=1

    smoothed_tracking_fail_body=signal.lfilter(np.ones(SmoothingWin), 1,tracking_fail_body)>0
    smoothed_tracking_fail_body=smoothed_tracking_fail_body.astype(float)
    return smoothed_tracking_fail_body,ind


#######################################
###### Tail Bout Classification #######
#######################################

NameCat=['L_slow_capture_swim','L_long_capture_swim','L_burst_swim','L_O_bend',
         'L_J_turn','L_short_latency_C_start','L_slow1','L_routine_turn','L_slow2',
         'L_long_latency_C_start','L_approach_swim','L_spot_avoidance_turn',
         'L_high_angle_turn',       
         'R_slow_capture_swim','R_long_capture_swim','R_burst_swim','R_O_bend',
         'R_J_turn','R_short_latency_C_start','R_slow1','R_routine_turn','R_slow2',
         'R_long_latency_C_start','R_approach_swim','R_spot_avoidance_turn',
         'R_high_angle_turn']

NameCatShort=['SCS+','LCS+','BS+','O+',
         'J+','SLC+','S1+','RT+','S2+',
         'LLC+','AS+','SAT+',
         'HAT+',       
         'SCS-','LCS-','BS-','O-',
         'J-','SLC-','S1-','RT-','S2-',
         'LLC-','AS-','SAT-',
         'HAT-']


NameCatSym=['slow_capture_swim','long_capture_swim','burst_swim','O_bend',
         'J_turn','short_latency_C_start','slow1','routine_turn','slow2',
         'long_latency_C_start','approach_swim','spot_avoidance_turn',
         'high_angle_turn',
         'slow_capture_swim','long_capture_swim','burst_swim','O_bend',
         'J_turn','short_latency_C_start','slow1','routine_turn','slow2',
         'long_latency_C_start','approach_swim','spot_avoidance_turn',
         'high_angle_turn']

NameCat=np.array(NameCat)
NameCatSym=np.array(NameCatSym)
NameCatShort=np.array(NameCatShort)

CatOrderMagnitudeSym=np.array([10,6,8,0,1,2,4,12,7,11,3,9,5])
CatOrderMagnitude=np.hstack((CatOrderMagnitudeSym[::-1],CatOrderMagnitudeSym+13))
CatOrderMagnitudeSym=np.hstack((CatOrderMagnitudeSym,CatOrderMagnitudeSym))

NameCatSym=NameCatSym[CatOrderMagnitude.tolist()]
NameCatShort=NameCatShort[CatOrderMagnitude.tolist()]
NameCat=NameCat[CatOrderMagnitude.tolist()]

CatRemapOrder=np.argsort(CatOrderMagnitude)#[::-1]



def JoaoColormap(): clrs = np.array([0.392156863, 0.392156863, 0.392156863, 0, 0, 0, 1, 0.666666667, 0, 0.862745098, 0, 0.862745098, 0.980392157, 0.501960784, 0.447058824, 1, 0, 0.196078431, 0, 0.588235294, 1, 0, 0.6, 0, 0, 0, 0.784313725, 1, 1, 0, 0.4, 1, 1, 0.576470588, 0.439215686, 0.858823529, 0.411764706, 1, 0.4])

CatOrderMagnitude=np.array([10,6,8,0,1,2,4,12,7,11,3,9,5])[::-1]
clrs=clrs[CatOrderMagnitude,:]

return colors.ListedColormap(clrs)


import numpy_indexed as npi

CatOrderMagnitudeSym=np.array([10,6,8,0,1,2,4,12,7,11,3,9,5])
CatOrderMagnitude=np.hstack((CatOrderMagnitudeSym,CatOrderMagnitudeSym+13))
CatRemapOrder=np.argsort(CatOrderMagnitude)#[::-1]

NameCatShort=['SCS+','LCS+','BS+','O+',
         'J+','SLC+','S1+','RT+','S2+',
         'LLC+','AS+','SAT+',
         'HAT+',       
         'SCS-','LCS-','BS-','O-',
         'J-','SLC-','S1-','RT-','S2-',
         'LLC-','AS-','SAT-',
         'HAT-']

NameCat=['L_slow_capture_swim','L_long_capture_swim','L_burst_swim','L_O_bend',
         'L_J_turn','L_short_latency_C_start','L_slow1','L_routine_turn','L_slow2',
         'L_long_latency_C_start','L_approach_swim','L_spot_avoidance_turn',
         'L_high_angle_turn',       
         'R_slow_capture_swim','R_long_capture_swim','R_burst_swim','R_O_bend',
         'R_J_turn','R_short_latency_C_start','R_slow1','R_routine_turn','R_slow2',
         'R_long_latency_C_start','R_approach_swim','R_spot_avoidance_turn',
         'R_high_angle_turn']

NameCat=np.array(NameCat)
NameCat=NameCat[CatOrderMagnitude.tolist()]

NameCatShort=np.array(NameCatShort)
NameCatShort=NameCatShort[CatOrderMagnitude.tolist()]

b_CS=np.array([4,5,17,18])

NameCatShort=np.delete(NameCatShort,b_CS-1)
NameCat=np.delete(NameCat,b_CS-1)


def JoaoColormap():
    clrs = np.array([[0.392156863, 0.392156863, 0.392156863],
    [0, 0, 0],
    [1, 0.666666667, 0],
    [0.862745098, 0, 0.862745098],
    [0.980392157, 0.501960784, 0.447058824],
    [1, 0, 0.196078431],
    [0, 0.588235294, 1],
    [0, 0.6, 0],
    [0, 0, 0.784313725],
    [1, 1, 0],
    [0.4, 1, 1],
    [0.576470588, 0.439215686, 0.858823529],
    [0.411764706, 1, 0.4]])
    
    CatOrderMagnitude=np.array([10,6,8,0,1,2,4,12,7,11,3,9,5])
    clrs=clrs[CatOrderMagnitude,:]
    clrs=np.delete(clrs,np.array([4,5])-1,axis=0)
    return colors.ListedColormap(clrs)




